﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="Form_1101.aspx.cs" Inherits="AMED.Form_1101" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <title>รง.ผสต.1 - กรมการแพทย์ทหารบก</title>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <div class="container-fluid" id="content">
		<div id="left">
			<div class="subnav">
				<div class="subnav-title">
					<a href="#" class='toggle-subnav'><i class="icon-angle-down"></i><span><p class="icon-building"></p> หน่วยสายแพทย์</span></a>
				</div>
				<ul class="subnav-menu">
					<li>
						<a href="Default.aspx">สรุปภาพรวม</a>
					</li>
					<li class="active">
							<a href="Hotpital.aspx">การบันทึกข้อมูล</a>
							
						</li>
					<li>
						<a href="Report.aspx">สรุปรายงาน</a>
					</li>
				</ul>
			</div>
			<div class="subnav">
				<div class="subnav-title">
					<a href="#" class='toggle-subnav'><i class="icon-angle-down"></i><span><p class="icon-building"></p>  ส่วนกลาง</span></a>
				</div>
				<ul class="subnav-menu">
					<li>
						<a href="Default.aspx">สรุปภาพรวม</a>
					</li>
					<li>
							<a href="Department.aspx">การตรวจสอบการบันทึกข้อมูล</a>
							
					</li>
					<li>
						<a href="Report.aspx">สรุปรายงาน</a>
					</li>
				</ul>
			</div>
            <div class="subnav">
				<div class="subnav-title">
					<a href="#" class='toggle-subnav'><i class="icon-angle-down"></i><span><p class="icon-cog"></p> ข้อมูลพื้นฐาน</span></a>
				</div>
				<ul class="subnav-menu">
					<li class='dropdown'>
						<a href="#" data-toggle="dropdown"> ข้อมุลทั่วไป</a>
						<ul class="dropdown-menu">
							<li>
								<a href="MT_Region.aspx"> กองทัพภาค</a>
							</li>
							<li>
								<a href="MT_Hospital.aspx"> หน่วยสายแพทย์</a>
							</li>
							<li>
								<a href="MT_Department.aspx"> หน่วยงาน (ส่วนกลาง)</a>
							</li>
							<li>
								<a href="MT_AddOrganization.aspx"> สังกัด</a>
							</li>
							<li>
								<a href="MT_Province.aspx"> จังหวัด</a>
							</li>
						</ul>
					</li>
					<li class='dropdown'>
						<a href="#" data-toggle="dropdown">กองวิทยาการ</a>
						<ul class="dropdown-menu">
							<li>
								<a href="MTS_ICD10.aspx"> รหัสโรค (ICD10)</a>
							</li>
							<li>
								<a href="MTS_GroupRule.aspx"> กลุ่มโรคที่ขัดต่อกฎกระทรวง</a>
							</li>
							<li>
								<a href="MTS_Rule.aspx"> โรคที่ขัดต่อกฎกระทรวง</a>
							</li>
							<li>
								<a href="MTS_Sizebed.aspx"> ขนาดเตียง</a>
							</li>
							<li>
								<a href="MTS_ProHotpital.aspx"> ตำแหน่งในโรงพยาบาล</a>
							</li>
							<li>
								<a href="MTS_Wards.aspx"> หอผู้ป่วย</a>
							</li>
						</ul>
					</li>
                    <li class='dropdown'>
						<a href="#" data-toggle="dropdown">กองงบประมาณการเงิน</a>
						<ul class="dropdown-menu">
							<li>
								<a href="MTF_Assets.aspx">สินทรัพย์และหนี้สิน</a>
							</li>
							<li>
								<a href="MTF_Progress.aspx">ผลการดำเนินงาน</a>
							</li>
							<li>
								<a href="MTF_Reception.aspx">การรับ-การจ่าย</a>
							</li>
							<li>
								<a href="MTF_Deposit.aspx">เงินฝาก</a>
							</li>
						</ul>
					</li>
                    <li class='dropdown'>
						<a href="#" data-toggle="dropdown">กองส่งกำลังบำรุง</a>
						<ul class="dropdown-menu">
							<li>
								<a href="MTT_TypeDrug.aspx"> ประเภทยา</a>
							</li>
							<li>
								<a href="MTT_Drug.aspx"> ยา</a>
							</li>
							<li>
								<a href="MTT_DrugNature.aspx">ลักษณะยา</a>
							</li>
							<li>
								<a href="MTT_MedicalSupplies.aspx">เวชภัณฑ์</a>
							</li>
						</ul>
					</li>
                    <li class='dropdown'>
						<a href="#" data-toggle="dropdown">ผู้ใช้งาน (User)</a>
						<ul class="dropdown-menu">
							<li>
								<a href="MT_PersonType.aspx"> ประเภทบุคคล</a>
							</li>
							<li>
								<a href="MT_Person.aspx">บุคคลย่อย</a>
							</li>
							<li>
								<a href="#">กำหนดสิทธิ์</a>
							</li>
							<li>
								<a href="MT_User.aspx">ผู้ใช้งานในระบบ</a>
							</li>
						</ul>
					</li>
                     <li class='dropdown'>
						<a href="#" data-toggle="dropdown">การส่งรายงาน</a>
						<ul class="dropdown-menu">
							<li>
								<a href="MTR_DeadlineReports.aspx"> กำหนดส่งรายงาน</a>
							</li>
							<li>
								<a href="MTR_EditReport.aspx"> แก้ไขข้อมูลรายงาน</a>
							</li>
							<li>
								<a href="MTR_NameReport.aspx"> ชื่อแบบฟอร์มรายงาน</a>
							</li>
						</ul>
					</li>
				</ul>
			</div>
		</div>
		<div id="main">
			<div class="container-fluid">
				<div class="page-header">
					<div class="pull-left">
						<h4>รายงานจำนวนผู้ป่วยจำแนกตามประเภทบุคคล ( รง.ผสต.1 )</h4>
					</div>
					
				</div>
				<div class="breadcrumbs">
					<ul>
						<li>
							<a href="Default.aspx">Dashborad</a>
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<a href="forms-basic.html">กองวิทยาการ</a>
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<a href="MT_User.aspx">รง.ผสต.1</a>
						</li>
					</ul>
					<div class="close-bread">
						<a href="#"><i class="icon-remove"></i></a>
					</div>
				</div>
				

				<div class="row-fluid">

						<div class="span12">
						  <div class="box">
							<div class="box-title">
                                <div class='basic-margin'>
								<div class="fileupload fileupload-new" data-provides="fileupload">
									<div class="input-append">
										<div class="uneditable-input span3"><i class="icon-file fileupload-exists"></i> <span class="fileupload-preview"></span></div><span class="btn btn-file"><span class="fileupload-new">Select file</span><span class="fileupload-exists">Change</span><input type="file" name="aaaa" /></span><a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
								    	</div>
								   <div class="btn-group span2 pull-right">
                                  <%-- <button class="btn btn-info" rel="tooltip" title="edit"><i class="icon-edit"></i> แก้ไขเอกสาร</button>--%>
												<a href="#" data-toggle="dropdown" class="btn btn-warning dropdown-toggle" rel="tooltip" title="print"><i class="icon-print"></i> พิมพ์รายงาน <span class="caret"></span></a>
												<ul class="dropdown-menu dropdown-warning">
													<li>
														<a href="#"><i class="icon-file"></i> Excel</a>
													</li>
													<li>
														<a href="#"><i class="icon-file"></i> PDF</a>
													</li>
												</ul>
											</div></div>
                                </div>
							</div>
                            <br />

                           <div style="overflow-x:auto;" class="box-content nopadding">
                              
                              <form action="#" method="get" class='form-vertical'>
                                    <div class="span2">
											<div class="control-group">
												<label for="textfield" class="control-label">กองทัพ</label>
                                                <select name="s2" id="s2" class='select2-me input-small'>
												    <option value="1">ทั้งหมด</option>
												    <option value="2" selected disabled>ส่วนกลาง</option>
												    <option value="3">ภาคที่ 1</option>
												    <option value="4">ภาคที่ 2</option>
												    <option value="5">ภาคที่ 3</option>
												    <option value="6">ภาคที่ 4</option>
											      </select>
                                                <%--<asp:DropDownList ID="ddlทั้งหมด" runat="server" Width="150px">
                                                <asp:ListItem Text="หน่วยงาน (ส่วนกลาง)" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="กองทัพภาคที่ 2" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="กองทัพภาคที่ 3" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="กองทัพภาคที่ 4" Value="3"></asp:ListItem>
                                            </asp:DropDownList>--%>
											</div>
                                            
										</div>
										<div class="span3">
                                            <div class="control-group">
												<label for="textfield" class="control-label">หน่วยสายแพทย์</label>
                                                <select name="s2" id="s2" class='select2-me input-large'>
												    <option value="1">ทั้งหมด</option>
												    <option value="2" selected disabled>รพ.พระมงกุฎเกล้า</option>
												    <option value="3">รพ.อานันทมหิดล</option>
												    <option value="4">รพ.ค่ายสุรนารี</option>
											      </select>
                                                <%--<asp:DropDownList ID="DropDownList2" runat="server" Width="230px">
                                                <asp:ListItem Text="ทั้งหมด" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="รพ.พระมงกุฎเกล้า" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="รพ.อานันทมหิดล" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="รพ.ค่ายสุรนารี" Value="3"></asp:ListItem>
                                            </asp:DropDownList>--%>
											</div>
										</div>
                                          <div class="span2">
										    <div class="control-group">
                                                <label for="textfield" class="control-label">ปีงบประมาณ</label>
                                                   <select name="s2" id="s2" class='select2-me input-small'>
												    <option value="1">ทั้งหมด</option>
												    <option value="2" selected disabled>2560</option>
												    <option value="3">2559</option>
												    <option value="4">2558</option>
												    <option value="5">2557</option>
												    <option value="6">2556</option>
												    <option value="7">2555</option>
											      </select>
												 <%--<label for="textfield" class="control-label">ปีงบประมาณ</label>
                                                    <asp:DropDownList ID="DropDownList1" runat="server" Width="120px">
                                                    <asp:ListItem Text="ทั้งหมด" Value="0"></asp:ListItem>
                                                    <asp:ListItem Text="2560" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="2559" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="2558" Value="3"></asp:ListItem>
                                                    <asp:ListItem Text="2557" Value="4"></asp:ListItem>
                                                </asp:DropDownList>--%>
											    </div>
                                              </div>
										<div class="span3">
                                            <div class="control-group">
                                              <label for="textfield" class="control-label">ประจำเดือน</label>
                                                <select name="s2" id="s2" class='select2-me input-large'>
												    <option value="1">ทั้งหมด</option>
												    <option value="2" selected disabled>มกราคม</option>
												    <option value="3">กุมภาพันธ์</option>
												    <option value="4">มีนาคม</option>
												    <option value="5">เมษายน</option>
												    <option value="6">พฤษภาคม</option>
												    <option value="7">กรกฎาคม</option>
												    <option value="4">สิงหาคม</option>
												    <option value="5">กันยายน</option>
												    <option value="6">ตุลาคม</option>
												    <option value="7">พฤศจิกายน</option>
												    <option value="7">ธันวาคม</option>
											      </select>
												<%--<label for="textfield" class="control-label">ประจำเดือน</label>
                                                <asp:DropDownList ID="DropDownList3" runat="server" Width="200">
                                                <asp:ListItem Text="ทั้งหมด" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="มกราคม" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="กุมภาพันธ์" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="มีนาคม" Value="3"></asp:ListItem>
                                                <asp:ListItem Text="เมษายน" Value="4"></asp:ListItem>
                                                <asp:ListItem Text="พฤษภาคม" Value="5"></asp:ListItem>
                                                <asp:ListItem Text="กรกฎาคม" Value="6"></asp:ListItem>
                                                <asp:ListItem Text="สิงหาคม" Value="7"></asp:ListItem>
                                                <asp:ListItem Text="กันยายน" Value="8"></asp:ListItem>
                                                <asp:ListItem Text="ตุลาคม" Value="9"></asp:ListItem>
                                                <asp:ListItem Text="พฤศจิกายน" Value="10"></asp:ListItem>
                                                <asp:ListItem Text="ธันวาคม" Value="11"></asp:ListItem>
                                            </asp:DropDownList>--%>
											</div>
										</div>
									
								</form><br />

								<table class="table table-hover table-nomargin table-condensed table-bordered table-edit">
									<thead>
										<tr>
											<th rowspan="2" colspan="2" style="width: 400px"><p class="text-center">ประเภทบุคคล</p></th>
											<th colspan="2" style="width: 50px"><p class="text-center">ผู้ป่วยนอก</p></th>
											<th colspan="5" style="width: 100px"><p class="text-center">ผู้ป่วยใน</p></th>
										</tr>
                                        <tr>
											<th><p class="text-center">รายใหม่(คน)</p></th>
											<th><p class="text-center">จำนวน(ครั้ง)</p></th>
											<th><p class="text-center">ยอดยกมา</p></th>
											<th><p class="text-center">รับใหม่</p></th>
											<th><p class="text-center">รวม</p></th>
											<th><p class="text-center">จำหน่าย</p></th>
											<th><p class="text-center">วันรักษา</p></th>
                                        </tr>
									</thead>
									<tbody>
                                         <%------------------------  ประเภท ก  --------------------------%>
										<tr>
											<td rowspan="5"><p class="text-center">ก</p></td>
											<td>ก1 นายทหารประจำการ</td>
											<td><asp:TextBox ID="TextBox1" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox2" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox3" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox4" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox5" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox6" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox7" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
										</tr>
										<tr>
											<td>ก2  นายสิบ พลทหารประจำการ</td>
											<td><asp:TextBox ID="TextBox8" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox9" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox10" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox11" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox12" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox13" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox14" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
										</tr>
										<tr>
											<td>ก3  ข้าราชการกลาโหมพลเรือน</td>
											<td><asp:TextBox ID="TextBox15" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox16" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox17" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox18" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox19" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox20" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox21" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
										</tr>
										<tr>
											<td>ก4  ลูกจ้างประจำ</td>
											<td><asp:TextBox ID="TextBox22" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox23" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox24" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox25" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox26" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox27" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox28" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
										</tr>
										<tr>
											<td>ก5  ลูกจ้างชั่วคราว</td>
											<td><asp:TextBox ID="TextBox29" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox30" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox31" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox32" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox33" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox34" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox35" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
										</tr>
                                        <tr>
											<td colspan="2"><b class="text-center">รวมประเภท ก</b></td>
											<td><asp:TextBox ID="TextBox36" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox37" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox38" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox39" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox40" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox41" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox42" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
										</tr>
                                        <%------------------------  ประเภท ข  --------------------------%>
										<tr>
											<td rowspan="5"><p class="text-center">ข</p></td>
											<td>ข1  นายสิบ พลทหารกองประจำการ</td>
											<td><asp:TextBox ID="TextBox43" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox44" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox45" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox46" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox47" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox48" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox49" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
										</tr>
										<tr>
											<td>ข2  นักเรียนทหาร</td>
											<td><asp:TextBox ID="TextBox50" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox51" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox52" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox53" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox54" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox55" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox56" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
										</tr>
										<tr>
											<td>ข3  อาสาสมัครทหารพราน</td>
											<td><asp:TextBox ID="TextBox57" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox58" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox59" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox60" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox61" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox62" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox63" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
										</tr>
										<tr>
											<td>ข4  นักโทษทหาร</td>
											<td><asp:TextBox ID="TextBox64" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox65" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox66" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox67" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox68" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox69" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox70" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
										</tr>
										<tr>
											<td>ข5  พนักงานราชการ</td>
											<td><asp:TextBox ID="TextBox71" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox72" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox73" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox74" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox75" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox76" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox77" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
										</tr>
                                        <tr>
											<td colspan="2"><b class="text-center">รวมประเภท ข</b></td>
											<td><asp:TextBox ID="TextBox78" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox79" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox80" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox81" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox82" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox83" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox84" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
										</tr>
                                         <%------------------------  ประเภท ค --------------------------%>
										<tr>
											<td rowspan="10"><p class="text-center">ค</p></td>
											<td>ค1  ครอบครัวทหาร</td>
											<td><asp:TextBox ID="TextBox85" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox86" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox87" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox88" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox89" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox90" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox91" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
										</tr>
										<tr>
											<td>ค2  ทหารนอกประจำการ</td>
											<td><asp:TextBox ID="TextBox92" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox93" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox94" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox95" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox96" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox97" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox98" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
										</tr>
										<tr>
											<td>ค3  นักศึกษาวิชาทหาร (รด.) ระหว่างฝึก</td>
											<td><asp:TextBox ID="TextBox99" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox100" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox101" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox102" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox103" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox104" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox105" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
										</tr>
										<tr>
											<td>ค4  บุคคลที่เข้าร่วมโครงการวิวัฒน์พลเมือง</td>
											<td><asp:TextBox ID="TextBox106" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox107" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox108" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox109" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox110" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox111" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox112" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
										</tr>
										<tr>
											<td>ค5  บุคคลที่ใช้สิทธิ์ สปส. (ประกันสังคม)</td>
											<td><asp:TextBox ID="TextBox113" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox114" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox115" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox116" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox117" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox118" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox119" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
										</tr>
										<tr>
											<td>ค6  บุคคลที่ใช้สิทธิ์ สปสช. <br />(ประกันสุขภาพ 30 บาท)</td>
											<td><asp:TextBox ID="TextBox120" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox121" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox122" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox123" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox124" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox125" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox126" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
										</tr>
										<tr>
											<td>ค7  ข้าราชการพลเรือน (เบิกต้นสังกัด)</td>
											<td><asp:TextBox ID="TextBox127" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox128" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox129" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox130" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox131" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox132" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox133" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
										</tr>
										<tr>
											<td>ค8  บุคคลที่ไม่ใช้สิทธิ์ใดๆ (สิทธิ์ว่าง)</td>
											<td><asp:TextBox ID="TextBox134" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox135" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox136" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox137" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox138" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox139" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox140" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
										</tr>
										<tr>
											<td>ค9  บุคคลที่ใช้สิทธิ์อื่น รวมทั้งบุคคล<br />สัญชาติอื่น</td>
											<td><asp:TextBox ID="TextBox141" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox142" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox143" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox144" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox145" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox146" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox147" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
										</tr>
										<tr>
											<td>ค10 บุคคลที่มีความสัมพันธ์กับกำลังพล ทบ.</td>
											<td><asp:TextBox ID="TextBox148" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox149" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox150" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox151" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox152" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox153" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox154" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
										</tr>
                                        <tr>
											<td colspan="2"><b class="text-center">รวมประเภท ค</b></td>
											<td><asp:TextBox ID="TextBox155" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox156" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox157" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox158" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox159" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox160" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox161" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
										</tr>
                                        <tr>
                                            <td colspan="9">
									        <div class="form-actions pull-right">
										        <button type="submit" class="btn btn-green"><i class="icon-ok"></i> บันทึกข้อมูล</button>
										        <button type="submit" class="btn btn-green"><i class="icon-ok"></i> ยืนยันและส่งข้อมูล</button>
										        <button type="button" class="btn btn-lightred"><i class="icon-remove"></i> ยกเลิก</button>
									        </div>
                                            </td>
                                        </tr>
									</tbody>
								</table>
							</div>
						</div>
				   	</div>

				</div>
				
			</div>
		</div></div>
</asp:Content>









