﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="FormDepartment_1102.aspx.cs" Inherits="AMED.FormDepartment_1102" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <title>รง.ผสต.2 - กรมการแพทย์ทหารบก</title>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <div class="container-fluid" id="content">
		<div id="left">
			<div class="subnav">
				<div class="subnav-title">
					<a href="#" class='toggle-subnav'><i class="icon-angle-down"></i><span><p class="icon-building"></p> หน่วยสายแพทย์</span></a>
				</div>
				<ul class="subnav-menu">
					<li>
						<a href="Default.aspx">สรุปภาพรวม</a>
					</li>
					<li class="active">
							<a href="Hotpital.aspx">การบันทึกข้อมูล</a>
							
						</li>
					<li>
						<a href="Report.aspx">สรุปรายงาน</a>
					</li>
				</ul>
			</div>
			<div class="subnav">
				<div class="subnav-title">
					<a href="#" class='toggle-subnav'><i class="icon-angle-down"></i><span><p class="icon-building"></p>  ส่วนกลาง</span></a>
				</div>
				<ul class="subnav-menu">
					<li>
						<a href="Default.aspx">สรุปภาพรวม</a>
					</li>
					<li>
							<a href="Department.aspx">การตรวจสอบการบันทึกข้อมูล</a>
							
					</li>
					<li>
						<a href="Report.aspx">สรุปรายงาน</a>
					</li>
				</ul>
			</div>
            <div class="subnav">
				<div class="subnav-title">
					<a href="#" class='toggle-subnav'><i class="icon-angle-down"></i><span><p class="icon-cog"></p> ข้อมูลพื้นฐาน</span></a>
				</div>
				<ul class="subnav-menu">
					<li class='dropdown'>
						<a href="#" data-toggle="dropdown"> ข้อมุลทั่วไป</a>
						<ul class="dropdown-menu">
							<li>
								<a href="MT_Region.aspx"> กองทัพภาค</a>
							</li>
							<li>
								<a href="MT_Hospital.aspx"> หน่วยสายแพทย์</a>
							</li>
							<li>
								<a href="MT_Department.aspx"> หน่วยงาน (ส่วนกลาง)</a>
							</li>
							<li>
								<a href="MT_AddOrganization.aspx"> สังกัด</a>
							</li>
							<li>
								<a href="MT_Province.aspx"> จังหวัด</a>
							</li>
						</ul>
					</li>
					<li class='dropdown'>
						<a href="#" data-toggle="dropdown">กองวิทยาการ</a>
						<ul class="dropdown-menu">
							<li>
								<a href="MTS_ICD10.aspx"> รหัสโรค (ICD10)</a>
							</li>
							<li>
								<a href="MTS_GroupRule.aspx"> กลุ่มโรคที่ขัดต่อกฎกระทรวง</a>
							</li>
							<li>
								<a href="MTS_Rule.aspx"> โรคที่ขัดต่อกฎกระทรวง</a>
							</li>
							<li>
								<a href="MTS_Sizebed.aspx"> ขนาดเตียง</a>
							</li>
							<li>
								<a href="MTS_ProHotpital.aspx"> ตำแหน่งในโรงพยาบาล</a>
							</li>
							<li>
								<a href="MTS_Wards.aspx"> หอผู้ป่วย</a>
							</li>
						</ul>
					</li>
                    <li class='dropdown'>
						<a href="#" data-toggle="dropdown">กองงบประมาณการเงิน</a>
						<ul class="dropdown-menu">
							<li>
								<a href="MTF_Assets.aspx">สินทรัพย์และหนี้สิน</a>
							</li>
							<li>
								<a href="MTF_Progress.aspx">ผลการดำเนินงาน</a>
							</li>
							<li>
								<a href="MTF_Reception.aspx">การรับ-การจ่าย</a>
							</li>
							<li>
								<a href="MTF_Deposit.aspx">เงินฝาก</a>
							</li>
						</ul>
					</li>
                    <li class='dropdown'>
						<a href="#" data-toggle="dropdown">กองส่งกำลังบำรุง</a>
						<ul class="dropdown-menu">
							<li>
								<a href="MTT_TypeDrug.aspx"> ประเภทยา</a>
							</li>
							<li>
								<a href="MTT_Drug.aspx"> ยา</a>
							</li>
							<li>
								<a href="MTT_DrugNature.aspx">ลักษณะยา</a>
							</li>
							<li>
								<a href="MTT_MedicalSupplies.aspx">เวชภัณฑ์</a>
							</li>
						</ul>
					</li>
                    <li class='dropdown'>
						<a href="#" data-toggle="dropdown">ผู้ใช้งาน (User)</a>
						<ul class="dropdown-menu">
							<li>
								<a href="MT_PersonType.aspx"> ประเภทบุคคล</a>
							</li>
							<li>
								<a href="MT_Person.aspx">บุคคลย่อย</a>
							</li>
							<li>
								<a href="#">กำหนดสิทธิ์</a>
							</li>
							<li>
								<a href="MT_User.aspx">ผู้ใช้งานในระบบ</a>
							</li>
						</ul>
					</li>
                     <li class='dropdown'>
						<a href="#" data-toggle="dropdown">การส่งรายงาน</a>
						<ul class="dropdown-menu">
							<li>
								<a href="MTR_DeadlineReports.aspx"> กำหนดส่งรายงาน</a>
							</li>
							<li>
								<a href="MTR_EditReport.aspx"> แก้ไขข้อมูลรายงาน</a>
							</li>
							<li>
								<a href="MTR_NameReport.aspx"> ชื่อแบบฟอร์มรายงาน</a>
							</li>
						</ul>
					</li>
				</ul>
			</div>
		</div>
		<div id="main">
			<div class="container-fluid">
				<div class="page-header">
					<div class="pull-left">
						<h4>รายงานจำนวนผู้ป่วยในจำแนกตามสาเหตุป่วย ( รง.ผสต.2 )</h4>
					</div>
					
				</div>
				<div class="breadcrumbs">
					<ul>
						<li>
							<a href="Default.aspx">Dashborad</a>
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<a href="forms-basic.html">กองวิทยาการ</a>
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<a href="MT_User.aspx">รง.ผสต.2</a>
						</li>
					</ul>
					<div class="close-bread">
						<a href="#"><i class="icon-remove"></i></a>
					</div>
				</div>
				

				<div class="row-fluid">

						<div class="span12">
						  <div class="box"><div class="box-title">
                                <div class='basic-margin'>
                                   <form action="#" method="get" class='form-vertical'>
                                    <div class="span2">
											<div class="control-group">
												<label for="textfield" class="control-label">กองทัพ</label>
                                                <select name="s2" id="s2" class='select2-me input-small'>
												    <option value="1">ทั้งหมด</option>
												    <option value="2" selected disabled>ส่วนกลาง</option>
												    <option value="3">ภาคที่ 1</option>
												    <option value="4">ภาคที่ 2</option>
												    <option value="5">ภาคที่ 3</option>
												    <option value="6">ภาคที่ 4</option>
											      </select>
                                                <%--<asp:DropDownList ID="ddlทั้งหมด" runat="server" Width="150px">
                                                <asp:ListItem Text="หน่วยงาน (ส่วนกลาง)" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="กองทัพภาคที่ 2" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="กองทัพภาคที่ 3" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="กองทัพภาคที่ 4" Value="3"></asp:ListItem>
                                            </asp:DropDownList>--%>
											</div>
                                            
										</div>
										<div class="span3">
                                            <div class="control-group">
												<label for="textfield" class="control-label">หน่วยสายแพทย์</label>
                                                <select name="s2" id="s2" class='select2-me input-large'>
												    <option value="1">ทั้งหมด</option>
												    <option value="2" selected disabled>รพ.พระมงกุฎเกล้า</option>
												    <option value="3">รพ.อานันทมหิดล</option>
												    <option value="4">รพ.ค่ายสุรนารี</option>
											      </select>
                                                <%--<asp:DropDownList ID="DropDownList2" runat="server" Width="230px">
                                                <asp:ListItem Text="ทั้งหมด" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="รพ.พระมงกุฎเกล้า" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="รพ.อานันทมหิดล" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="รพ.ค่ายสุรนารี" Value="3"></asp:ListItem>
                                            </asp:DropDownList>--%>
											</div>
										</div>
                                          <div class="span2">
										    <div class="control-group">
                                                <label for="textfield" class="control-label">ปีงบประมาณ</label>
                                                   <select name="s2" id="s2" class='select2-me input-small'>
												    <option value="1">ทั้งหมด</option>
												    <option value="2" selected disabled>2560</option>
												    <option value="3">2559</option>
												    <option value="4">2558</option>
												    <option value="5">2557</option>
												    <option value="6">2556</option>
												    <option value="7">2555</option>
											      </select>
												 <%--<label for="textfield" class="control-label">ปีงบประมาณ</label>
                                                    <asp:DropDownList ID="DropDownList1" runat="server" Width="120px">
                                                    <asp:ListItem Text="ทั้งหมด" Value="0"></asp:ListItem>
                                                    <asp:ListItem Text="2560" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="2559" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="2558" Value="3"></asp:ListItem>
                                                    <asp:ListItem Text="2557" Value="4"></asp:ListItem>
                                                </asp:DropDownList>--%>
											    </div>
                                              </div>
										<div class="span3">
                                            <div class="control-group">
                                              <label for="textfield" class="control-label">ประจำเดือน</label>
                                                <select name="s2" id="s2" class='select2-me input-large'>
												    <option value="1">ทั้งหมด</option>
												    <option value="2" selected disabled>มกราคม</option>
												    <option value="3">กุมภาพันธ์</option>
												    <option value="4">มีนาคม</option>
												    <option value="5">เมษายน</option>
												    <option value="6">พฤษภาคม</option>
												    <option value="7">กรกฎาคม</option>
												    <option value="4">สิงหาคม</option>
												    <option value="5">กันยายน</option>
												    <option value="6">ตุลาคม</option>
												    <option value="7">พฤศจิกายน</option>
												    <option value="7">ธันวาคม</option>
											      </select>
												<%--<label for="textfield" class="control-label">ประจำเดือน</label>
                                                <asp:DropDownList ID="DropDownList3" runat="server" Width="200">
                                                <asp:ListItem Text="ทั้งหมด" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="มกราคม" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="กุมภาพันธ์" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="มีนาคม" Value="3"></asp:ListItem>
                                                <asp:ListItem Text="เมษายน" Value="4"></asp:ListItem>
                                                <asp:ListItem Text="พฤษภาคม" Value="5"></asp:ListItem>
                                                <asp:ListItem Text="กรกฎาคม" Value="6"></asp:ListItem>
                                                <asp:ListItem Text="สิงหาคม" Value="7"></asp:ListItem>
                                                <asp:ListItem Text="กันยายน" Value="8"></asp:ListItem>
                                                <asp:ListItem Text="ตุลาคม" Value="9"></asp:ListItem>
                                                <asp:ListItem Text="พฤศจิกายน" Value="10"></asp:ListItem>
                                                <asp:ListItem Text="ธันวาคม" Value="11"></asp:ListItem>
                                            </asp:DropDownList>--%>
											</div>
										</div>
									
								</form><br />
								   <div class="btn-group span2 pull-right">
										<a href="#" data-toggle="dropdown" class="btn btn-warning dropdown-toggle"><i class="icon-print"></i> พิมพ์รายงาน <span class="caret"></span></a>
										<ul class="dropdown-menu dropdown-warning">
											<li><a href="#"><i class="icon-file"></i> Excel</a></li>
											<li><a href="#"><i class="icon-file"></i> PDF</a></li>
										</ul>
									</div>
                                </div>
							</div><br />
								<table class="table table-hover table-nomargin table-condensed table-bordered table-edit">
									<thead>
										<tr>
											<th rowspan="2" style="width: 20px"><p class="text-center">ลำดับ</p></th>
											<th rowspan="2" style="width: 50px"><p class="text-center">ประเภทบุคคล</p></th>
											<th rowspan="2" style="width: 50px"><p class="text-center">รหัสโรค ที่ 1</p></th>
											<th rowspan="2" style="width: 50px"><p class="text-center">รหัสโรค ที่ 2</p></th>
											<th colspan="3" style="width: 50px"><p class="text-center">รับ</p></th>
											<th colspan="4" style="width: 100px"><p class="text-center">จำหน่าย</p></th>
										</tr>
                                        <tr>
											<th><p class="text-center">ยกมา</p></th>
											<th><p class="text-center">รับใหม่</p></th>
											<th><p class="text-center">รวม</p></th>
											<th><p class="text-center">ปลดพิการ</p></th>
											<th><p class="text-center">ตาย</p></th>
											<th><p class="text-center">อื่นๆ</p></th>
											<th><p class="text-center">รวม</p></th>
                                        </tr>
									</thead>
									<tbody>
										<tr>
											<td><p class="text-center">1</p></td>
											<td>
                                            <asp:DropDownList ID="ddlประเภทบุคคล" class="control" runat="server" Width="100px">
                                                <asp:ListItem Text="ประเภทบุคคล" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="ประเภท ก" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="ประเภท ข" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="ประเภท ค" Value="3"></asp:ListItem>
                                            </asp:DropDownList>
                                           </td>
                                            <td>
                                            <asp:DropDownList ID="DropDownList19" class="control"  runat="server" Width="150px">
                                                <asp:ListItem Text="Select Disease 1" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="A000-1" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="A000-2" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="A000-3" Value="3"></asp:ListItem>
                                                <asp:ListItem Text="A000-4" Value="4"></asp:ListItem>
                                            </asp:DropDownList>
                                           </td>
											<td>
                                            <asp:DropDownList ID="DropDownList20" class="control"  runat="server" Width="150px">
                                                <asp:ListItem Text="Select Disease2" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="A000-1" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="A000-2" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="A000-3" Value="3"></asp:ListItem>
                                                <asp:ListItem Text="A000-4" Value="4"></asp:ListItem>
                                            </asp:DropDownList>
                                           </td>
											<td><asp:TextBox ID="TextBox1" runat="server" CssClass="input-block-level text-right text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox2" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox3" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox4" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox5" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox6" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox7" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
										</tr>
										<tr>
											<td><p class="text-center">2</p></td>
											<td>
                                            <asp:DropDownList ID="DropDownList21" class="control"  runat="server" Width="100px">
                                                <asp:ListItem Text="ประเภทบุคคล" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="ประเภท ก" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="ประเภท ข" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="ประเภท ค" Value="3"></asp:ListItem>
                                            </asp:DropDownList>
                                           </td>
											<td>
                                            <asp:DropDownList ID="DropDownList17" class="control" runat="server" Width="150px">
                                                <asp:ListItem Text="Select Disease 1" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="A000-1" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="A000-2" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="A000-3" Value="3"></asp:ListItem>
                                                <asp:ListItem Text="A000-4" Value="4"></asp:ListItem>
                                            </asp:DropDownList>
                                           </td>
											<td>
                                            <asp:DropDownList ID="DropDownList18" class="control" runat="server" Width="150px">
                                                <asp:ListItem Text="Select Disease2" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="A000-1" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="A000-2" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="A000-3" Value="3"></asp:ListItem>
                                                <asp:ListItem Text="A000-4" Value="4"></asp:ListItem>
                                            </asp:DropDownList>
                                           </td>
											<td><asp:TextBox ID="TextBox8" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox9" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox10" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox11" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox12" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox13" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox14" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
										</tr>
										<tr>
											<td><p class="text-center">3</p></td>
											<td>
                                            <asp:DropDownList ID="DropDownList22" class="control" runat="server" Width="100px">
                                                <asp:ListItem Text="ประเภทบุคคล" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="ประเภท ก" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="ประเภท ข" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="ประเภท ค" Value="3"></asp:ListItem>
                                            </asp:DropDownList>
                                           </td>
											<td>
                                            <asp:DropDownList ID="DropDownList15" class="control" runat="server" Width="150px">
                                                <asp:ListItem Text="Select Disease 1" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="A000-1" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="A000-2" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="A000-3" Value="3"></asp:ListItem>
                                                <asp:ListItem Text="A000-4" Value="4"></asp:ListItem>
                                            </asp:DropDownList>
                                           </td>
											<td>
                                            <asp:DropDownList ID="DropDownList16" class="control" runat="server" Width="150px">
                                                <asp:ListItem Text="Select Disease2" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="A000-1" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="A000-2" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="A000-3" Value="3"></asp:ListItem>
                                                <asp:ListItem Text="A000-4" Value="4"></asp:ListItem>
                                            </asp:DropDownList>
                                           </td>
											<td><asp:TextBox ID="TextBox15" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox16" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox17" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox18" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox19" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox20" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox21" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
										</tr>
										<tr>
											<td><p class="text-center">4</p></td>
											<td>
                                            <asp:DropDownList ID="DropDownList23" class="control"  runat="server" Width="100px">
                                                <asp:ListItem Text="ประเภทบุคคล" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="ประเภท ก" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="ประเภท ข" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="ประเภท ค" Value="3"></asp:ListItem>
                                            </asp:DropDownList>
                                           </td>
											<td>
                                            <asp:DropDownList ID="DropDownList13" class="control" runat="server" Width="150px">
                                                <asp:ListItem Text="Select Disease 1" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="A000-1" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="A000-2" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="A000-3" Value="3"></asp:ListItem>
                                                <asp:ListItem Text="A000-4" Value="4"></asp:ListItem>
                                            </asp:DropDownList>
                                           </td>
											<td>
                                            <asp:DropDownList ID="DropDownList14" class="control" runat="server" Width="150px">
                                                <asp:ListItem Text="Select Disease2" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="A000-1" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="A000-2" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="A000-3" Value="3"></asp:ListItem>
                                                <asp:ListItem Text="A000-4" Value="4"></asp:ListItem>
                                            </asp:DropDownList>
                                           </td>
											<td><asp:TextBox ID="TextBox22" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox23" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox24" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox25" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox26" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox27" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox28" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
										</tr>
										<tr>
											<td><p class="text-center">5</p></td>
											<td>
                                            <asp:DropDownList ID="DropDownList24" class="control" runat="server" Width="100px">
                                                <asp:ListItem Text="ประเภทบุคคล" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="ประเภท ก" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="ประเภท ข" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="ประเภท ค" Value="3"></asp:ListItem>
                                            </asp:DropDownList>
                                           </td>
											<td>
                                            <asp:DropDownList ID="DropDownList11" class="control" runat="server" Width="150px">
                                                <asp:ListItem Text="Select Disease 1" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="A000-1" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="A000-2" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="A000-3" Value="3"></asp:ListItem>
                                                <asp:ListItem Text="A000-4" Value="4"></asp:ListItem>
                                            </asp:DropDownList>
                                           </td>
											<td>
                                            <asp:DropDownList ID="DropDownList12" class="control" runat="server" Width="150px">
                                                <asp:ListItem Text="Select Disease2" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="A000-1" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="A000-2" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="A000-3" Value="3"></asp:ListItem>
                                                <asp:ListItem Text="A000-4" Value="4"></asp:ListItem>
                                            </asp:DropDownList>
                                           </td>
											<td><asp:TextBox ID="TextBox29" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox30" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox31" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox32" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox33" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox34" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox35" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
										</tr>
										<tr>
                                            <td colspan="11">
									        <div class="form-actions pull-right">
										        <button type="submit" class="btn btn-green"><i class="icon-ok"></i> ตรวจสอบข้อมูล</button>
										        <button type="button" class="btn btn-lightred"><i class="icon-remove"></i> ยกเลิก</button>
									        </div>
                                            </td>
                                        </tr>
									</tbody>
								</table>
							</div>
						</div>
				   	</div>

				</div>
			  </div>	
		   </div>
        </div>
</asp:Content>










