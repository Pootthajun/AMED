﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="Form_1112.aspx.cs" Inherits="AMED.Form_1112" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <title>รง.ผสต.12 - กรมการแพทย์ทหารบก</title>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <div class="container-fluid" id="content">
		<div id="left">
			<div class="subnav">
				<div class="subnav-title">
					<a href="#" class='toggle-subnav'><i class="icon-angle-down"></i><span><p class="icon-building"></p> หน่วยสายแพทย์</span></a>
				</div>
				<ul class="subnav-menu">
					<li>
						<a href="Default.aspx">สรุปภาพรวม</a>
					</li>
					<li class="active">
							<a href="Hotpital.aspx">การบันทึกข้อมูล</a>
							
						</li>
					<li>
						<a href="Report.aspx">สรุปรายงาน</a>
					</li>
				</ul>
			</div>
			<div class="subnav">
				<div class="subnav-title">
					<a href="#" class='toggle-subnav'><i class="icon-angle-down"></i><span><p class="icon-building"></p>  ส่วนกลาง</span></a>
				</div>
				<ul class="subnav-menu">
					<li>
						<a href="Default.aspx">สรุปภาพรวม</a>
					</li>
					<li>
							<a href="Department.aspx">การตรวจสอบการบันทึกข้อมูล</a>
							
					</li>
					<li>
						<a href="Report.aspx">สรุปรายงาน</a>
					</li>
				</ul>
			</div>
            <div class="subnav">
				<div class="subnav-title">
					<a href="#" class='toggle-subnav'><i class="icon-angle-down"></i><span><p class="icon-cog"></p> ข้อมูลพื้นฐาน</span></a>
				</div>
				<ul class="subnav-menu">
					<li class='dropdown'>
						<a href="#" data-toggle="dropdown"> ข้อมุลทั่วไป</a>
						<ul class="dropdown-menu">
							<li>
								<a href="MT_Region.aspx"> กองทัพภาค</a>
							</li>
							<li>
								<a href="MT_Hospital.aspx"> หน่วยสายแพทย์</a>
							</li>
							<li>
								<a href="MT_Department.aspx"> หน่วยงาน (ส่วนกลาง)</a>
							</li>
							<li>
								<a href="MT_AddOrganization.aspx"> สังกัด</a>
							</li>
							<li>
								<a href="MT_Province.aspx"> จังหวัด</a>
							</li>
						</ul>
					</li>
					<li class='dropdown'>
						<a href="#" data-toggle="dropdown">กองวิทยาการ</a>
						<ul class="dropdown-menu">
							<li>
								<a href="MTS_ICD10.aspx"> รหัสโรค (ICD10)</a>
							</li>
							<li>
								<a href="MTS_GroupRule.aspx"> กลุ่มโรคที่ขัดต่อกฎกระทรวง</a>
							</li>
							<li>
								<a href="MTS_Rule.aspx"> โรคที่ขัดต่อกฎกระทรวง</a>
							</li>
							<li>
								<a href="MTS_Sizebed.aspx"> ขนาดเตียง</a>
							</li>
							<li>
								<a href="MTS_ProHotpital.aspx"> ตำแหน่งในโรงพยาบาล</a>
							</li>
							<li>
								<a href="MTS_Wards.aspx"> หอผู้ป่วย</a>
							</li>
						</ul>
					</li>
                    <li class='dropdown'>
						<a href="#" data-toggle="dropdown">กองงบประมาณการเงิน</a>
						<ul class="dropdown-menu">
							<li>
								<a href="MTF_Assets.aspx">สินทรัพย์และหนี้สิน</a>
							</li>
							<li>
								<a href="MTF_Progress.aspx">ผลการดำเนินงาน</a>
							</li>
							<li>
								<a href="MTF_Reception.aspx">การรับ-การจ่าย</a>
							</li>
							<li>
								<a href="MTF_Deposit.aspx">เงินฝาก</a>
							</li>
						</ul>
					</li>
                    <li class='dropdown'>
						<a href="#" data-toggle="dropdown">กองส่งกำลังบำรุง</a>
						<ul class="dropdown-menu">
							<li>
								<a href="MTT_TypeDrug.aspx"> ประเภทยา</a>
							</li>
							<li>
								<a href="MTT_Drug.aspx"> ยา</a>
							</li>
							<li>
								<a href="MTT_DrugNature.aspx">ลักษณะยา</a>
							</li>
							<li>
								<a href="MTT_MedicalSupplies.aspx">เวชภัณฑ์</a>
							</li>
						</ul>
					</li>
                    <li class='dropdown'>
						<a href="#" data-toggle="dropdown">ผู้ใช้งาน (User)</a>
						<ul class="dropdown-menu">
							<li>
								<a href="MT_PersonType.aspx"> ประเภทบุคคล</a>
							</li>
							<li>
								<a href="MT_Person.aspx">บุคคลย่อย</a>
							</li>
							<li>
								<a href="#">กำหนดสิทธิ์</a>
							</li>
							<li>
								<a href="MT_User.aspx">ผู้ใช้งานในระบบ</a>
							</li>
						</ul>
					</li>
                     <li class='dropdown'>
						<a href="#" data-toggle="dropdown">การส่งรายงาน</a>
						<ul class="dropdown-menu">
							<li>
								<a href="MTR_DeadlineReports.aspx"> กำหนดส่งรายงาน</a>
							</li>
							<li>
								<a href="MTR_EditReport.aspx"> แก้ไขข้อมูลรายงาน</a>
							</li>
							<li>
								<a href="MTR_NameReport.aspx"> ชื่อแบบฟอร์มรายงาน</a>
							</li>
						</ul>
					</li>
				</ul>
			</div>
		</div>
			
		<div id="main">
			<div class="container-fluid">
				<div class="page-header">
					<div class="pull-left">
						<h4>รายงานข้อมูลการตรวจร่างกายของครอบครัวกำลังพลกองทัพบกรายบุคคล ( รง.ผสต.12 )</h4>
					</div>
					
				</div>
				<div class="breadcrumbs">
					<ul>
						<li>
							<a href="Default.aspx">Dashborad</a>
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<a href="forms-basic.html">กองวิทยาการ</a>
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<a href="MT_User.aspx">รง.ผสต.12</a>
						</li>
					</ul>
					<div class="close-bread">
						<a href="#"><i class="icon-remove"></i></a>
					</div>
				</div>
				

				<div class="row-fluid">

						<div class="span12">
						  <div class="box">
							<div class="box-title">
                                <div class='basic-margin'>
								  <div class="fileupload fileupload-new" data-provides="fileupload">
									<div class="input-append">
										<div class="uneditable-input span3"><i class="icon-file fileupload-exists"></i> <span class="fileupload-preview"></span></div><span class="btn btn-file"><span class="fileupload-new">Select file</span><span class="fileupload-exists">Change</span><input type="file" name="aaaa" /></span><a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
								    	</div>
                                    
                                    <div class="btn-group span2 pull-left">
                                        <button class="btn btn-info" rel="tooltip" title="Add"><i class="icon-plus"></i> เพิ่มข้อมูล</button>
                                    </div>
								   <div class="btn-group span2 pull-right">
												<a href="#" data-toggle="dropdown" class="btn btn-warning dropdown-toggle" rel="tooltip" title="print"><i class="icon-print"></i> พิมพ์รายงาน <span class="caret"></span></a>
												<ul class="dropdown-menu dropdown-warning">
													<li>
														<a href="#"><i class="icon-file"></i> Excel</a>
													</li>
													<li>
														<a href="#"><i class="icon-file"></i> PDF</a>
													</li>
												</ul>
											</div></div>
                                </div>
							</div>
                            <br />
                            <div style="overflow-x:auto;" class="box-content nopadding"><br />
                               <form action="#" method="get" class='form-vertical'>
                                    <div class="span1">
											<div class="control-group">
												<label for="textfield" class="control-label">กองทัพ</label>
                                                <asp:DropDownList ID="ddlทั้งหมด" runat="server" Width="90px">
                                                <asp:ListItem Text=" ส่วนกลาง" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="ภาคที่ 2" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="ภาคที่ 3" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="ภาคที่ 4" Value="3"></asp:ListItem>
                                            </asp:DropDownList>
											</div>
                                            
										</div>
										<div class="span3">
                                            <div class="control-group">
												<label for="textfield" class="control-label">หน่วยสายแพทย์</label>
                                                <asp:DropDownList ID="DropDownList6" runat="server" Width="200px">
                                                <asp:ListItem Text="ทั้งหมด" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="รพ.พระมงกุฎเกล้า" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="รพ.อานันทมหิดล" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="รพ.ค่ายสุรนารี" Value="3"></asp:ListItem>
                                            </asp:DropDownList>
											</div>
										</div>
									 <div class="span1">
										    <div class="control-group">
												    <label for="textfield" class="control-label">ปีงบประมาณ</label>
                                                    <asp:DropDownList ID="DropDownList7" runat="server" Width="90px">
                                                    <asp:ListItem Text="ทั้งหมด" Value="0"></asp:ListItem>
                                                    <asp:ListItem Text="2560" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="2559" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="2558" Value="3"></asp:ListItem>
                                                    <asp:ListItem Text="2557" Value="4"></asp:ListItem>
                                                </asp:DropDownList>
											    </div>
                                              </div>
										<div class="span4">
                                            <div class="control-group">
												<label for="textfield" class="control-label">ประจำเดือน</label>
                                                <asp:DropDownList ID="DropDownList8" runat="server" Width="200">
                                                <asp:ListItem Text="ทั้งหมด" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="มกราคม" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="กุมภาพันธ์" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="มีนาคม" Value="3"></asp:ListItem>
                                                <asp:ListItem Text="เมษายน" Value="4"></asp:ListItem>
                                                <asp:ListItem Text="พฤษภาคม" Value="5"></asp:ListItem>
                                                <asp:ListItem Text="กรกฎาคม" Value="6"></asp:ListItem>
                                                <asp:ListItem Text="สิงหาคม" Value="7"></asp:ListItem>
                                                <asp:ListItem Text="กันยายน" Value="8"></asp:ListItem>
                                                <asp:ListItem Text="ตุลาคม" Value="9"></asp:ListItem>
                                                <asp:ListItem Text="พฤศจิกายน" Value="10"></asp:ListItem>
                                                <asp:ListItem Text="ธันวาคม" Value="11"></asp:ListItem>
                                            </asp:DropDownList>
											</div>
										</div>
								</form><br />
								<table class="table table-hover table-nomargin table-condensed table-bordered table-edit">
									<thead>
										<tr>
											<th rowspan="3" style="width: 20px"><p class="text-center">ลำดับ</p></th>
											<th colspan="3"><p class="text-center">เครื่องมือ</p></th>
											<th rowspan="3" style="width: 100px"><p class="text-center">ยศ</p></th>
											<th rowspan="3" style="width: 100px"><p class="text-center">ชื่อ</p></th>
											<th rowspan="3" style="width: 100px"><p class="text-center">นามสกุล</p></th>
											<th rowspan="3" style="width: 100px"><p class="text-center">เลขประจำตัวประชาชน</p></th>
											<th rowspan="3" style="width: 100px"><p class="text-center">สังกัด</p></th>
											<th rowspan="3" style="width: 100px"><p class="text-center">ตำแหน่ง</p></th>
											<th rowspan="3" style="width: 150px"><p class="text-center">ช่วยราชการ (ระบุ)</p></th>
											<th rowspan="3" style="width: 100px"><p class="text-center">อายุ (ปี)</p></th>
											<th rowspan="3" style="width: 100px"><p class="text-center">เพศ</p></th>
											<th rowspan="3" style="width: 100px"><p class="text-center">สิทธิเบิก</p></th>
											<th colspan="4" style="width: 200px"><p class="text-center">ดัชนีมวลกาย (BMI)</p></th>
											<th colspan="2" style="width: 100px"><p class="text-center">BP</p></th>
											<th colspan="2" style="width: 100px"><p class="text-center">Chest x-ray</p></th>
											<th colspan="5" style="width: 100px"><p class="text-center">Urine Examination</p></th>
											<th colspan="12" style="width: 100px"><p class="text-center">ผลการตรวจเลือด</p></th>
											<th colspan="2" style="width: 100px"><p class="text-center">Pap smear</p></th>
											<th colspan="2" style="width: 100px"><p class="text-center">ประวัติโรคประจำตัว</p></th>
											<th colspan="3" style="width: 100px"><p class="text-center">พฤติกรรมการดำเนินชีวิตที่มีผลต่อความเสี่ยงเป็นโรค</p></th>
										</tr>
										<tr>
											<th rowspan="2"><p class="text-center">ดู</p></th>
											<th rowspan="2"><p class="text-center">แก้ไข</p></th>
											<th rowspan="2"><p class="text-center">ลบ</p></th>
											<th rowspan="2" style="width: 100px"><p class="text-center">น้ำหนัก(kg)</p></th>
											<th rowspan="2" style="width: 100px"><p class="text-center">ความสูง(m)</p></th>
											<th rowspan="2" style="width: 100px"><p class="text-center">BMI(kg/m2)</p></th>
											<th rowspan="2" style="width: 100px"><p class="text-center">รอบเอว(inch)</p></th>
											<th rowspan="2" style="width: 100px"><p class="text-center">Systolic(mmHg)</p></th>
											<th rowspan="2" style="width: 100px"><p class="text-center">Diastolic(mmHg)</p></th>
											<th rowspan="2" style="width: 100px"><p class="text-center">ผล</p></th>
											<th rowspan="2" style="width: 100px"><p class="text-center">ผิดปกติ</p></th>
											<th colspan="2" style="width: 100px"><p class="text-center">ผล</p></th>
											<th colspan="3" style="width: 150px"><p class="text-center">ผิดปกติ</p></th>
											<th colspan="2" style="width: 100px"><p class="text-center">CBC</p></th>
											<th colspan="10" style="width: 100px"><p class="text-center">Blood Chemistry</p></th>
											<th rowspan="2" style="width: 100px"><p class="text-center">ผล</p></th>
											<th rowspan="2" style="width: 100px"><p class="text-center">ผิดปกติ</p></th>
											<th rowspan="2" style="width: 100px"><p class="text-center">ผล</p></th>
											<th rowspan="2" style="width: 100px"><p class="text-center">โรคอื่นๆ (ระบุ)</p></th>
											<th rowspan="2" style="width: 100px"><p class="text-center">การสูบบุหรี่</p></th>
											<th rowspan="2" style="width: 100px"><p class="text-center">การดื่มเครื่องดื่มแอลกอฮอล์</p></th>
											<th rowspan="2" style="width: 100px"><p class="text-center">การออกกำลังกาย</p></th>
										</tr>
                                        <tr>
											<th style="width: 100px"><p class="text-center">ผล</p></th>
											<th style="width: 100px"><p class="text-center">ผิดปกติ (อื่นๆ)</p></th>
											<th style="width: 100px"><p class="text-center">Proteinurea>1+</p></th>
											<th style="width: 100px"><p class="text-center">Hematuria>1+</p></th>
											<th style="width: 100px"><p class="text-center">ผิดปกติอื่นๆ</p></th>
											<th style="width: 100px"><p class="text-center">ผล</p></th>
											<th style="width: 100px"><p class="text-center">ผิดปกติ (อื่นๆ)</p></th>
											<th style="width: 100px"><p class="text-center">Glu(mg/dL)</p></th>
											<th style="width: 100px"><p class="text-center">Chol(mg/dL)</p></th>
											<th style="width: 100px"><p class="text-center">TG(mg/dL)</p></th>
											<th style="width: 100px"><p class="text-center">HDL-C(mg/dL)</p></th>
											<th style="width: 100px"><p class="text-center">LDL-C(mg/dL)</p></th>
											<th style="width: 100px"><p class="text-center">BUN(mg/dL)</p></th>
											<th style="width: 100px"><p class="text-center">Cr(mg/dL)</p></th>
											<th style="width: 100px"><p class="text-center">Uric(mg/dL)</p></th>
											<th style="width: 100px"><p class="text-center">AST(U/L)</p></th>
											<th style="width: 100px"><p class="text-center">ALT(U/L)</p></th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td><p class="text-center">1</p></td>
											<td><a href="#" class="btn" rel="tooltip" title="View"><i class="icon-search text-info"></i></a></td>
											<td><a href="#" class="btn" rel="tooltip" title="Edit"><i class="icon-edit text-success"></i></a></td>	
											<td><a href="#" class="btn" rel="tooltip" title="Delete"><i class="icon-remove text-warning"></i></a></td>
											<td><asp:TextBox ID="TextBox1" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox2" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox3" runat="server" CssClass="input-block-level text-right" Width="150"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox4" runat="server" CssClass="input-block-level text-right" Width="150"></asp:TextBox></td>
											<td><asp:DropDownList ID="DropDownList1" Class="control" runat="server" Width="150px">
                                                <asp:ListItem Text="สังกัด" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="สังกัด 1" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="สังกัด 2" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="สังกัด 3" Value="3"></asp:ListItem>
                                                <asp:ListItem Text="สังกัด 4" Value="4"></asp:ListItem>
                                            </asp:DropDownList>
                                           </td>
                                            <td><asp:DropDownList ID="DropDownList12" Class="control" runat="server" Width="150px">
                                                <asp:ListItem Text="ตำแหน่ง" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="ตำแหน่ง 1" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="ตำแหน่ง 2" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="ตำแหน่ง 3" Value="3"></asp:ListItem>
                                                <asp:ListItem Text="ตำแหน่ง 4" Value="4"></asp:ListItem>
                                            </asp:DropDownList>
                                           </td>
											<td><asp:TextBox ID="TextBox27" runat="server" CssClass="input-block-level text-right" Width="150"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox28" runat="server" CssClass="input-block-level text-right" Width="50"></asp:TextBox></td>
											<td><asp:DropDownList ID="DropDownList16" Class="control" runat="server" Width="80px">
                                                <asp:ListItem Text="เพศ" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="ชาย 1" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="หญิง 2" Value="2"></asp:ListItem>
                                            </asp:DropDownList>
                                           </td>
                                            <td><asp:DropDownList ID="DropDownList5" Class="control" runat="server" Width="120px">
                                                <asp:ListItem Text="ข้าราชการ" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="รัฐวิสาหกิจ" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="ปกส." Value="2"></asp:ListItem>
                                                <asp:ListItem Text="สปสช." Value="3"></asp:ListItem>
                                                <asp:ListItem Text="อื่นๆ" Value="4"></asp:ListItem>
                                            </asp:DropDownList></td>
											<td><asp:TextBox ID="TextBox29" runat="server" CssClass="input-block-level text-right" Width="50"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox30" runat="server" CssClass="input-block-level text-right" Width="50"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox31" runat="server" CssClass="input-block-level text-right" Width="50"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox32" runat="server" CssClass="input-block-level text-right" Width="50"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox33" runat="server" CssClass="input-block-level text-right" Width="50"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox34" runat="server" CssClass="input-block-level text-right" Width="50"></asp:TextBox></td>
										
                                            <td><asp:DropDownList ID="DropDownList17" Class="control" runat="server" Width="100px">
                                                <asp:ListItem Text="Select" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="0=ไม่มีโรคประจำตัว" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="1=ปกติ" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="2=ผิดปกติ" Value="3"></asp:ListItem>
                                            </asp:DropDownList>
                                           </td>
											<td><input type="text" name="textfield" id="num11" class="input-block-level text-right" Width="150"></td>
											<td><asp:DropDownList ID="DropDownList36" Class="control" runat="server" Width="100px">
                                                <asp:ListItem Text="Select" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="0=ไม่ได้ตรวจ,error" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="1=ปกติ" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="2=ผิดปกติ" Value="3"></asp:ListItem>
                                            </asp:DropDownList>
                                           </td>
											<td><input type="text" name="textfield" id="num13" class="input-block-level text-right" Width="150"></td>
											<td><asp:DropDownList ID="DropDownList40" Class="control" runat="server" Width="100px">
                                                <asp:ListItem Text="Select" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="1=ไม่มี" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="2=มี" Value="2"></asp:ListItem>
                                            </asp:DropDownList>
                                           </td>
											<td><asp:DropDownList ID="DropDownList39" Class="control" runat="server" Width="100px">
                                                <asp:ListItem Text="Select" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="1=ไม่มี" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="2=มี" Value="2"></asp:ListItem>
                                            </asp:DropDownList>
                                           </td>
											<td><asp:TextBox ID="TextBox109" runat="server" CssClass="input-block-level text-right" Width="150"></asp:TextBox></td>
											<td><asp:DropDownList ID="DropDownList30" Class="control" runat="server" Width="100px">
                                                <asp:ListItem Text="Select" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="0=ไม่มีโรคประจำตัว" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="1=ปกติ" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="2=ผิดปกติ" Value="3"></asp:ListItem>
                                                <asp:ListItem Text="3=ผิดปกติอื่นๆ" Value="4"></asp:ListItem>
                                            </asp:DropDownList></td>
											<td><asp:TextBox ID="TextBox44" runat="server" CssClass="input-block-level text-right" Width="150"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox45" runat="server" CssClass="input-block-level text-right" Width="80"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox46" runat="server" CssClass="input-block-level text-right" Width="80"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox47" runat="server" CssClass="input-block-level text-right" Width="80"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox48" runat="server" CssClass="input-block-level text-right" Width="80"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox49" runat="server" CssClass="input-block-level text-right" Width="80"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox50" runat="server" CssClass="input-block-level text-right" Width="80"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox51" runat="server" CssClass="input-block-level text-right" Width="80"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox52" runat="server" CssClass="input-block-level text-right" Width="80"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox53" runat="server" CssClass="input-block-level text-right" Width="80"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox54" runat="server" CssClass="input-block-level text-right" Width="80"></asp:TextBox></td>
											<td><asp:DropDownList ID="DropDownList18" Class="control" runat="server" Width="100px">
                                                <asp:ListItem Text="Select" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="0=ไม่ได้ตรวจ,error" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="1=ปกติ" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="2=ผิดปกติ" Value="3"></asp:ListItem>
                                            </asp:DropDownList></td>
											<td><asp:TextBox ID="TextBox108" runat="server" CssClass="input-block-level text-right" Width="150"></asp:TextBox></td>
											<td><asp:DropDownList ID="DropDownList41" Class="control" runat="server" Width="120px">
                                                <asp:ListItem Text="Select" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="0=ไม่มีโรคประจำตัว" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="1=ความดันโลหิตสูง" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="2=เบาหวาน" Value="3"></asp:ListItem>
                                                <asp:ListItem Text="3=โรคหัวใจและหลอดเลือด" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="4=ไขมันในเลือดสูง" Value="3"></asp:ListItem>
                                                <asp:ListItem Text="5=มีโรคประจำตัวที่กำหนดไว้ตั้งแต่ 2 โรคขึ้นไป" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="6=โรคประจำตัวอื่นๆ" Value="3"></asp:ListItem>
                                            </asp:DropDownList></td>

											<td><asp:TextBox ID="TextBox107" runat="server" CssClass="input-block-level text-right" Width="150"></asp:TextBox></td>
                                            <td><asp:DropDownList ID="DropDownList50" Class="control" runat="server" Width="120px">
                                                <asp:ListItem Text="Select" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="0=ไม่เคยสูบบุหรี่" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="2=สูบบุหรี่ เป็นครั้งคราว" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="3=สูบบุหรี่ เป็นประจำ" Value="3"></asp:ListItem>
                                            </asp:DropDownList></td>
											<td><asp:DropDownList ID="DropDownList48" Class="control" runat="server" Width="120px">
                                                <asp:ListItem Text="Select" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="0=ไม่เคยดื่ม" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="1=เคยดื่ม แต่เลิกแล้ว" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="2=ดื่ม เป็นครั้งคราว" Value="3"></asp:ListItem>
                                                <asp:ListItem Text="3=ดื่ม เป็นประจำ" Value="2"></asp:ListItem>
                                            </asp:DropDownList></td>
											<td><asp:DropDownList ID="DropDownList49" Class="control" runat="server" Width="120px">
                                                <asp:ListItem Text="Select" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="0=ไม่เคยออกกำลังกาย" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="1=ออกกำลังกาย ต่ำกว่าเกณฑ์" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="2=ออกกำลังกาย ตามเกณฑ์" Value="3"></asp:ListItem>
                                            </asp:DropDownList></td>
										</tr>
										
										<tr>
											<td><p class="text-center">2</p></td>
											<td><a href="#" class="btn" rel="tooltip" title="View"><i class="icon-search text-info"></i></a></td>
											<td><a href="#" class="btn" rel="tooltip" title="Edit"><i class="icon-edit text-success"></i></a></td>	
											<td><a href="#" class="btn" rel="tooltip" title="Delete"><i class="icon-remove text-warning"></i></a></td>
											<td><asp:TextBox ID="TextBox5" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox6" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox7" runat="server" CssClass="input-block-level text-right" Width="150"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox8" runat="server" CssClass="input-block-level text-right" Width="150"></asp:TextBox></td>
										<td>
                                            <asp:DropDownList ID="DropDownList2" Class="control" runat="server" Width="150px">
                                                <asp:ListItem Text="สังกัด" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="สังกัด 1" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="สังกัด 2" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="สังกัด 3" Value="3"></asp:ListItem>
                                                <asp:ListItem Text="สังกัด 4" Value="4"></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                        <td>
                                            <asp:DropDownList ID="DropDownList11" Class="control" runat="server" Width="150px">
                                                <asp:ListItem Text="ตำแหน่ง" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="ตำแหน่ง 1" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="ตำแหน่ง 2" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="ตำแหน่ง 3" Value="3"></asp:ListItem>
                                                <asp:ListItem Text="ตำแหน่ง 4" Value="4"></asp:ListItem>
                                            </asp:DropDownList>
                                         </td>
											<td><asp:TextBox ID="TextBox17" runat="server" CssClass="input-block-level text-right" Width="150"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox20" runat="server" CssClass="input-block-level text-right" Width="50"></asp:TextBox></td>
										 <td>
                                            <asp:DropDownList ID="DropDownList15" Class="control" runat="server" Width="80px">
                                                <asp:ListItem Text="เพศ" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="ชาย 1" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="หญิง 2" Value="2"></asp:ListItem>
                                            </asp:DropDownList>
                                         </td>
                                            <td><asp:DropDownList ID="DropDownList32" Class="control" runat="server" Width="120px">
                                                <asp:ListItem Text="ข้าราชการ" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="รัฐวิสาหกิจ" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="ปกส." Value="2"></asp:ListItem>
                                                <asp:ListItem Text="สปสช." Value="3"></asp:ListItem>
                                                <asp:ListItem Text="อื่นๆ" Value="4"></asp:ListItem>
                                            </asp:DropDownList></td>
										 <td><asp:TextBox ID="TextBox35" runat="server" CssClass="input-block-level text-right" Width="50"></asp:TextBox></td>
										 <td><asp:TextBox ID="TextBox36" runat="server" CssClass="input-block-level text-right" Width="50"></asp:TextBox></td>
										 <td><asp:TextBox ID="TextBox37" runat="server" CssClass="input-block-level text-right" Width="50"></asp:TextBox></td>
										 <td><asp:TextBox ID="TextBox38" runat="server" CssClass="input-block-level text-right" Width="50"></asp:TextBox></td>
										 <td><asp:TextBox ID="TextBox39" runat="server" CssClass="input-block-level text-right" Width="50"></asp:TextBox></td>
										 <td><asp:TextBox ID="TextBox40" runat="server" CssClass="input-block-level text-right" Width="50"></asp:TextBox></td>
										 <td><asp:DropDownList ID="DropDownList19" Class="control" runat="server" Width="100px">
                                                <asp:ListItem Text="Select" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="0=ไม่มีโรคประจำตัว" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="1=ปกติ" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="2=ผิดปกติ" Value="3"></asp:ListItem>
                                            </asp:DropDownList>
                                          </td>
											<td><asp:TextBox ID="TextBox41" runat="server" CssClass="input-block-level text-right" Width="150"></asp:TextBox></td>
											<td><asp:DropDownList ID="DropDownList35" Class="control" runat="server" Width="100px">
                                                <asp:ListItem Text="Select" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="0=ไม่ได้ตรวจ,error" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="1=ปกติ" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="2=ผิดปกติ" Value="3"></asp:ListItem>
                                            </asp:DropDownList>
                                           </td>
											<td><asp:TextBox ID="TextBox42" runat="server" CssClass="input-block-level text-right" Width="150"></asp:TextBox></td>
											<td><asp:DropDownList ID="DropDownList37" Class="control" runat="server" Width="100px">
                                                <asp:ListItem Text="Select" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="1=ไม่มี" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="2=มี" Value="2"></asp:ListItem>
                                            </asp:DropDownList>
                                           </td>
											<td><asp:DropDownList ID="DropDownList38" Class="control" runat="server" Width="100px">
                                                <asp:ListItem Text="Select" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="1=ไม่มี" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="2=มี" Value="2"></asp:ListItem>
                                            </asp:DropDownList>
                                           </td>
											<td><asp:TextBox ID="TextBox43" runat="server" CssClass="input-block-level text-right" Width="150"></asp:TextBox></td>
											<td><asp:DropDownList ID="DropDownList31" Class="control" runat="server" Width="100px">
                                                <asp:ListItem Text="Select" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="0=ไม่มีโรคประจำตัว" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="1=ปกติ" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="2=ผิดปกติ" Value="3"></asp:ListItem>
                                                <asp:ListItem Text="3=ผิดปกติอื่นๆ" Value="4"></asp:ListItem>
                                            </asp:DropDownList></td>
											<td><asp:TextBox ID="TextBox23" runat="server" CssClass="input-block-level text-right" Width="150"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox24" runat="server" CssClass="input-block-level text-right" Width="80"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox25" runat="server" CssClass="input-block-level text-right" Width="80"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox26" runat="server" CssClass="input-block-level text-right" Width="80"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox55" runat="server" CssClass="input-block-level text-right" Width="80"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox56" runat="server" CssClass="input-block-level text-right" Width="80"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox57" runat="server" CssClass="input-block-level text-right" Width="80"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox58" runat="server" CssClass="input-block-level text-right" Width="80"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox59" runat="server" CssClass="input-block-level text-right" Width="80"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox60" runat="server" CssClass="input-block-level text-right" Width="80"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox61" runat="server" CssClass="input-block-level text-right" Width="80"></asp:TextBox></td>
											<td><asp:DropDownList ID="DropDownList20" Class="control" runat="server" Width="100px">
                                                <asp:ListItem Text="Select" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="0=ไม่ได้ตรวจ,error" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="1=ปกติ" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="2=ผิดปกติ" Value="3"></asp:ListItem>
                                            </asp:DropDownList></td>
											<td><asp:TextBox ID="TextBox106" runat="server" CssClass="input-block-level text-right" Width="150"></asp:TextBox></td>
											<td><asp:DropDownList ID="DropDownList47" Class="control" runat="server" Width="120px">
                                                <asp:ListItem Text="Select" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="0=ไม่มีโรคประจำตัว" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="1=ความดันโลหิตสูง" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="2=เบาหวาน" Value="3"></asp:ListItem>
                                                <asp:ListItem Text="3=โรคหัวใจและหลอดเลือด" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="4=ไขมันในเลือดสูง" Value="3"></asp:ListItem>
                                                <asp:ListItem Text="5=มีโรคประจำตัวที่กำหนดไว้ตั้งแต่ 2 โรคขึ้นไป" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="6=โรคประจำตัวอื่นๆ" Value="3"></asp:ListItem>
                                            </asp:DropDownList></td>
											<td><asp:TextBox ID="TextBox105" runat="server" CssClass="input-block-level text-right" Width="150"></asp:TextBox></td>
                                            <td><asp:DropDownList ID="DropDownList51" Class="control" runat="server" Width="120px">
                                                <asp:ListItem Text="Select" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="0=ไม่เคยสูบบุหรี่" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="2=สูบบุหรี่ เป็นครั้งคราว" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="3=สูบบุหรี่ เป็นประจำ" Value="3"></asp:ListItem>
                                            </asp:DropDownList></td>
											<td><asp:DropDownList ID="DropDownList52" Class="control" runat="server" Width="120px">
                                                <asp:ListItem Text="Select" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="0=ไม่เคยดื่ม" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="1=เคยดื่ม แต่เลิกแล้ว" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="2=ดื่ม เป็นครั้งคราว" Value="3"></asp:ListItem>
                                                <asp:ListItem Text="3=ดื่ม เป็นประจำ" Value="2"></asp:ListItem>
                                            </asp:DropDownList></td>
											<td><asp:DropDownList ID="DropDownList53" Class="control" runat="server" Width="120px">
                                                <asp:ListItem Text="Select" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="0=ไม่เคยออกกำลังกาย" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="1=ออกกำลังกาย ต่ำกว่าเกณฑ์" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="2=ออกกำลังกาย ตามเกณฑ์" Value="3"></asp:ListItem>
                                            </asp:DropDownList></td>
										</tr>
										
										<tr>
											<td><p class="text-center">3</p></td>
											<td><a href="#" class="btn" rel="tooltip" title="View"><i class="icon-search text-info"></i></a></td>
											<td><a href="#" class="btn" rel="tooltip" title="Edit"><i class="icon-edit text-success"></i></a></td>	
											<td><a href="#" class="btn" rel="tooltip" title="Delete"><i class="icon-remove text-warning"></i></a></td>
											<td><asp:TextBox ID="TextBox9" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox10" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox11" runat="server" CssClass="input-block-level text-right" Width="150"></asp:TextBox></td>
											<td>
                                            <td><asp:DropDownList ID="DropDownList3" Class="control" runat="server" Width="150px">
                                                <asp:ListItem Text="สังกัด" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="สังกัด 1" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="สังกัด 2" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="สังกัด 3" Value="3"></asp:ListItem>
                                                <asp:ListItem Text="สังกัด 4" Value="4"></asp:ListItem>
                                            </asp:DropDownList>
                                           </td>
                                            <td>
                                            <asp:DropDownList ID="DropDownList9" Class="control" runat="server" Width="150px">
                                                <asp:ListItem Text="ตำแหน่ง" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="ตำแหน่ง 1" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="ตำแหน่ง 2" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="ตำแหน่ง 3" Value="3"></asp:ListItem>
                                                <asp:ListItem Text="ตำแหน่ง 4" Value="4"></asp:ListItem>
                                            </asp:DropDownList>
                                           </td>
											<td><asp:TextBox ID="TextBox21" runat="server" CssClass="input-block-level text-right" Width="150"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox22" runat="server" CssClass="input-block-level text-right" Width="50"></asp:TextBox></td>
											<td>
                                            <asp:DropDownList ID="DropDownList14" Class="control" runat="server" Width="80px">
                                                <asp:ListItem Text="เพศ" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="ชาย 1" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="หญิง 2" Value="2"></asp:ListItem>
                                            </asp:DropDownList>
                                           </td>
                                            <td><asp:DropDownList ID="DropDownList33" Class="control" runat="server" Width="120px">
                                                <asp:ListItem Text="ข้าราชการ" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="รัฐวิสาหกิจ" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="ปกส." Value="2"></asp:ListItem>
                                                <asp:ListItem Text="สปสช." Value="3"></asp:ListItem>
                                                <asp:ListItem Text="อื่นๆ" Value="4"></asp:ListItem>
                                            </asp:DropDownList></td>
											<td><asp:TextBox ID="TextBox98" runat="server" CssClass="input-block-level text-right" Width="50"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox99" runat="server" CssClass="input-block-level text-right" Width="50"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox100" runat="server" CssClass="input-block-level text-right" Width="50"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox101" runat="server" CssClass="input-block-level text-right" Width="50"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox102" runat="server" CssClass="input-block-level text-right" Width="50"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox103" runat="server" CssClass="input-block-level text-right" Width="50"></asp:TextBox></td>
											<td><asp:DropDownList ID="DropDownList21" Class="control" runat="server" Width="100px">
                                                <asp:ListItem Text="Select" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="0=ไม่มีโรคประจำตัว" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="1=ปกติ" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="2=ผิดปกติ" Value="3"></asp:ListItem>
                                            </asp:DropDownList></td>
											<td><asp:TextBox ID="TextBox104" runat="server" CssClass="input-block-level text-right" Width="150"></asp:TextBox></td>
							
                                            <td><asp:DropDownList ID="DropDownList25" Class="control" runat="server" Width="100px">
                                                <asp:ListItem Text="Select" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="0=ไม่ได้ตรวจ,error" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="1=ปกติ" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="2=ผิดปกติ" Value="3"></asp:ListItem>
                                            </asp:DropDownList></td>
											<td><asp:TextBox ID="TextBox87" runat="server" CssClass="input-block-level text-right" Width="150"></asp:TextBox></td>
											
											<td><asp:DropDownList ID="DropDownList26" Class="control" runat="server" Width="100px">
                                                <asp:ListItem Text="1=ไม่มี" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="2=มี" Value="2"></asp:ListItem>
                                            </asp:DropDownList></td>
											<td><asp:DropDownList ID="DropDownList27" Class="control" runat="server" Width="100px">
                                                <asp:ListItem Text="1=ไม่มี" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="2=มี" Value="2"></asp:ListItem>
                                            </asp:DropDownList></td>
											<td><asp:TextBox ID="TextBox86" runat="server" CssClass="input-block-level text-right" Width="150"></asp:TextBox></td>
											<td><asp:DropDownList ID="DropDownList28" Class="control" runat="server" Width="100px">
                                                <asp:ListItem Text="Select" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="0=ไม่มีโรคประจำตัว" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="1=ปกติ" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="2=ผิดปกติ" Value="3"></asp:ListItem>
                                                <asp:ListItem Text="3=ผิดปกติอื่นๆ" Value="4"></asp:ListItem>
                                            </asp:DropDownList></td>
											<td><asp:TextBox ID="TextBox62" runat="server" CssClass="input-block-level text-right" Width="150"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox63" runat="server" CssClass="input-block-level text-right" Width="80"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox64" runat="server" CssClass="input-block-level text-right" Width="80"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox65" runat="server" CssClass="input-block-level text-right" Width="80"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox66" runat="server" CssClass="input-block-level text-right" Width="80"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox67" runat="server" CssClass="input-block-level text-right" Width="80"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox68" runat="server" CssClass="input-block-level text-right" Width="80"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox69" runat="server" CssClass="input-block-level text-right" Width="80"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox70" runat="server" CssClass="input-block-level text-right" Width="80"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox71" runat="server" CssClass="input-block-level text-right" Width="80"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox72" runat="server" CssClass="input-block-level text-right" Width="80"></asp:TextBox></td>
											<td><asp:DropDownList ID="DropDownList22" Class="control" runat="server" Width="100px">
                                                <asp:ListItem Text="Select" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="0=ไม่ได้ตรวจ,error" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="1=ปกติ" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="2=ผิดปกติ" Value="3"></asp:ListItem>
                                            </asp:DropDownList></td>
											<td><asp:TextBox ID="TextBox85" runat="server" CssClass="input-block-level text-right" Width="150"></asp:TextBox></td>
											
                                            <td><asp:DropDownList ID="DropDownList46" Class="control" runat="server" Width="120px">
                                                <asp:ListItem Text="Select" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="0=ไม่มีโรคประจำตัว" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="1=ความดันโลหิตสูง" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="2=เบาหวาน" Value="3"></asp:ListItem>
                                                <asp:ListItem Text="3=โรคหัวใจและหลอดเลือด" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="4=ไขมันในเลือดสูง" Value="3"></asp:ListItem>
                                                <asp:ListItem Text="5=มีโรคประจำตัวที่กำหนดไว้ตั้งแต่ 2 โรคขึ้นไป" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="6=โรคประจำตัวอื่นๆ" Value="3"></asp:ListItem>
                                            </asp:DropDownList></td>
											<td><asp:TextBox ID="TextBox84" runat="server" CssClass="input-block-level text-right" Width="150"></asp:TextBox></td>
                                            <td><asp:DropDownList ID="DropDownList54" Class="control" runat="server" Width="120px">
                                                <asp:ListItem Text="Select" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="0=ไม่เคยสูบบุหรี่" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="2=สูบบุหรี่ เป็นครั้งคราว" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="3=สูบบุหรี่ เป็นประจำ" Value="3"></asp:ListItem>
                                            </asp:DropDownList></td>
											<td><asp:DropDownList ID="DropDownList55" Class="control" runat="server" Width="120px">
                                                <asp:ListItem Text="Select" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="0=ไม่เคยดื่ม" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="1=เคยดื่ม แต่เลิกแล้ว" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="2=ดื่ม เป็นครั้งคราว" Value="3"></asp:ListItem>
                                                <asp:ListItem Text="3=ดื่ม เป็นประจำ" Value="2"></asp:ListItem>
                                            </asp:DropDownList></td>
											<td><asp:DropDownList ID="DropDownList56" Class="control" runat="server" Width="120px">
                                                <asp:ListItem Text="Select" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="0=ไม่เคยออกกำลังกาย" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="1=ออกกำลังกาย ต่ำกว่าเกณฑ์" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="2=ออกกำลังกาย ตามเกณฑ์" Value="3"></asp:ListItem>
                                            </asp:DropDownList></td>
										</tr>
										
										<tr>
											<td><p class="text-center">4</p></td>
											<td><a href="#" class="btn" rel="tooltip" title="View"><i class="icon-search text-info"></i></a></td>
											<td><a href="#" class="btn" rel="tooltip" title="Edit"><i class="icon-edit text-success"></i></a></td>	
											<td><a href="#" class="btn" rel="tooltip" title="Delete"><i class="icon-remove text-warning"></i></a></td>
											<td><asp:TextBox ID="TextBox13" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox14" runat="server" CssClass="input-block-level text-right" Width="100"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox15" runat="server" CssClass="input-block-level text-right" Width="150"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox16" runat="server" CssClass="input-block-level text-right" Width="150"></asp:TextBox></td>
											<td>
                                            <asp:DropDownList ID="DropDownList4" Class="control" runat="server" Width="150px">
                                                <asp:ListItem Text="สังกัด" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="สังกัด 1" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="สังกัด 2" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="สังกัด 3" Value="3"></asp:ListItem>
                                                <asp:ListItem Text="สังกัด 4" Value="4"></asp:ListItem>
                                            </asp:DropDownList>
                                           </td>
                                            <td>
                                            <asp:DropDownList ID="DropDownList10" Class="control" runat="server" Width="150px">
                                                <asp:ListItem Text="ตำแหน่ง" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="ตำแหน่ง 1" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="ตำแหน่ง 2" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="ตำแหน่ง 3" Value="3"></asp:ListItem>
                                                <asp:ListItem Text="ตำแหน่ง 4" Value="4"></asp:ListItem>
                                            </asp:DropDownList>
                                           </td>
                                            
											<td><asp:TextBox ID="TextBox18" runat="server" CssClass="input-block-level text-right" Width="150"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox19" runat="server" CssClass="input-block-level text-right" Width="50"></asp:TextBox></td>
											<td>
                                            <asp:DropDownList ID="DropDownList13" Class="control" runat="server" Width="80px">
                                                <asp:ListItem Text="เพศ" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="ชาย 1" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="หญิง 2" Value="2"></asp:ListItem>
                                            </asp:DropDownList>
                                           </td>
                                            <td><asp:DropDownList ID="DropDownList34" Class="control" runat="server" Width="120px">
                                                <asp:ListItem Text="ข้าราชการ" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="รัฐวิสาหกิจ" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="ปกส." Value="2"></asp:ListItem>
                                                <asp:ListItem Text="สปสช." Value="3"></asp:ListItem>
                                                <asp:ListItem Text="อื่นๆ" Value="4"></asp:ListItem>
                                            </asp:DropDownList></td>
											<td><asp:TextBox ID="TextBox97" runat="server" CssClass="input-block-level text-right" Width="50"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox96" runat="server" CssClass="input-block-level text-right" Width="50"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox95" runat="server" CssClass="input-block-level text-right" Width="50"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox94" runat="server" CssClass="input-block-level text-right" Width="50"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox93" runat="server" CssClass="input-block-level text-right" Width="50"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox92" runat="server" CssClass="input-block-level text-right" Width="50"></asp:TextBox></td>
											
                                            <td><asp:DropDownList ID="DropDownList23" Class="control" runat="server" Width="100px">
                                                <asp:ListItem Text="Select" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="0=ไม่มีโรคประจำตัว" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="1=ปกติ" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="2=ผิดปกติ" Value="3"></asp:ListItem>
                                            </asp:DropDownList></td>
											<td><asp:TextBox ID="TextBox91" runat="server" CssClass="input-block-level text-right" Width="150"></asp:TextBox></td>
											<td><asp:DropDownList ID="DropDownList24" Class="control" runat="server" Width="100px">
                                                <asp:ListItem Text="Select" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="0=ไม่ได้ตรวจ,error" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="1=ปกติ" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="2=ผิดปกติ" Value="3"></asp:ListItem>
                                            </asp:DropDownList></td>
											<td><asp:TextBox ID="TextBox90" runat="server" CssClass="input-block-level text-right" Width="150"></asp:TextBox></td>
											<td><asp:DropDownList ID="DropDownList42" Class="control" runat="server" Width="100px">
                                                <asp:ListItem Text="1=ไม่มี" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="2=มี" Value="2"></asp:ListItem>
                                            </asp:DropDownList></td>
											<td><asp:DropDownList ID="DropDownList43" Class="control" runat="server" Width="100px">
                                                <asp:ListItem Text="1=ไม่มี" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="2=มี" Value="2"></asp:ListItem>
                                            </asp:DropDownList></td>
											<td><asp:TextBox ID="TextBox89" runat="server" CssClass="input-block-level text-right" Width="150"></asp:TextBox></td>
											<td><asp:DropDownList ID="DropDownList29" Class="control" runat="server" Width="100px">
                                                <asp:ListItem Text="Select" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="0=ไม่มีโรคประจำตัว" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="1=ปกติ" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="2=ผิดปกติ" Value="3"></asp:ListItem>
                                                <asp:ListItem Text="3=ผิดปกติอื่นๆ" Value="4"></asp:ListItem>
                                            </asp:DropDownList></td>
											<td><asp:TextBox ID="TextBox73" runat="server" CssClass="input-block-level text-right" Width="150"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox74" runat="server" CssClass="input-block-level text-right" Width="80"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox75" runat="server" CssClass="input-block-level text-right" Width="80"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox76" runat="server" CssClass="input-block-level text-right" Width="80"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox77" runat="server" CssClass="input-block-level text-right" Width="80"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox78" runat="server" CssClass="input-block-level text-right" Width="80"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox79" runat="server" CssClass="input-block-level text-right" Width="80"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox80" runat="server" CssClass="input-block-level text-right" Width="80"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox81" runat="server" CssClass="input-block-level text-right" Width="80"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox82" runat="server" CssClass="input-block-level text-right" Width="80"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox12" runat="server" CssClass="input-block-level text-right" Width="80"></asp:TextBox></td>
											<td><asp:DropDownList ID="DropDownList44" Class="control" runat="server" Width="100px">
                                                <asp:ListItem Text="Select" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="0=ไม่ได้ตรวจ,error" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="1=ปกติ" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="2=ผิดปกติ" Value="3"></asp:ListItem>
                                            </asp:DropDownList></td>
											<td><asp:TextBox ID="TextBox88" runat="server" CssClass="input-block-level text-right" Width="150"></asp:TextBox></td>
											
                                            <td><asp:DropDownList ID="DropDownList45" Class="control" runat="server" Width="120px">
                                                <asp:ListItem Text="Select" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="0=ไม่มีโรคประจำตัว" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="1=ความดันโลหิตสูง" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="2=เบาหวาน" Value="3"></asp:ListItem>
                                                <asp:ListItem Text="3=โรคหัวใจและหลอดเลือด" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="4=ไขมันในเลือดสูง" Value="3"></asp:ListItem>
                                                <asp:ListItem Text="5=มีโรคประจำตัวที่กำหนดไว้ตั้งแต่ 2 โรคขึ้นไป" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="6=โรคประจำตัวอื่นๆ" Value="3"></asp:ListItem>
                                            </asp:DropDownList></td>
											<td><asp:TextBox ID="TextBox83" runat="server" CssClass="input-block-level text-right" Width="150"></asp:TextBox></td>
                                            <td><asp:DropDownList ID="DropDownList57" Class="control" runat="server" Width="120px">
                                                <asp:ListItem Text="Select" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="0=ไม่เคยสูบบุหรี่" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="2=สูบบุหรี่ เป็นครั้งคราว" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="3=สูบบุหรี่ เป็นประจำ" Value="3"></asp:ListItem>
                                            </asp:DropDownList></td>
											<td><asp:DropDownList ID="DropDownList58" Class="control" runat="server" Width="120px">
                                                <asp:ListItem Text="Select" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="0=ไม่เคยดื่ม" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="1=เคยดื่ม แต่เลิกแล้ว" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="2=ดื่ม เป็นครั้งคราว" Value="3"></asp:ListItem>
                                                <asp:ListItem Text="3=ดื่ม เป็นประจำ" Value="2"></asp:ListItem>
                                            </asp:DropDownList></td>
											<td><asp:DropDownList ID="DropDownList59" Class="control" runat="server" Width="120px">
                                                <asp:ListItem Text="Select" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="0=ไม่เคยออกกำลังกาย" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="1=ออกกำลังกาย ต่ำกว่าเกณฑ์" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="2=ออกกำลังกาย ตามเกณฑ์" Value="3"></asp:ListItem>
                                            </asp:DropDownList></td>
										</tr>
                                       
									</tbody>
								</table>
							</div>
                                <div class="span43">
									        <div class="form-actions pull-right">
										        <button type="submit" class="btn btn-green"><i class="icon-ok"></i> บันทึกข้อมูล</button>
										        <button type="submit" class="btn btn-green"><i class="icon-ok"></i> ยืนยันและส่งข้อมูล</button>
										        <button type="button" class="btn btn-lightred"><i class="icon-remove"></i> ยกเลิก</button>
									        </div>
									</div>
						</div>
				   	  </div>
                   </div>
				
			</div>
		</div></div>
</asp:Content>










