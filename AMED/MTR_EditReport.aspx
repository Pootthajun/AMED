﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="MTR_EditReport.aspx.cs" Inherits="AMED.MTR_EditReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <title>แก้ไขข้อมูลรายงาน - กรมการแพทย์ทหารบก</title>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <div class="container-fluid" id="content">
		<div id="left">
			<div class="subnav">
				<div class="subnav-title">
					<a href="#" class='toggle-subnav'><i class="icon-angle-down"></i><span><p class="icon-building"></p> หน่วยสายแพทย์</span></a>
				</div>
				<ul class="subnav-menu">
					<li>
						<a href="Default.aspx">สรุปภาพรวม</a>
					</li>
					<li>
							<a href="Hotpital.aspx">การบันทึกข้อมูล</a>
							
						</li>
					<li>
						<a href="Report.aspx">สรุปรายงาน</a>
					</li>
				</ul>
			</div>
			<div class="subnav">
				<div class="subnav-title">
					<a href="#" class='toggle-subnav'><i class="icon-angle-down"></i><span><p class="icon-building"></p>  ส่วนกลาง</span></a>
				</div>
				<ul class="subnav-menu">
					<li>
						<a href="Default.aspx">สรุปภาพรวม</a>
					</li>
					<li>
							<a href="Department.aspx">การตรวจสอบการบันทึกข้อมูล</a>
							
					</li>
					<li>
						<a href="Report.aspx">สรุปรายงาน</a>
					</li>
				</ul>
			</div>
            <div class="subnav">
				<div class="subnav-title">
					<a href="#" class='toggle-subnav'><i class="icon-angle-down"></i><span><p class="icon-cog"></p> ข้อมูลพื้นฐาน</span></a>
				</div>
				<ul class="subnav-menu">
					<li class='dropdown'>
						<a href="#" data-toggle="dropdown"> ข้อมุลทั่วไป</a>
						<ul class="dropdown-menu">
							<li>
								<a href="MT_Region.aspx"> กองทัพภาค</a>
							</li>
							<li>
								<a href="MT_Hospital.aspx"> หน่วยสายแพทย์</a>
							</li>
							<li>
								<a href="MT_Department.aspx"> หน่วยงาน (ส่วนกลาง)</a>
							</li>
							<li>
								<a href="MT_AddOrganization.aspx"> สังกัด</a>
							</li>
							<li>
								<a href="MT_Province.aspx"> จังหวัด</a>
							</li>
						</ul>
					</li>
					<li class='dropdown'>
						<a href="#" data-toggle="dropdown">กองวิทยาการ</a>
						<ul class="dropdown-menu">
							<li>
								<a href="MTS_ICD10.aspx"> รหัสโรค (ICD10)</a>
							</li>
							<li>
								<a href="MTS_GroupRule.aspx"> กลุ่มโรคที่ขัดต่อกฎกระทรวง</a>
							</li>
							<li>
								<a href="MTS_Rule.aspx"> โรคที่ขัดต่อกฎกระทรวง</a>
							</li>
							<li>
								<a href="MTS_Sizebed.aspx"> ขนาดเตียง</a>
							</li>
							<li>
								<a href="MTS_ProHotpital.aspx"> ตำแหน่งในโรงพยาบาล</a>
							</li>
							<li>
								<a href="MTS_Wards.aspx"> หอผู้ป่วย</a>
							</li>
						</ul>
					</li>
                    <li class='dropdown'>
						<a href="#" data-toggle="dropdown">กองงบประมาณการเงิน</a>
						<ul class="dropdown-menu">
							<li>
								<a href="MTF_Assets.aspx">สินทรัพย์และหนี้สิน</a>
							</li>
							<li>
								<a href="MTF_Progress.aspx">ผลการดำเนินงาน</a>
							</li>
							<li>
								<a href="MTF_Reception.aspx">การรับ-การจ่าย</a>
							</li>
							<li>
								<a href="MTF_Deposit.aspx">เงินฝาก</a>
							</li>
						</ul>
					</li>
                    <li class='dropdown'>
						<a href="#" data-toggle="dropdown">กองส่งกำลังบำรุง</a>
						<ul class="dropdown-menu">
							<li>
								<a href="MTT_TypeDrug.aspx"> ประเภทยา</a>
							</li>
							<li>
								<a href="MTT_Drug.aspx"> ยา</a>
							</li>
							<li>
								<a href="MTT_DrugNature.aspx">ลักษณะยา</a>
							</li>
							<li>
								<a href="MTT_MedicalSupplies.aspx">เวชภัณฑ์</a>
							</li>
						</ul>
					</li>
                    <li class='dropdown'>
						<a href="#" data-toggle="dropdown">ผู้ใช้งาน (User)</a>
						<ul class="dropdown-menu">
							<li>
								<a href="MT_PersonType.aspx"> ประเภทบุคคล</a>
							</li>
							<li>
								<a href="MT_Person.aspx">บุคคลย่อย</a>
							</li>
							<li>
								<a href="#">กำหนดสิทธิ์</a>
							</li>
							<li>
								<a href="MT_User.aspx">ผู้ใช้งานในระบบ</a>
							</li>
						</ul>
					</li>
                     <li class='dropdown active'>
						<a href="#" data-toggle="dropdown">การส่งรายงาน</a>
						<ul class="dropdown-menu">
							<li>
								<a href="MTR_DeadlineReports.aspx"> กำหนดส่งรายงาน</a>
							</li>
							<li class="active">
								<a href="MTR_EditReport.aspx"> แก้ไขข้อมูลรายงาน</a>
							</li>
							<li>
								<a href="MTR_NameReport.aspx"> ชื่อแบบฟอร์มรายงาน</a>
							</li>
						</ul>
					</li>
				</ul>
			</div>
		</div>
        
		<div id="main">
			<div class="container-fluid">
				<div class="page-header">
					<div class="pull-left">
						<h4>ชื่อแบบฟอร์มรายงาน</h4>
					</div>
					
				</div>
				<div class="breadcrumbs">
					<ul>
						<li>
							<a href="Default.aspx">Dashborad</a>
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<a href="forms-basic.html">ตั้งค่า</a>
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<a href="forms-wizard.html">ชื่อแบบฟอร์มรายงาน</a>
						</li>
					</ul>
					<div class="close-bread">
						<a href="#"><i class="icon-remove"></i></a>
					</div>
				</div>
				

				<div class="row-fluid">

						<div class="span12"><br />
                            <a href="MT_AddRegion.aspx" class="btn btn-darkblue"><i class="icon-plus"></i> เพิ่ม แก้ไขข้อมูลรายงาน</a>
							<div class="box box-color box-small box-bordered">
								<div class="box-title">
									<h5 style="color:white;">
									    <i class="icon-file"></i>
									    รายละเอียด
								    </h5>
									
								</div>
								<div class="box-content nopadding">
								<table class="table table-hover table-nomargin">
									<thead>
										<tr>
											<th style="width: 50px">ลำดับ</th>
											<th style="width: 550px">แก้ไขข้อมูลรายงาน</th>
											<th style="width: 50px">ดู</th>
											<th style="width: 50px">แก้ไข</th>
											<th style="width: 50px">ลบ</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>1</td>
											<td>
												Internet
												Explorer 4.0
											</td>
											<td><a href="#" class="btn" rel="tooltip" title="View"><i class="icon-search"></i></a></td>
											<td><a href="#" class="btn" rel="tooltip" title="Edit"><i class="icon-edit"></i></a></td>	
											<td><a href="#" class="btn" rel="tooltip" title="Delete"><i class="icon-remove"></i></a></td>
										</tr>
										<tr>
											<td>2</td>
											<td>Nokia N800</td>
											<td><a href="#" class="btn" rel="tooltip" title="View"><i class="icon-search"></i></a></td>
											<td><a href="#" class="btn" rel="tooltip" title="Edit"><i class="icon-edit"></i></a></td>	
											<td><a href="#" class="btn" rel="tooltip" title="Delete"><i class="icon-remove"></i></a></td>
										</tr>
										<tr>
											<td>3</td>
											<td>NetFront 3.4</td>
											<td><a href="#" class="btn" rel="tooltip" title="View"><i class="icon-search"></i></a></td>
											<td><a href="#" class="btn" rel="tooltip" title="Edit"><i class="icon-edit"></i></a></td>	
											<td><a href="#" class="btn" rel="tooltip" title="Delete"><i class="icon-remove"></i></a></td>
										</tr>
										<tr>
											<td>4</td>
											<td>Dillo 0.8</td>
											<td><a href="#" class="btn" rel="tooltip" title="View"><i class="icon-search"></i></a></td>
											<td><a href="#" class="btn" rel="tooltip" title="Edit"><i class="icon-edit"></i></a></td>	
											<td><a href="#" class="btn" rel="tooltip" title="Delete"><i class="icon-remove"></i></a></td>
										</tr>
										<tr>
											<td>5</td>
											<td>Links</td>
											<td><a href="#" class="btn" rel="tooltip" title="View"><i class="icon-search"></i></a></td>
											<td><a href="#" class="btn" rel="tooltip" title="Edit"><i class="icon-edit"></i></a></td>	
											<td><a href="#" class="btn" rel="tooltip" title="Delete"><i class="icon-remove"></i></a></td>
										</tr>
										<tr>
											<td>6</td>
											<td>Lynx</td>
											<td><a href="#" class="btn" rel="tooltip" title="View"><i class="icon-search"></i></a></td>
											<td><a href="#" class="btn" rel="tooltip" title="Edit"><i class="icon-edit"></i></a></td>	
											<td><a href="#" class="btn" rel="tooltip" title="Delete"><i class="icon-remove"></i></a></td>
										</tr>
									</tbody>
								</table>
								<div class="table-pagination">
									<a href="#" class='disabled'>Previous</a>
									<span>
										<a href="#" class='active'>1</a>
										<a href="#">2</a>
										<a href="#">3</a>
									</span>
									<a href="#">Next</a>
								</div>
							</div>
							</div>
						</div>
						

					</div>
				
			</div>
		</div>

    </div>
</asp:Content>







