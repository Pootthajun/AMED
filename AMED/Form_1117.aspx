﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="Form_1117.aspx.cs" Inherits="AMED.Form_1117" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <title>รง.ผสต.17 - กรมการแพทย์ทหารบก</title>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <div class="container-fluid" id="content">
		<div id="left">
			<div class="subnav">
				<div class="subnav-title">
					<a href="#" class='toggle-subnav'><i class="icon-angle-down"></i><span><p class="icon-building"></p> หน่วยสายแพทย์</span></a>
				</div>
				<ul class="subnav-menu">
					<li>
						<a href="Default.aspx">สรุปภาพรวม</a>
					</li>
					<li class="active">
							<a href="Hotpital.aspx">การบันทึกข้อมูล</a>
							
						</li>
					<li>
						<a href="Report.aspx">สรุปรายงาน</a>
					</li>
				</ul>
			</div>
			<div class="subnav">
				<div class="subnav-title">
					<a href="#" class='toggle-subnav'><i class="icon-angle-down"></i><span><p class="icon-building"></p>  ส่วนกลาง</span></a>
				</div>
				<ul class="subnav-menu">
					<li>
						<a href="Default.aspx">สรุปภาพรวม</a>
					</li>
					<li>
							<a href="Department.aspx">การตรวจสอบการบันทึกข้อมูล</a>
							
					</li>
					<li>
						<a href="Report.aspx">สรุปรายงาน</a>
					</li>
				</ul>
			</div>
            <div class="subnav">
				<div class="subnav-title">
					<a href="#" class='toggle-subnav'><i class="icon-angle-down"></i><span><p class="icon-cog"></p> ข้อมูลพื้นฐาน</span></a>
				</div>
				<ul class="subnav-menu">
					<li class='dropdown'>
						<a href="#" data-toggle="dropdown"> ข้อมุลทั่วไป</a>
						<ul class="dropdown-menu">
							<li>
								<a href="MT_Region.aspx"> กองทัพภาค</a>
							</li>
							<li>
								<a href="MT_Hospital.aspx"> หน่วยสายแพทย์</a>
							</li>
							<li>
								<a href="MT_Department.aspx"> หน่วยงาน (ส่วนกลาง)</a>
							</li>
							<li>
								<a href="MT_AddOrganization.aspx"> สังกัด</a>
							</li>
							<li>
								<a href="MT_Province.aspx"> จังหวัด</a>
							</li>
						</ul>
					</li>
					<li class='dropdown'>
						<a href="#" data-toggle="dropdown">กองวิทยาการ</a>
						<ul class="dropdown-menu">
							<li>
								<a href="MTS_ICD10.aspx"> รหัสโรค (ICD10)</a>
							</li>
							<li>
								<a href="MTS_GroupRule.aspx"> กลุ่มโรคที่ขัดต่อกฎกระทรวง</a>
							</li>
							<li>
								<a href="MTS_Rule.aspx"> โรคที่ขัดต่อกฎกระทรวง</a>
							</li>
							<li>
								<a href="MTS_Sizebed.aspx"> ขนาดเตียง</a>
							</li>
							<li>
								<a href="MTS_ProHotpital.aspx"> ตำแหน่งในโรงพยาบาล</a>
							</li>
							<li>
								<a href="MTS_Wards.aspx"> หอผู้ป่วย</a>
							</li>
						</ul>
					</li>
                    <li class='dropdown'>
						<a href="#" data-toggle="dropdown">กองงบประมาณการเงิน</a>
						<ul class="dropdown-menu">
							<li>
								<a href="MTF_Assets.aspx">สินทรัพย์และหนี้สิน</a>
							</li>
							<li>
								<a href="MTF_Progress.aspx">ผลการดำเนินงาน</a>
							</li>
							<li>
								<a href="MTF_Reception.aspx">การรับ-การจ่าย</a>
							</li>
							<li>
								<a href="MTF_Deposit.aspx">เงินฝาก</a>
							</li>
						</ul>
					</li>
                    <li class='dropdown'>
						<a href="#" data-toggle="dropdown">กองส่งกำลังบำรุง</a>
						<ul class="dropdown-menu">
							<li>
								<a href="MTT_TypeDrug.aspx"> ประเภทยา</a>
							</li>
							<li>
								<a href="MTT_Drug.aspx"> ยา</a>
							</li>
							<li>
								<a href="MTT_DrugNature.aspx">ลักษณะยา</a>
							</li>
							<li>
								<a href="MTT_MedicalSupplies.aspx">เวชภัณฑ์</a>
							</li>
						</ul>
					</li>
                    <li class='dropdown'>
						<a href="#" data-toggle="dropdown">ผู้ใช้งาน (User)</a>
						<ul class="dropdown-menu">
							<li>
								<a href="MT_PersonType.aspx"> ประเภทบุคคล</a>
							</li>
							<li>
								<a href="MT_Person.aspx">บุคคลย่อย</a>
							</li>
							<li>
								<a href="#">กำหนดสิทธิ์</a>
							</li>
							<li>
								<a href="MT_User.aspx">ผู้ใช้งานในระบบ</a>
							</li>
						</ul>
					</li>
                     <li class='dropdown'>
						<a href="#" data-toggle="dropdown">การส่งรายงาน</a>
						<ul class="dropdown-menu">
							<li>
								<a href="MTR_DeadlineReports.aspx"> กำหนดส่งรายงาน</a>
							</li>
							<li>
								<a href="MTR_EditReport.aspx"> แก้ไขข้อมูลรายงาน</a>
							</li>
							<li>
								<a href="MTR_NameReport.aspx"> ชื่อแบบฟอร์มรายงาน</a>
							</li>
						</ul>
					</li>
				</ul>
			</div>
		</div>
		<div id="main">
			<div class="container-fluid">
				<div class="page-header">
					<div class="pull-left">
						<h4>รายงานจำนวนผู้ป่วยนอก จำนวนผู้ป่วยรับมา และจำนวนผุ้ป่วยส่งต่อไปที่อื่น ( รง.ผสต.17 )</h4>
					</div>
					
				</div>
				<div class="breadcrumbs">
					<ul>
						<li>
							<a href="Default.aspx">Dashborad</a>
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<a href="forms-basic.html">กองวิทยาการ</a>
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<a href="MT_User.aspx">รง.ผสต.17</a>
						</li>
					</ul>
					<div class="close-bread">
						<a href="#"><i class="icon-remove"></i></a>
					</div>
				</div>
				

				<div class="row-fluid">

						<div class="span12">
						  <div class="box">
							<div class="box-title">
                                <div class='basic-margin'>
								<div class="fileupload fileupload-new" data-provides="fileupload">
									<div class="input-append">
										<div class="uneditable-input span3"><i class="icon-file fileupload-exists"></i> <span class="fileupload-preview"></span></div><span class="btn btn-file"><span class="fileupload-new">Select file</span><span class="fileupload-exists">Change</span><input type="file" name="aaaa" /></span><a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
								    	</div>
								   <div class="btn-group span2 pull-right">
                                  <%-- <button class="btn btn-info" rel="tooltip" title="edit"><i class="icon-edit"></i> แก้ไขเอกสาร</button>--%>
												<a href="#" data-toggle="dropdown" class="btn btn-warning dropdown-toggle" rel="tooltip" title="print"><i class="icon-print"></i> พิมพ์รายงาน <span class="caret"></span></a>
												<ul class="dropdown-menu dropdown-warning">
													<li>
														<a href="#"><i class="icon-file"></i> Excel</a>
													</li>
													<li>
														<a href="#"><i class="icon-file"></i> PDF</a>
													</li>
												</ul>
											</div></div>
                                </div>
							</div>
                            <br />
                            <br />

                           <div style="overflow-x:auto;" class="box-content nopadding"><br />
                               <form action="#" method="get" class='form-vertical'>
                                    <div class="span2">
											<div class="control-group">
												<label for="textfield" class="control-label">กองทัพ</label>
                                                <asp:DropDownList ID="ddlทั้งหมด" runat="server" Width="150px">
                                                <asp:ListItem Text="หน่วยงาน (ส่วนกลาง)" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="กองทัพภาคที่ 2" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="กองทัพภาคที่ 3" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="กองทัพภาคที่ 4" Value="3"></asp:ListItem>
                                            </asp:DropDownList>
											</div>
                                            
										</div>
										<div class="span3">
                                            <div class="control-group">
												<label for="textfield" class="control-label">หน่วยสายแพทย์</label>
                                                <asp:DropDownList ID="DropDownList2" runat="server" Width="230px">
                                                <asp:ListItem Text="ทั้งหมด" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="รพ.พระมงกุฎเกล้า" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="รพ.อานันทมหิดล" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="รพ.ค่ายสุรนารี" Value="3"></asp:ListItem>
                                            </asp:DropDownList>
											</div>
											
										</div>
                                          <div class="span2">
										    <div class="control-group">
												    <label for="textfield" class="control-label">ปีงบประมาณ</label>
                                                    <asp:DropDownList ID="DropDownList4" runat="server" Width="150px">
                                                    <asp:ListItem Text="ทั้งหมด" Value="0"></asp:ListItem>
                                                    <asp:ListItem Text="2560" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="2559" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="2558" Value="3"></asp:ListItem>
                                                    <asp:ListItem Text="2557" Value="4"></asp:ListItem>
                                                </asp:DropDownList>
											    </div>
                                              </div>
										<div class="span3">
                                            <div class="control-group">
												<label for="textfield" class="control-label">ประจำเดือน</label>
                                                <asp:DropDownList ID="DropDownList5" runat="server" Width="230px">
                                                <asp:ListItem Text="ทั้งหมด" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="มกราคม" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="กุมภาพันธ์" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="มีนาคม" Value="3"></asp:ListItem>
                                                <asp:ListItem Text="เมษายน" Value="4"></asp:ListItem>
                                                <asp:ListItem Text="พฤษภาคม" Value="5"></asp:ListItem>
                                                <asp:ListItem Text="กรกฎาคม" Value="6"></asp:ListItem>
                                                <asp:ListItem Text="สิงหาคม" Value="7"></asp:ListItem>
                                                <asp:ListItem Text="กันยายน" Value="8"></asp:ListItem>
                                                <asp:ListItem Text="ตุลาคม" Value="9"></asp:ListItem>
                                                <asp:ListItem Text="พฤศจิกายน" Value="10"></asp:ListItem>
                                                <asp:ListItem Text="ธันวาคม" Value="11"></asp:ListItem>
                                            </asp:DropDownList>
											</div>
										</div>
									
								</form>
								 <table class="table table-hover table-nomargin table-bordered dataTable dataTable-noheader  dataTable-fixedcolumn dataTable-scroll-x dataTable-scroll-y table-edit">
									<thead>
										<tr>
											<th style="width: 20px"><p class="text-center">ลำดับ</p></th>
											<th style="width: 150px"><p class="text-center">แผนกให้บริการ</p></th>
											<th style="width: 50px"><p class="text-center">จำนวนผู้ป่วยนอก</p></th>
											<th style="width: 50px"><p class="text-center">จำนวนผู้ป่วยรับมาจากที่อื่น</p></th>
											<th style="width: 50px"><p class="text-center">จำนวนผู้ป่วยส่งต่อไปที่อื่น</p></th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td><p class="text-center">01</p></td>
											<td>เวชศาสตร์ครอบครัว</td>
											
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num12" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num13" placeholder="0" class="input-block-level text-right"></td>
										</tr>
										
										<tr>
											<td><p class="text-center">02</p></td>
											<td>ตรวจร่างกาย - เพื่อออกใบรับรองแพทย์</td>
											
											<td><input type="text" name="textfield" id="num21" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num22" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num23" placeholder="0" class="input-block-level text-right"></td>
										</tr>
										
										<tr>
											<td><p class="text-center">03</p></td>
											<td>ตรวจร่างกาย - เพื่อไปต่างประเทศ / ตรวจร่างกายเป็นหมู่คณะ</td>
											
											<td><input type="text" name="textfield" id="num31" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num32" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num33" placeholder="0" class="input-block-level text-right"></td>
										</tr>
										
										<tr>
											<td><p class="text-center">04</p></td>
											<td>ตรวจร่างกาย - ด้านเวชศาสตร์การบิน</td>
											
											<td><input type="text" name="textfield" id="num41" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num42" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num43" placeholder="0" class="input-block-level text-right"></td>
										</tr>
										
										<tr>
											<td><p class="text-center">05</p></td>
											<td>ตรวจร่างกาย - ทหารใหม่</td>
											
											<td><input type="text" name="textfield" id="num51" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num52" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num53" placeholder="0" class="input-block-level text-right"></td>
										</tr>
										
										<tr>
											<td><p class="text-center">06</p></td>
											<td>บริการปฐมภูมิ (PCU)</td>
											
											<td><input type="text" name="textfield" id="num61" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num62" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num63" placeholder="0" class="input-block-level text-right"></td>
										</tr>
										
										<tr>
											<td><p class="text-center">07</p></td>
											<td>อายุรกรรม - ทั่วไป</td>
											
											<td><input type="text" name="textfield" id="num71" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num72" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num73" placeholder="0" class="input-block-level text-right"></td>
										</tr>
										
										<tr>
											<td><p class="text-center">08</p></td>
											<td>อายุรกรรม - หัวใจ</td>
											
											<td><input type="text" name="textfield" id="num81" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num82" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num83" placeholder="0" class="input-block-level text-right"></td>
										</tr>
										
										<tr>
											<td><p class="text-center">09</p></td>
											<td>อายุรกรรม - แผนกสวนหัวใจ</td>
											
											<td><input type="text" name="textfield" id="num91" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num92" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num93" placeholder="0" class="input-block-level text-right"></td>
										</tr>
										
										<tr>
											<td><p class="text-center">10</p></td>
											<td>อายุรกรรม - เบาหวาน / ต่อมไร้ท่อ</td>
											
											<td><input type="text" name="textfield" id="num101" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num102" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num103" placeholder="0" class="input-block-level text-right"></td>
										</tr>

										<tr>
											<td><p class="text-center">11</p></td>
											<td>อายุรกรรม - ไทรอยด์</td>
											
											<td><input type="text" name="textfield" id="num111" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num112" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num113" placeholder="0" class="input-block-level text-right"></td>
										</tr>

										<tr>
											<td><p class="text-center">12</p></td>
											<td>อายุรกรรม - มะเร็งวิทยา</td>
											
											<td><input type="text" name="textfield" id="num121" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num122" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num123" placeholder="0" class="input-block-level text-right"></td>
										</tr>
                                        
										<tr>
											<td><p class="text-center">13</p></td>
											<td>อายุรกรรม - เคมีบำบัด</td>
											
											<td><input type="text" name="textfield" id="num101" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num102" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num103" placeholder="0" class="input-block-level text-right"></td>
										</tr>

										<tr>
											<td><p class="text-center">14</p></td>
											<td>อายุรกรรม - รูมาติสสัม</td>
											
											<td><input type="text" name="textfield" id="num111" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num112" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num113" placeholder="0" class="input-block-level text-right"></td>
										</tr>

										<tr>
											<td><p class="text-center">15</p></td>
											<td>อายุรกรรม - โรคไต</td>
											
											<td><input type="text" name="textfield" id="num121" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num122" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num123" placeholder="0" class="input-block-level text-right"></td>
										</tr>

										<tr>
											<td><p class="text-center">16</p></td>
											<td>อายุรกรรม - ไตเทียม / CAPD / ปลูกถ่ายไต</td>
											
											<td><input type="text" name="textfield" id="num101" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num102" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num103" placeholder="0" class="input-block-level text-right"></td>
										</tr>

										<tr>
											<td><p class="text-center">17</p></td>
											<td>อายุรกรรม - ผิวหนัง</td>
											
											<td><input type="text" name="textfield" id="num111" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num112" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num113" placeholder="0" class="input-block-level text-right"></td>
										</tr>

										<tr>
											<td><p class="text-center">18</p></td>
											<td>อายุรกรรม - โรคปอด</td>
											
											<td><input type="text" name="textfield" id="num121" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num122" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num123" placeholder="0" class="input-block-level text-right"></td>
										</tr>

										<tr>
											<td><p class="text-center">19</p></td>
											<td>อายุรกรรมโรคปอด - ภูมิแพ้</td>
											
											<td><input type="text" name="textfield" id="num111" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num112" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num113" placeholder="0" class="input-block-level text-right"></td>
										</tr>

										<tr>
											<td><p class="text-center">20</p></td>
											<td>อายุรกรรมโรคปอด - หืด / ปอดอุดกั้นเรื้อรัง</td>
											
											<td><input type="text" name="textfield" id="num121" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num122" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num123" placeholder="0" class="input-block-level text-right"></td>
										</tr>
                                        
										<tr>
											<td><p class="text-center">21</p></td>
											<td>อายุรกรรม - ตรวจสมรรถภาพปอด</td>
											
											<td><input type="text" name="textfield" id="num121" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num122" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num123" placeholder="0" class="input-block-level text-right"></td>
										</tr>
                                        
										<tr>
											<td><p class="text-center">22</p></td>
											<td>อายุรกรรม - โลหิตวิทยา</td>
											
											<td><input type="text" name="textfield" id="num121" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num122" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num123" placeholder="0" class="input-block-level text-right"></td>
										</tr>
                                        
										<tr>
											<td><p class="text-center">23</p></td>
											<td>อายุรกรรม - ตรวจสมรรถภาพปอด</td>
											
											<td><input type="text" name="textfield" id="num121" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num122" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num123" placeholder="0" class="input-block-level text-right"></td>
										</tr>
                                        
										<tr>
											<td><p class="text-center">24</p></td>
											<td>อายุรกรรม - ทางเดินอาหาร</td>
											
											<td><input type="text" name="textfield" id="num121" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num122" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num123" placeholder="0" class="input-block-level text-right"></td>
										</tr>
                                        
										<tr>
											<td><p class="text-center">25</p></td>
											<td>อายุรกรรมทางเดินอาหาร - ส่องกล้องทางเดินอาหาร</td>
											
											<td><input type="text" name="textfield" id="num121" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num122" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num123" placeholder="0" class="input-block-level text-right"></td>
										</tr>
                                        
										<tr>
											<td><p class="text-center">26</p></td>
											<td>อายุรกรรม - ประสาทวิทยา</td>
											
											<td><input type="text" name="textfield" id="num121" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num122" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num123" placeholder="0" class="input-block-level text-right"></td>
										</tr>
                                        
										<tr>
											<td><p class="text-center">27</p></td>
											<td>อายุรกรรม - พันธุกรรม</td>
											
											<td><input type="text" name="textfield" id="num121" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num122" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num123" placeholder="0" class="input-block-level text-right"></td>
										</tr>
										<tr>
											<td><p class="text-center">28</p></td>
											<td>คลินิกผู้สูงอายุ</td>
											
											<td><input type="text" name="textfield" id="num121" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num122" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num123" placeholder="0" class="input-block-level text-right"></td>
										</tr>
										<tr>
											<td><p class="text-center">29</p></td>
											<td>คลินิกวัยทอง (ชาย)</td>
											
											<td><input type="text" name="textfield" id="num121" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num122" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num123" placeholder="0" class="input-block-level text-right"></td>
										</tr>
										<tr>
											<td><p class="text-center">30</p></td>
											<td>คลินิกวัณโรค</td>
											
											<td><input type="text" name="textfield" id="num121" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num122" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num123" placeholder="0" class="input-block-level text-right"></td>
										</tr>
										<tr>
											<td><p class="text-center">31</p></td>
											<td>นิติเวช</td>
											
											<td><input type="text" name="textfield" id="num121" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num122" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num123" placeholder="0" class="input-block-level text-right"></td>
										</tr>
										<tr>
											<td><p class="text-center">32</p></td>
											<td>คลินิกด้านวิจัยการแพทย์</td>
											
											<td><input type="text" name="textfield" id="num121" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num122" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num123" placeholder="0" class="input-block-level text-right"></td>
										</tr>
										<tr>
											<td><p class="text-center">33</p></td>
											<td>โสต ศอ นาสิกกรรม (หู คอ จมูก)</td>
											
											<td><input type="text" name="textfield" id="num121" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num122" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num123" placeholder="0" class="input-block-level text-right"></td>
										</tr>
										<tr>
											<td><p class="text-center">34</p></td>
											<td>โสต ศอ นาสิกกรรม - ภูมิแพ้</td>
											
											<td><input type="text" name="textfield" id="num121" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num122" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num123" placeholder="0" class="input-block-level text-right"></td>
										</tr>
										<tr>
											<td><p class="text-center">35</p></td>
											<td>โสต ศอ นาสิกกรรม - ห้องตรวจการได้ยิน</td>
											
											<td><input type="text" name="textfield" id="num121" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num122" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num123" placeholder="0" class="input-block-level text-right"></td>
										</tr>
										<tr>
											<td><p class="text-center">36</p></td>
											<td>จักษุกรรม (ตา)</td>
											
											<td><input type="text" name="textfield" id="num121" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num122" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num123" placeholder="0" class="input-block-level text-right"></td>
										</tr>
										<tr>
											<td><p class="text-center">37</p></td>
											<td>ศัลยกรรม - ทั่วไป / ทางเดินอาหาร / เต้านม</td>
											
											<td><input type="text" name="textfield" id="num121" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num122" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num123" placeholder="0" class="input-block-level text-right"></td>
										</tr>
										<tr>
											<td><p class="text-center">38</p></td>
											<td>ศัลยกรรม - ทรวงอก</td>
											
											<td><input type="text" name="textfield" id="num121" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num122" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num123" placeholder="0" class="input-block-level text-right"></td>
										</tr>
										<tr>
											<td><p class="text-center">39</p></td>
											<td>ศัลยกรรม - ทางเดินปัสสาวะ</td>
											
											<td><input type="text" name="textfield" id="num121" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num122" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num123" placeholder="0" class="input-block-level text-right"></td>
										</tr>
										<tr>
											<td><p class="text-center">40</p></td>
											<td>ศัลยกรรม - ประสาท</td>
											
											<td><input type="text" name="textfield" id="num121" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num122" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num123" placeholder="0" class="input-block-level text-right"></td>
										</tr>
										<tr>
											<td><p class="text-center">41</p></td>
											<td>ศัลยกรรม - ตกแต่ง</td>
											
											<td><input type="text" name="textfield" id="num121" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num122" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num123" placeholder="0" class="input-block-level text-right"></td>
										</tr>
										<tr>
											<td><p class="text-center">42</p></td>
											<td>ศัลยกรรม - ลำไส้ใหญ่ / ทวารหนัก</td>
											
											<td><input type="text" name="textfield" id="num121" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num122" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num123" placeholder="0" class="input-block-level text-right"></td>
										</tr>
										<tr>
											<td><p class="text-center">43</p></td>
											<td>ศัลยกรรมอื่นๆ </td>
											
											<td><input type="text" name="textfield" id="num121" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num122" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num123" placeholder="0" class="input-block-level text-right"></td>
										</tr>
										<tr>
											<td><p class="text-center">44</p></td>
											<td>บริการหัตถการ - ฉีดยา / ทำแผล / ผ่าตัดเล็ก</td>
											
											<td><input type="text" name="textfield" id="num121" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num122" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num123" placeholder="0" class="input-block-level text-right"></td>
										</tr>
										<tr>
											<td><p class="text-center">45</p></td>
											<td>ศัลยกรรมออร์โธปิดิกส์</td>
											
											<td><input type="text" name="textfield" id="num121" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num122" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num123" placeholder="0" class="input-block-level text-right"></td>
										</tr>
										<tr>
											<td><p class="text-center">46</p></td>
											<td>ศัลยกรรมออร์โธปิดิกส์ - หน่วยแขนขาเทียม</td>
											
											<td><input type="text" name="textfield" id="num121" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num122" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num123" placeholder="0" class="input-block-level text-right"></td>
										</tr>
										<tr>
											<td><p class="text-center">47</p></td>
											<td>คลินิกวิสัญญีวิทยาและระงับปวด</td>
											
											<td><input type="text" name="textfield" id="num121" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num122" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num123" placeholder="0" class="input-block-level text-right"></td>
										</tr>
										<tr>
											<td><p class="text-center">48</p></td>
											<td>กุมารเวชกรรม - ทั่วไป</td>
											
											<td><input type="text" name="textfield" id="num121" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num122" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num123" placeholder="0" class="input-block-level text-right"></td>
										</tr>
										<tr>
											<td><p class="text-center">49</p></td>
											<td>กุมารเวชกรรม - คลินิกเด็กสุขภาพดี</td>
											
											<td><input type="text" name="textfield" id="num121" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num122" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num123" placeholder="0" class="input-block-level text-right"></td>
										</tr>
										<tr>
											<td><p class="text-center">50</p></td>
											<td>กุมารเวชกรรม - ไตเทียมเด็ก</td>
											
											<td><input type="text" name="textfield" id="num121" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num122" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num123" placeholder="0" class="input-block-level text-right"></td>
										</tr>
										<tr>
											<td><p class="text-center">51</p></td>
											<td>สูตินรีเวชกรรม - ฝากครรภ์</td>
											
											<td><input type="text" name="textfield" id="num121" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num122" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num123" placeholder="0" class="input-block-level text-right"></td>
										</tr>
										<tr>
											<td><p class="text-center">52</p></td>
											<td>สูตินรีเวชกรรม - นรีเวช</td>
											
											<td><input type="text" name="textfield" id="num121" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num122" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num123" placeholder="0" class="input-block-level text-right"></td>
										</tr>
										<tr>
											<td><p class="text-center">53</p></td>
											<td>สูตินรีเวชกรรม - คลินิกมีบุตรยาก</td>
											
											<td><input type="text" name="textfield" id="num121" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num122" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num123" placeholder="0" class="input-block-level text-right"></td>
										</tr>
										<tr>
											<td><p class="text-center">54</p></td>
											<td>สูตินรีเวชกรรม - คลินิกวัยทอง</td>
											
											<td><input type="text" name="textfield" id="num121" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num122" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num123" placeholder="0" class="input-block-level text-right"></td>
										</tr>
										<tr>
											<td><p class="text-center">55</p></td>
											<td>เวชศาสตร์นิวเคลียร์</td>
											
											<td><input type="text" name="textfield" id="num121" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num122" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num123" placeholder="0" class="input-block-level text-right"></td>
										</tr>
										<tr>
											<td><p class="text-center">56</p></td>
											<td>รังสีรักษา</td>
											
											<td><input type="text" name="textfield" id="num121" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num122" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num123" placeholder="0" class="input-block-level text-right"></td>
										</tr>
										<tr>
											<td><p class="text-center">57</p></td>
											<td>บริการเอ็กชเรย์คอมพิวเตอร์ (CT SCAN)</td>
											
											<td><input type="text" name="textfield" id="num121" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num122" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num123" placeholder="0" class="input-block-level text-right"></td>
										</tr>
										<tr>
											<td><p class="text-center">58</p></td>
											<td>บริการเอ็กซเรย์คลื่นแม่เหล็กไฟฟ้า (MRI)</td>
											
											<td><input type="text" name="textfield" id="num121" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num122" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num123" placeholder="0" class="input-block-level text-right"></td>
										</tr>
										<tr>
											<td><p class="text-center">59</p></td>
											<td>บริการตรวจทางรังสีอื่นๆ</td>
											
											<td><input type="text" name="textfield" id="num121" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num122" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num123" placeholder="0" class="input-block-level text-right"></td>
										</tr>
										<tr>
											<td><p class="text-center">60</p></td>
											<td>อุบัติเหตุ / เวชกรรมฉุกเฉิน</td>
											
											<td><input type="text" name="textfield" id="num121" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num122" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num123" placeholder="0" class="input-block-level text-right"></td>
										</tr>
										<tr>
											<td><p class="text-center">61</p></td>
											<td>จิตเวช</td>
											
											<td><input type="text" name="textfield" id="num121" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num122" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num123" placeholder="0" class="input-block-level text-right"></td>
										</tr>
										<tr>
											<td><p class="text-center">62</p></td>
											<td>บริการรักษาด้านยาเสพติด</td>
											
											<td><input type="text" name="textfield" id="num121" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num122" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num123" placeholder="0" class="input-block-level text-right"></td>
										</tr>
										<tr>
											<td><p class="text-center">63</p></td>
											<td>เวชศาสตร์ฟื้นฟู</td>
											
											<td><input type="text" name="textfield" id="num121" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num122" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num123" placeholder="0" class="input-block-level text-right"></td>
										</tr>
										<tr>
											<td><p class="text-center">64</p></td>
											<td>กายภาพบำบัด</td>
											
											<td><input type="text" name="textfield" id="num121" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num122" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num123" placeholder="0" class="input-block-level text-right"></td>
										</tr>
										<tr>
											<td><p class="text-center">65</p></td>
											<td>บริการตรวจทางห้องปฏิบัติการ - ตรวจเลือดและส่วนประกอบ / อุจจาระ / ปัสสาวะ / ภูมิคุ้มกัน / ชิ้นเนื้อ / เซลล์ / อิมมูโน</td>
											
											<td><input type="text" name="textfield" id="num121" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num122" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num123" placeholder="0" class="input-block-level text-right"></td>
										</tr>
										<tr>
											<td><p class="text-center">66</p></td>
											<td>บริการแพทย์ฝังเข็ม</td>
											
											<td><input type="text" name="textfield" id="num121" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num122" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num123" placeholder="0" class="input-block-level text-right"></td>
										</tr>
										<tr>
											<td><p class="text-center">67</p></td>
											<td>บริการนวดแผนไทย</td>
											
											<td><input type="text" name="textfield" id="num121" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num122" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num123" placeholder="0" class="input-block-level text-right"></td>
										</tr>
										<tr>
											<td><p class="text-center">68</p></td>
											<td>บริการแพทย์ทางเลือกอื่นๆ</td>
											
											<td><input type="text" name="textfield" id="num121" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num122" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num123" placeholder="0" class="input-block-level text-right"></td>
										</tr>
										<tr>
											<td><p class="text-center">69</p></td>
											<td>คลินิกนอกเวลา (รวมทุกแผนก)</td>
											
											<td><input type="text" name="textfield" id="num121" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num122" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num123" placeholder="0" class="input-block-level text-right"></td>
										</tr>
										<tr>
											<td><p class="text-center">70</p></td>
											<td>ทันตกรรม</td>
											
											<td><input type="text" name="textfield" id="num121" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num122" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num123" placeholder="0" class="input-block-level text-right"></td>
										</tr>
										<tr>
											<td><p class="text-center">71</p></td>
											<td>บริการอื่นๆ  โปรดระบุ  ..................................................</td>
											
											<td><input type="text" name="textfield" id="num121" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num122" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num123" placeholder="0" class="input-block-level text-right"></td>
										</tr>
									</tbody>
								</table>
                               <div class="span9">
									        <div class="form-actions pull-right">
										        <button type="submit" class="btn btn-green"><i class="icon-ok"></i> บันทึกข้อมูล</button>
										        <button type="submit" class="btn btn-green"><i class="icon-ok"></i> ยืนยันและส่งข้อมูล</button>
										        <button type="button" class="btn btn-lightred"><i class="icon-remove"></i> ยกเลิก</button>
									        </div>
									</div>
							</div>
						</div>
				   	</div>

				</div>
				
			</div>
		</div></div>
</asp:Content>









