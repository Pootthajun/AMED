﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="Hotpital_Check.aspx.cs" Inherits="AMED.Hotpital_Check" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <title>การตรวจสอบการบันทึกข้อมูล - กรมการแพทย์ทหารบก</title>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <div class="container-fluid" id="content">
		<div id="left">
			<div class="subnav">
				<div class="subnav-title">
					<a href="#" class='toggle-subnav'><i class="icon-angle-down"></i><span><p class="icon-building"></p> หน่วยสายแพทย์</span></a>
				</div>
				<ul class="subnav-menu">
					<li>
						<a href="Default.aspx">สรุปภาพรวม</a>
					</li>
					<li>
							<a href="Hotpital.aspx">การบันทึกข้อมูล</a>
							
						</li>
					<li>
						<a href="Report.aspx">สรุปรายงาน</a>
					</li>
				</ul>
			</div>
			<div class="subnav">
				<div class="subnav-title">
					<a href="#" class='toggle-subnav'><i class="icon-angle-down"></i><span><p class="icon-building"></p>  ส่วนกลาง</span></a>
				</div>
				<ul class="subnav-menu">
					<li>
						<a href="Default.aspx">สรุปภาพรวม</a>
					</li>
					<li class="active">
							<a href="Department.aspx">การตรวจสอบการบันทึกข้อมูล</a>
							
					</li>
					<li>
						<a href="Report.aspx">สรุปรายงาน</a>
					</li>
				</ul>
			</div>
            <div class="subnav">
				<div class="subnav-title">
					<a href="#" class='toggle-subnav'><i class="icon-angle-down"></i><span><p class="icon-cog"></p> ข้อมูลพื้นฐาน</span></a>
				</div>
				<ul class="subnav-menu">
					<li class='dropdown'>
						<a href="#" data-toggle="dropdown"> ข้อมุลทั่วไป</a>
						<ul class="dropdown-menu">
							<li>
								<a href="MT_Region.aspx"> กองทัพภาค</a>
							</li>
							<li>
								<a href="MT_Hospital.aspx"> หน่วยสายแพทย์</a>
							</li>
							<li>
								<a href="MT_Department.aspx"> หน่วยงาน (ส่วนกลาง)</a>
							</li>
							<li>
								<a href="MT_AddOrganization.aspx"> สังกัด</a>
							</li>
							<li>
								<a href="MT_Province.aspx"> จังหวัด</a>
							</li>
						</ul>
					</li>
					<li class='dropdown'>
						<a href="#" data-toggle="dropdown">กองวิทยาการ</a>
						<ul class="dropdown-menu">
							<li>
								<a href="MTS_ICD10.aspx"> รหัสโรค (ICD10)</a>
							</li>
							<li>
								<a href="MTS_GroupRule.aspx"> กลุ่มโรคที่ขัดต่อกฎกระทรวง</a>
							</li>
							<li>
								<a href="MTS_Rule.aspx"> โรคที่ขัดต่อกฎกระทรวง</a>
							</li>
							<li>
								<a href="MTS_Sizebed.aspx"> ขนาดเตียง</a>
							</li>
							<li>
								<a href="MTS_ProHotpital.aspx"> ตำแหน่งในโรงพยาบาล</a>
							</li>
							<li>
								<a href="MTS_Wards.aspx"> หอผู้ป่วย</a>
							</li>
						</ul>
					</li>
                    <li class='dropdown'>
						<a href="#" data-toggle="dropdown">กองงบประมาณการเงิน</a>
						<ul class="dropdown-menu">
							<li>
								<a href="MTF_Assets.aspx">สินทรัพย์และหนี้สิน</a>
							</li>
							<li>
								<a href="MTF_Progress.aspx">ผลการดำเนินงาน</a>
							</li>
							<li>
								<a href="MTF_Reception.aspx">การรับ-การจ่าย</a>
							</li>
							<li>
								<a href="MTF_Deposit.aspx">เงินฝาก</a>
							</li>
						</ul>
					</li>
                    <li class='dropdown'>
						<a href="#" data-toggle="dropdown">กองส่งกำลังบำรุง</a>
						<ul class="dropdown-menu">
							<li>
								<a href="MTT_TypeDrug.aspx"> ประเภทยา</a>
							</li>
							<li>
								<a href="MTT_Drug.aspx"> ยา</a>
							</li>
							<li>
								<a href="MTT_DrugNature.aspx">ลักษณะยา</a>
							</li>
							<li>
								<a href="MTT_MedicalSupplies.aspx">เวชภัณฑ์</a>
							</li>
						</ul>
					</li>
                    <li class='dropdown'>
						<a href="#" data-toggle="dropdown">ผู้ใช้งาน (User)</a>
						<ul class="dropdown-menu">
							<li>
								<a href="MT_PersonType.aspx"> ประเภทบุคคล</a>
							</li>
							<li>
								<a href="MT_Person.aspx">บุคคลย่อย</a>
							</li>
							<li>
								<a href="#">กำหนดสิทธิ์</a>
							</li>
							<li>
								<a href="MT_User.aspx">ผู้ใช้งานในระบบ</a>
							</li>
						</ul>
					</li>
                     <li class='dropdown'>
						<a href="#" data-toggle="dropdown">การส่งรายงาน</a>
						<ul class="dropdown-menu">
							<li>
								<a href="MTR_DeadlineReports.aspx"> กำหนดส่งรายงาน</a>
							</li>
							<li>
								<a href="MTR_EditReport.aspx"> แก้ไขข้อมูลรายงาน</a>
							</li>
							<li>
								<a href="MTR_NameReport.aspx"> ชื่อแบบฟอร์มรายงาน</a>
							</li>
						</ul>
					</li>
				</ul>
			</div>
		</div>
		<div id="main">
			<div class="container-fluid">
				<div class="page-header">
					<div class="pull-left">
						<h4>การตรวจสอบการบันทึกข้อมูล</h4>
					</div>
					
				</div>
				<div class="breadcrumbs">
					<ul>
						<li>
							<a href="Default.aspx">Dashborad</a>
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<a href="forms-basic.html">กองวิทยาการ</a>
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<a href="Hotpital_Check.aspx">การตรวจสอบการบันทึกข้อมูล</a>
						</li>
					</ul>
					<div class="close-bread">
						<a href="#"><i class="icon-remove"></i></a>
					</div>
				</div>
				

				<div class="row-fluid">

						<div class="span12">
						  <div class="box">
							<div class="box-title">
                                <div class='basic-margin'>
                                   <form action="#" method="get" class='form-vertical'>
                                    <div class="span2">
											<div class="control-group">
												<label for="textfield" class="control-label">กองทัพ</label>
                                                <select name="s2" id="s2" class='select2-me input-small'>
												    <option value="1">ทั้งหมด</option>
												    <option value="2" selected disabled>ส่วนกลาง</option>
												    <option value="3">ภาคที่ 1</option>
												    <option value="4">ภาคที่ 2</option>
												    <option value="5">ภาคที่ 3</option>
												    <option value="6">ภาคที่ 4</option>
											      </select>
                                                <%--<asp:DropDownList ID="ddlทั้งหมด" runat="server" Width="150px">
                                                <asp:ListItem Text="หน่วยงาน (ส่วนกลาง)" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="กองทัพภาคที่ 2" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="กองทัพภาคที่ 3" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="กองทัพภาคที่ 4" Value="3"></asp:ListItem>
                                            </asp:DropDownList>--%>
											</div>
                                            
										</div>
										<div class="span3">
                                            <div class="control-group">
												<label for="textfield" class="control-label">หน่วยสายแพทย์</label>
                                                <select name="s2" id="s2" class='select2-me input-large'>
												    <option value="1">ทั้งหมด</option>
												    <option value="2" selected disabled>รพ.พระมงกุฎเกล้า</option>
												    <option value="3">รพ.อานันทมหิดล</option>
												    <option value="4">รพ.ค่ายสุรนารี</option>
											      </select>
                                                <%--<asp:DropDownList ID="DropDownList2" runat="server" Width="230px">
                                                <asp:ListItem Text="ทั้งหมด" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="รพ.พระมงกุฎเกล้า" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="รพ.อานันทมหิดล" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="รพ.ค่ายสุรนารี" Value="3"></asp:ListItem>
                                            </asp:DropDownList>--%>
											</div>
										</div>
                                          <div class="span2">
										    <div class="control-group">
                                                <label for="textfield" class="control-label">ปีงบประมาณ</label>
                                                   <select name="s2" id="s2" class='select2-me input-small'>
												    <option value="1">ทั้งหมด</option>
												    <option value="2" selected disabled>2560</option>
												    <option value="3">2559</option>
												    <option value="4">2558</option>
												    <option value="5">2557</option>
												    <option value="6">2556</option>
												    <option value="7">2555</option>
											      </select>
												 <%--<label for="textfield" class="control-label">ปีงบประมาณ</label>
                                                    <asp:DropDownList ID="DropDownList1" runat="server" Width="120px">
                                                    <asp:ListItem Text="ทั้งหมด" Value="0"></asp:ListItem>
                                                    <asp:ListItem Text="2560" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="2559" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="2558" Value="3"></asp:ListItem>
                                                    <asp:ListItem Text="2557" Value="4"></asp:ListItem>
                                                </asp:DropDownList>--%>
											    </div>
                                              </div>
										<div class="span3">
                                            <div class="control-group">
                                              <label for="textfield" class="control-label">ประจำเดือน</label>
                                                <select name="s2" id="s2" class='select2-me input-large'>
												    <option value="1">ทั้งหมด</option>
												    <option value="2" selected disabled>มกราคม</option>
												    <option value="3">กุมภาพันธ์</option>
												    <option value="4">มีนาคม</option>
												    <option value="5">เมษายน</option>
												    <option value="6">พฤษภาคม</option>
												    <option value="7">กรกฎาคม</option>
												    <option value="4">สิงหาคม</option>
												    <option value="5">กันยายน</option>
												    <option value="6">ตุลาคม</option>
												    <option value="7">พฤศจิกายน</option>
												    <option value="7">ธันวาคม</option>
											      </select>
												<%--<label for="textfield" class="control-label">ประจำเดือน</label>
                                                <asp:DropDownList ID="DropDownList3" runat="server" Width="200">
                                                <asp:ListItem Text="ทั้งหมด" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="มกราคม" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="กุมภาพันธ์" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="มีนาคม" Value="3"></asp:ListItem>
                                                <asp:ListItem Text="เมษายน" Value="4"></asp:ListItem>
                                                <asp:ListItem Text="พฤษภาคม" Value="5"></asp:ListItem>
                                                <asp:ListItem Text="กรกฎาคม" Value="6"></asp:ListItem>
                                                <asp:ListItem Text="สิงหาคม" Value="7"></asp:ListItem>
                                                <asp:ListItem Text="กันยายน" Value="8"></asp:ListItem>
                                                <asp:ListItem Text="ตุลาคม" Value="9"></asp:ListItem>
                                                <asp:ListItem Text="พฤศจิกายน" Value="10"></asp:ListItem>
                                                <asp:ListItem Text="ธันวาคม" Value="11"></asp:ListItem>
                                            </asp:DropDownList>--%>
											</div>
										</div>
									
								</form><br />
								   <div class="btn-group span2 pull-right">
										<a href="#" data-toggle="dropdown" class="btn btn-warning dropdown-toggle"><i class="icon-print"></i> พิมพ์รายงาน <span class="caret"></span></a>
										<ul class="dropdown-menu dropdown-warning">
											<li><a href="#"><i class="icon-file"></i> Excel</a></li>
											<li><a href="#"><i class="icon-file"></i> PDF</a></li>
										</ul>
									</div>
                                </div>
							</div><br />
                           <div class="box-content nopadding">
								
                                   <table class="table table-hover table-nomargin table-bordered dataTable-columnfilter dataTable dataTable-noheader table-edit">
										<thead>
										<tr>
											<th style="width: 85px"><p class="text-center">รายงานหน่วยสายแพทย์</p></th>
											<th style="width: 3px"><p class="text-center">บันทึก</p></th>
											<th style="width: 3px"><p class="text-center">ยืนยัน</p></th>
											<th style="width: 3px"><p class="text-center">ตรวจสอบ</p></th>
											<th style="width: 3px"><p class="text-center">วันที่</p></th>
											<th style="width: 3px"><p class="text-center">ผู้ส่งรายงาน</p></th>
										</tr>
									</thead>
										<tbody>
											<tr>
											<td><a href="FormDepartment_1101.aspx"><p class="text-warning"> 1.รายงานจำนวนผู้ป่วยจำแนกตามประเภทบุคคล ( รง.ผสต.1 ) </p></a></td>
											
											<td><p class="text-center"><i class="icon-ok text-warning"></i></p></td>
											<td><p class="text-center"><i class="icon-ok text-warning"></i></p></td>
											<td><a class="text-center"></a></td>
											<td><a class="text-center"></a></td>
											<td><a class="text-center"></a></td>
										</tr>
											<tr>
											<td><a href="FormDepartment_1102.aspx"><p class="text-warning"> 2. รายงานจำนวนผู้ป่วยในจำแนกตามสาเหตุป่วย ( รง.ผสต.2 )</p></a></td>
											
											<td><p class="text-center"><i class="icon-ok text-warning"></i></p></td>
											<td><p class="text-center"><i class="icon-ok text-warning"></i></p></td>
											<td><a class="text-center"></a></td>
											<td><a class="text-center"></a></td>
											<td><a class="text-center"></a></td>
										</tr>
											<tr>
											<td><a href="FormDepartment_1103.aspx"><p class="text-warning"> 3. รายงานจำนวนผู้ป่วยในจำแนกตามสาเหตุปลดพิการ ( รง.ผสต.3 )</p></a></td>
											
											<td><p class="text-center"><i class="icon-ok text-warning"></i></p></td>
											<td><p class="text-center"><i class="icon-ok text-warning"></i></p></td>
											<td><a class="text-center"></a></td>
											<td><a class="text-center"></a></td>
											<td><a class="text-center"></a></td>
										</tr>
											<tr>
											<td><a href="FormDepartment_1104.aspx"> 4. รายงานจำนวนผู้ป่วยในจำแนกตามสาเหตุตาย( รง.ผสต.4 )</a></td>
											
											<td><p class="text-center"><i class="icon-ok text-success"></i></p></td>
											<td><p class="text-center"><i class="icon-ok text-success"></i></p></td>
											<td><p class="text-center"><i class="icon-ok text-success"></i></p></td>
                                            <td><a class="text-center"><p class="text-success">5/02/2560</p></a></td>
											<td><a class="text-center"><p class="text-success">ปิยนุช  สุขใจ</p></a></td>
										</tr>
											
											<tr>
											<td><a href="FormDepartment_1105.aspx"> 5. รายงานจำนวนผู้ป่วยนอกจำแนกตามสาเหตุป่วย ( รง.ผสต.5 )</a></td>
											
											<td><p class="text-center"><i class="icon-ok text-success"></i></p></td>
											<td><p class="text-center"><i class="icon-ok text-success"></i></p></td>
											<td><p class="text-center"><i class="icon-ok text-success"></i></p></td>
                                            <td><a class="text-center"><p class="text-success">5/02/2560</p></a></td>
											<td><a class="text-center"><p class="text-success">ปิยนุช  สุขใจ</p></a></td>
										</tr>
											<tr>
											<td><a href="FormDepartment_1106.aspx"> 6. บัญชียอดกำลังพลซึ่งต้องสนับสนุนบริการแพทย์( รง.ผสต.6 )</a></td>
											
											<td><p class="text-center"><i class="icon-ok text-success"></i></p></td>
											<td><p class="text-center"><i class="icon-ok text-success"></i></p></td>
											<td><p class="text-center"><i class="icon-ok text-success"></i></p></td>
                                            <td><a class="text-center"><p class="text-success">5/02/2560</p></a></td>
											<td><a class="text-center"><p class="text-success">ปิยนุช  สุขใจ</p></a></td>
										</tr>
											<tr>
											<td><a href="FormDepartment_1108.aspx"> 7. รายงานจำนวนผู้มารับบริการของ รพ.ทบ. ( ตัวชี้วัด ) ( รง.ผสต.8 )</a></td>
											
											<td><p class="text-center"><i class="icon-ok text-success"></i></p></td>
											<td><p class="text-center"><i class="icon-ok text-success"></i></p></td>
											<td><p class="text-center"><i class="icon-ok text-success"></i></p></td>
                                            <td><a class="text-center"><p class="text-success">5/02/2560</p></a></td>
											<td><a class="text-center"><p class="text-success">ปิยนุช  สุขใจ</p></a></td>
										</tr>
											<tr>
											<td><a href="FormDepartment_1109.aspx"> 8. รายงานจำนวนผู้ป่วยที่มารับบริการฝังเข็มจำแนกตามประเภทบุคคล ( รง.ผสต.9 )</a></td>
											
											<td><p class="text-center"><i class="icon-ok text-success"></i></p></td>
											<td><p class="text-center"><i class="icon-ok text-success"></i></p></td>
											<td><p class="text-center"><i class="icon-ok text-success"></i></p></td>
                                            <td><a class="text-center"><p class="text-success">5/02/2560</p></a></td>
											<td><a class="text-center"><p class="text-success">ปิยนุช  สุขใจ</p></a></td>
										</tr>
										<tr>
											<td><a href="FormDepartment_1110.aspx"> 9. รายงานจำนวนผู้ป่วยที่มารับบริการฝังเข็มจำแนกตาม <br /> สาเหตุป่วยและความพึงพอใจของผู้มารับบริการฝังเข็ม ( รง.ผสต.10 )</a></td>
											
											<td><p class="text-center"><i class="icon-ok text-success"></i></p></td>
											<td><p class="text-center"><i class="icon-ok text-success"></i></p></td>
											<td><p class="text-center"><i class="icon-ok text-success"></i></p></td>
                                            <td><a class="text-center"><p class="text-success">5/02/2560</p></a></td>
											<td><a class="text-center"><p class="text-success">ปิยนุช  สุขใจ</p></a></td>
										</tr>
										<tr>
											<td><a href="FormDepartment_1111.aspx">10. ข้อมูลการตรวจร่างกายของกำลังกองพลทัพบกรายบุคคล ( รง.ผสต.11 )</a></td>
											
											<td><p class="text-center"><i class="icon-ok text-success"></i></p></td>
											<td><p class="text-center"><i class="icon-ok text-success"></i></p></td>
											<td><p class="text-center"><i class="icon-ok text-success"></i></p></td>
                                            <td><a class="text-center"><p class="text-success">5/02/2560</p></a></td>
											<td><a class="text-center"><p class="text-success">ปิยนุช  สุขใจ</p></a></td>
										</tr>
										<tr>
											<td><a href="FormDepartment_1112.aspx">11. ข้อมูลการตรวจร่างกายของครอบครัวกำลังกองพลทัพบกรายบุคคล ( รง.ผสต.12 )</a></td>
											
											<td><p class="text-center"><i class="icon-ok text-success"></i></p></td>
											<td><p class="text-center"><i class="icon-ok text-success"></i></p></td>
											<td><p class="text-center"><i class="icon-ok text-success"></i></p></td>
                                            <td><a class="text-center"><p class="text-success">5/02/2560</p></a></td>
											<td><a class="text-center"><p class="text-success">ปิยนุช  สุขใจ</p></a></td>
										</tr>
										<tr>
											<td><a href="FormDepartment_1117.aspx">12. รายงานจำนวนผู้ป่วยนอก จำนวนผู้ป่วยรับมา และจำนวนผุ้ป่วยส่งต่อไปที่อื่น ( รง.ผสต.17 )</a></td>
											
											<td><p class="text-center"><i class="icon-ok text-success"></i></p></td>
											<td><p class="text-center"><i class="icon-ok text-success"></i></p></td>
											<td><p class="text-center"><i class="icon-ok text-success"></i></p></td>
                                            <td><a class="text-center"><p class="text-success">5/02/2560</p></a></td>
											<td><a class="text-center"><p class="text-success">ปิยนุช  สุขใจ</p></a></td>
										</tr>
										<tr>
											<td><a href="FormDepartment_1118.aspx">13. รายงานจำนวนผู้ป่วยใน จำนวนพยาบาล และจำนวนเตียงที่ใช้จริง ( รง.ผสต.18 )</a></td>
											
											<td><p class="text-center"><i class="icon-ok text-success"></i></p></td>
											<td><p class="text-center"><i class="icon-ok text-success"></i></p></td>
											<td><p class="text-center"><i class="icon-ok text-success"></i></p></td>
                                            <td><a class="text-center"><p class="text-success">5/02/2560</p></a></td>
											<td><a class="text-center"><p class="text-success">ปิยนุช  สุขใจ</p></a></td>
										</tr>
										<tr>
											<td><a href="FormDepartment_1119.aspx">14. รายงานจำนวนแพทย์ จำแนกตามสาขาของการแพทย์เฉพาะทาง ( รง.ผสต.19 )</a></td>
											
											<td><p class="text-center"><i class="icon-ok text-success"></i></p></td>
											<td><p class="text-center"><i class="icon-ok text-success"></i></p></td>
											<td><p class="text-center"><i class="icon-ok text-success"></i></p></td>
                                            <td><a class="text-center"><p class="text-success">5/02/2560</p></a></td>
											<td><a class="text-center"><p class="text-success">ปิยนุช  สุขใจ</p></a></td>
										</tr>
										<tr>
											<td><a href="FormDepartment_1122_1.aspx">15. รายงานข้อมูลสถานที่ เครื่องมือแพทย์ และครุภัณฑ์การแพทย์ ( รง.ผสต.22 )</a></td>
											
											<td><p class="text-center"><i class="icon-ok text-success"></i></p></td>
											<td><p class="text-center"><i class="icon-ok text-success"></i></p></td>
											<td><p class="text-center"><i class="icon-ok text-success"></i></p></td>
                                            <td><a class="text-center"><p class="text-success">5/02/2560</p></a></td>
											<td><a class="text-center"><p class="text-success">ปิยนุช  สุขใจ</p></a></td>
										</tr>
										<tr>
											<td><a href="FormDepartment_1122_2.aspx">16. รายงานข้อมูลสถานที่ เครื่องมือแพทย์ และครุภัณฑ์การแพทย์ ( รง.ผสต.22 )</a></td>
											
											<td><p class="text-center"><i class="icon-ok text-success"></i></p></td>
											<td><p class="text-center"><i class="icon-ok text-success"></i></p></td>
											<td><p class="text-center"><i class="icon-ok text-success"></i></p></td>
                                            <td><a class="text-center"><p class="text-success">5/02/2560</p></a></td>
											<td><a class="text-center"><p class="text-success">ปิยนุช  สุขใจ</p></a></td>
										</tr>
										<tr>
											<td><a href="FormDepartment_1123.aspx">17. รายงานจำนวนกำลังพลของหน่วยสายแพทย์ จำแนกตามตำแหน่ง ( รง.ผสต.23 )</a></td>
											
											<td><p class="text-center"><i class="icon-ok text-success"></i></p></td>
											<td><p class="text-center"><i class="icon-ok text-success"></i></p></td>
											<td><p class="text-center"><i class="icon-ok text-success"></i></p></td>
                                            <td><a class="text-center"><p class="text-success">5/02/2560</p></a></td>
											<td><a class="text-center"><p class="text-success">ปิยนุช  สุขใจ</p></a></td>
										</tr>
										<tr>
											<td><a href="FormDepartment_Draft.aspx">18. รายงานการตรวจโรคชายไทยก่อนเกณฑ์ทหาร</a></td>
											
											<td><p class="text-center"><i class="icon-ok text-success"></i></p></td>
											<td><p class="text-center"><i class="icon-ok text-success"></i></p></td>
											<td><p class="text-center"><i class="icon-ok text-success"></i></p></td>
                                            <td><a class="text-center"><p class="text-success">5/02/2560</p></a></td>
											<td><a class="text-center"><p class="text-success">ปิยนุช  สุขใจ</p></a></td>
										</tr>
										<tr>
											<td><a href="FormDepartment_Extinction.aspx">19. รายงานสถิติการสุญเสีย</a></td>
											
											<td><p class="text-center"><i class="icon-ok text-success"></i></p></td>
											<td><p class="text-center"><i class="icon-ok text-success"></i></p></td>
											<td><p class="text-center"><i class="icon-ok text-success"></i></p></td>
                                            <td><a class="text-center"><p class="text-success">5/02/2560</p></a></td>
											<td><a class="text-center"><p class="text-success">ปิยนุช  สุขใจ</p></a></td>
										</tr>
										<tr>
											<td><a href="Finance_Statements170.aspx">20. งบแสดงฐานะการเงิน ว170</a></td>
											
											<td><p class="text-center"><i class="icon-ok text-success"></i></p></td>
											<td><p class="text-center"><i class="icon-ok text-success"></i></p></td>
											<td><p class="text-center"><i class="icon-ok text-success"></i></p></td>
                                            <td><a class="text-center"><p class="text-success">5/02/2560</p></a></td>
											<td><a class="text-center"><p class="text-success">ปิยนุช  สุขใจ</p></a></td>
										</tr>
										<tr>
											<td><a href="Finance_Performance170.aspx">21. งบแสดงผลการดำเนินงาน ว170</a></td>
											
											<td><p class="text-center"><i class="icon-ok text-success"></i></p></td>
											<td><p class="text-center"><i class="icon-ok text-success"></i></p></td>
											<td><p class="text-center"><i class="icon-ok text-success"></i></p></td>
                                            <td><a class="text-center"><p class="text-success">5/02/2560</p></a></td>
											<td><a class="text-center"><p class="text-success">ปิยนุช  สุขใจ</p></a></td>
										</tr>
										<tr>
											<td><a href="Finance_Reception.aspx">22. รายงานการรับ-จ่ายเงิน ว170</a></td>
											
											<td><p class="text-center"><i class="icon-ok text-success"></i></p></td>
											<td><p class="text-center"><i class="icon-ok text-success"></i></p></td>
											<td><p class="text-center"><i class="icon-ok text-success"></i></p></td>
                                            <td><a class="text-center"><p class="text-success">5/02/2560</p></a></td>
											<td><a class="text-center"><p class="text-success">ปิยนุช  สุขใจ</p></a></td>
										</tr>
										<tr>
											<td><a href="Finance_Balance.aspx">22. รายงานเงินคงเหลือ ณ วันปลาย ว170</a></td>
											
											<td><p class="text-center"><i class="icon-ok text-success"></i></p></td>
											<td><p class="text-center"><i class="icon-ok text-success"></i></p></td>
											<td><p class="text-center"><i class="icon-ok text-success"></i></p></td>
                                            <td><a class="text-center"><p class="text-success">5/02/2560</p></a></td>
											<td><a class="text-center"><p class="text-success">ปิยนุช  สุขใจ</p></a></td>
										</tr>
										<tr>
											<td><a href="Finance_Statements237.aspx">23. งบแสดงฐานะการเงิน ว237</a></td>
											
											<td><p class="text-center"><i class="icon-ok text-success"></i></p></td>
											<td><p class="text-center"><i class="icon-ok text-success"></i></p></td>
											<td><p class="text-center"><i class="icon-ok text-success"></i></p></td>
                                            <td><a class="text-center"><p class="text-success">5/02/2560</p></a></td>
											<td><a class="text-center"><p class="text-success">ปิยนุช  สุขใจ</p></a></td>
										</tr>
										<tr>
											<td><a href="Finance_Performance237.aspx">24. งบแสดงผลการดำเนินงาน ว237</a></td>

											<td><p class="text-center"><i class="icon-ok text-success"></i></p></td>
											<td><p class="text-center"><i class="icon-ok text-success"></i></p></td>
											<td><p class="text-center"><i class="icon-ok text-success"></i></p></td>
                                            <td><a class="text-center"><p class="text-success">5/02/2560</p></a></td>
											<td><a class="text-center"><p class="text-success">ปิยนุช  สุขใจ</p></a></td>
										</tr>
										<tr>
											<td><a href="Medicine_Manage.aspx">25. สรุปการบริหารยาและเวชภัณฑ์ ก</a></td>
											
											<td><p class="text-center"><i class="icon-ok text-success"></i></p></td>
											<td><p class="text-center"><i class="icon-ok text-success"></i></p></td>
											<td><p class="text-center"><i class="icon-ok text-success"></i></p></td>
                                            <td><a class="text-center"><p class="text-success">5/02/2560</p></a></td>
											<td><a class="text-center"><p class="text-success">ปิยนุช  สุขใจ</p></a></td>
										</tr>
										<tr>
											<td><a href="Medicine_Status.aspx">26. สถานภาพยาและเวชภัณฑ์ ข</a></td>
											
											<td><p class="text-center"><i class="icon-ok text-success"></i></p></td>
											<td><p class="text-center"><i class="icon-ok text-success"></i></p></td>
											<td><p class="text-center"><i class="icon-ok text-success"></i></p></td>
                                            <td><a class="text-center"><p class="text-success">5/02/2560</p></a></td>
											<td><a class="text-center"><p class="text-success">ปิยนุช  สุขใจ</p></a></td>
										</tr>
										<tr>
											<td><a href="Medicine_Amount.aspx">27. สรุปจำนวนรายการยา (Item)</a></td>
											
											<td><p class="text-center"><i class="icon-ok text-success"></i></p></td>
											<td><p class="text-center"><i class="icon-ok text-success"></i></p></td>
											<td><p class="text-center"><i class="icon-ok text-success"></i></p></td>
                                            <td><a class="text-center"><p class="text-success">5/02/2560</p></a></td>
											<td><a class="text-center"><p class="text-success">ปิยนุช  สุขใจ</p></a></td>
										</tr>
										</tbody>
									</table>
								
						   </div>
							</div>
						</div>
				   	</div>

				</div>
				
			</div>
		</div>
</asp:Content>









