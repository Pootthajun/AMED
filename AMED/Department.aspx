﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="Department.aspx.cs" Inherits="AMED.Department" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <title>ตรวจสอบการบันทึกข้อมูล - กรมการแพทย์ทหารบก</title>
</asp:Content>
<%--<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
        <div class="container-fluid">
	      <img src="assets/img/header/logo.png">
			    
			    <div class="user">
				    <ul class='main-nav'>
				    
                    <li class="highli">
					    <a href="#" data-toggle="dropdown" class='dropdown-toggle'>
						    <span><i class="icon-user"></i>  Atiwat</span>
					    </a>
					    <ul class="dropdown-menu pull-right">
						    <li>
							    <a href="more-userprofile.html">Edit profile</a>
						    </li>
						    <li>
							    <a href="#">Account settings</a>
						    </li>
						    <li>
							    <a href="more-login.html">Sign out</a>
						    </li>
					    </ul>
				    </li>
			    </ul>
				    
			    </div>
		    </div>
</asp:Content>--%>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <div class="container-fluid" id="content">
		<div id="left">
			<div class="subnav">
				<div class="subnav-title">
					<a href="#" class='toggle-subnav'><i class="icon-angle-down"></i><span><p class="icon-building"></p> หน่วยสายแพทย์</span></a>
				</div>
				<ul class="subnav-menu">
					<li>
						<a href="Default.aspx">สรุปภาพรวม</a>
					</li>
					<li>
							<a href="Hotpital.aspx">การบันทึกข้อมูล</a>
							
						</li>
					<li>
						<a href="Report.aspx">สรุปรายงาน</a>
					</li>
				</ul>
			</div>
			<div class="subnav">
				<div class="subnav-title">
					<a href="#" class='toggle-subnav'><i class="icon-angle-down"></i><span><p class="icon-building"></p>  ส่วนกลาง</span></a>
				</div>
				<ul class="subnav-menu">
					<li>
						<a href="Default.aspx">สรุปภาพรวม</a>
					</li>
					<li class="active">
							<a href="Department.aspx">การตรวจสอบการบันทึกข้อมูล</a>
							
					</li>
					<li>
						<a href="Report.aspx">สรุปรายงาน</a>
					</li>
				</ul>
			</div>
            <div class="subnav">
				<div class="subnav-title">
					<a href="#" class='toggle-subnav'><i class="icon-angle-down"></i><span><p class="icon-cog"></p> ข้อมูลพื้นฐาน</span></a>
				</div>
				<ul class="subnav-menu">
					<li class='dropdown'>
						<a href="#" data-toggle="dropdown"> ข้อมุลทั่วไป</a>
						<ul class="dropdown-menu">
							<li>
								<a href="MT_Region.aspx"> กองทัพภาค</a>
							</li>
							<li>
								<a href="MT_Hospital.aspx"> หน่วยสายแพทย์</a>
							</li>
							<li>
								<a href="MT_Department.aspx"> หน่วยงาน (ส่วนกลาง)</a>
							</li>
							<li>
								<a href="MT_AddOrganization.aspx"> สังกัด</a>
							</li>
							<li>
								<a href="MT_Province.aspx"> จังหวัด</a>
							</li>
						</ul>
					</li>
					<li class='dropdown'>
						<a href="#" data-toggle="dropdown">กองวิทยาการ</a>
						<ul class="dropdown-menu">
							<li>
								<a href="MTS_ICD10.aspx"> รหัสโรค (ICD10)</a>
							</li>
							<li>
								<a href="MTS_GroupRule.aspx"> กลุ่มโรคที่ขัดต่อกฎกระทรวง</a>
							</li>
							<li>
								<a href="MTS_Rule.aspx"> โรคที่ขัดต่อกฎกระทรวง</a>
							</li>
							<li>
								<a href="MTS_Sizebed.aspx"> ขนาดเตียง</a>
							</li>
							<li>
								<a href="MTS_ProHotpital.aspx"> ตำแหน่งในโรงพยาบาล</a>
							</li>
							<li>
								<a href="MTS_Wards.aspx"> หอผู้ป่วย</a>
							</li>
						</ul>
					</li>
                    <li class='dropdown'>
						<a href="#" data-toggle="dropdown">กองงบประมาณการเงิน</a>
						<ul class="dropdown-menu">
							<li>
								<a href="MTF_Assets.aspx">สินทรัพย์และหนี้สิน</a>
							</li>
							<li>
								<a href="MTF_Progress.aspx">ผลการดำเนินงาน</a>
							</li>
							<li>
								<a href="MTF_Reception.aspx">การรับ-การจ่าย</a>
							</li>
							<li>
								<a href="MTF_Deposit.aspx">เงินฝาก</a>
							</li>
						</ul>
					</li>
                    <li class='dropdown'>
						<a href="#" data-toggle="dropdown">กองส่งกำลังบำรุง</a>
						<ul class="dropdown-menu">
							<li>
								<a href="MTT_TypeDrug.aspx"> ประเภทยา</a>
							</li>
							<li>
								<a href="MTT_Drug.aspx"> ยา</a>
							</li>
							<li>
								<a href="MTT_DrugNature.aspx">ลักษณะยา</a>
							</li>
							<li>
								<a href="MTT_MedicalSupplies.aspx">เวชภัณฑ์</a>
							</li>
						</ul>
					</li>
                    <li class='dropdown'>
						<a href="#" data-toggle="dropdown">ผู้ใช้งาน (User)</a>
						<ul class="dropdown-menu">
							<li>
								<a href="MT_PersonType.aspx"> ประเภทบุคคล</a>
							</li>
							<li>
								<a href="MT_Person.aspx">บุคคลย่อย</a>
							</li>
							<li>
								<a href="#">กำหนดสิทธิ์</a>
							</li>
							<li>
								<a href="MT_User.aspx">ผู้ใช้งานในระบบ</a>
							</li>
						</ul>
					</li>
                     <li class='dropdown'>
						<a href="#" data-toggle="dropdown">การส่งรายงาน</a>
						<ul class="dropdown-menu">
							<li>
								<a href="MTR_DeadlineReports.aspx"> กำหนดส่งรายงาน</a>
							</li>
							<li>
								<a href="MTR_EditReport.aspx"> แก้ไขข้อมูลรายงาน</a>
							</li>
							<li>
								<a href="MTR_NameReport.aspx"> ชื่อแบบฟอร์มรายงาน</a>
							</li>
						</ul>
					</li>
				</ul>
			</div>
		</div>
		<div id="main">
			<div class="container-fluid">
				<div class="page-header">
					<div class="pull-left">
						<h4>ตรวจสอบการบันทึกข้อมูล</h4>
					</div>
				</div>
				<div class="breadcrumbs">
					<ul>
						<li>
							<a href="Default.aspx">Dashborad</a>
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<a href="forms-basic.html">กองวิทยาการ</a>
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<a href="MT_User.aspx">ตรวจสอบการบันทึกข้อมูล</a>
						</li>
					</ul>
					<div class="close-bread">
						<a href="#"><i class="icon-remove"></i></a>
					</div>
				</div>
				

				<div class="row-fluid">

						<div class="span12">
						  <div class="box">
                              <div class="box-title">
                                <div class='basic-margin'>
                                    <form action="#" method="get" class='form-vertical'>
									 <div class="span2">
										    <div class="control-group">
                                                <label for="textfield" class="control-label">ปีงบประมาณ</label>
                                                   <select name="s2" id="s2" class='select2-me input-small'>
												    <option value="1">ทั้งหมด</option>
												    <option value="2" selected>2560</option>
												    <option value="3">2559</option>
												    <option value="4">2558</option>
												    <option value="5">2557</option>
												    <option value="6">2556</option>
												    <option value="7">2555</option>
											      </select>
												 <%--<label for="textfield" class="control-label">ปีงบประมาณ</label>
                                                    <asp:DropDownList ID="DropDownList1" runat="server" Width="120px">
                                                    <asp:ListItem Text="ทั้งหมด" Value="0"></asp:ListItem>
                                                    <asp:ListItem Text="2560" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="2559" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="2558" Value="3"></asp:ListItem>
                                                    <asp:ListItem Text="2557" Value="4"></asp:ListItem>
                                                </asp:DropDownList>--%>
											    </div>
                                              </div>
										<div class="span3">
                                            <div class="control-group">
                                              <label for="textfield" class="control-label">ประจำเดือน</label>
                                                <select name="s2" id="s2" class='select2-me input-large'>
												    <option value="1">ทั้งหมด</option>
												    <option value="2" selected>มกราคม</option>
												    <option value="3">กุมภาพันธ์</option>
												    <option value="4">มีนาคม</option>
												    <option value="5">เมษายน</option>
												    <option value="6">พฤษภาคม</option>
												    <option value="7">กรกฎาคม</option>
												    <option value="4">สิงหาคม</option>
												    <option value="5">กันยายน</option>
												    <option value="6">ตุลาคม</option>
												    <option value="7">พฤศจิกายน</option>
												    <option value="7">ธันวาคม</option>
											      </select>
												<%--<label for="textfield" class="control-label">ประจำเดือน</label>
                                                <asp:DropDownList ID="DropDownList3" runat="server" Width="200">
                                                <asp:ListItem Text="ทั้งหมด" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="มกราคม" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="กุมภาพันธ์" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="มีนาคม" Value="3"></asp:ListItem>
                                                <asp:ListItem Text="เมษายน" Value="4"></asp:ListItem>
                                                <asp:ListItem Text="พฤษภาคม" Value="5"></asp:ListItem>
                                                <asp:ListItem Text="กรกฎาคม" Value="6"></asp:ListItem>
                                                <asp:ListItem Text="สิงหาคม" Value="7"></asp:ListItem>
                                                <asp:ListItem Text="กันยายน" Value="8"></asp:ListItem>
                                                <asp:ListItem Text="ตุลาคม" Value="9"></asp:ListItem>
                                                <asp:ListItem Text="พฤศจิกายน" Value="10"></asp:ListItem>
                                                <asp:ListItem Text="ธันวาคม" Value="11"></asp:ListItem>
                                            </asp:DropDownList>--%>
											</div>
										</div>
                                        
								</form><br />
								   <div class="btn-group span2 pull-right">
										<a href="#" data-toggle="dropdown" class="btn btn-warning dropdown-toggle"><i class="icon-print"></i> พิมพ์รายงาน <span class="caret"></span></a>
										<ul class="dropdown-menu dropdown-warning">
											<li><a href="#"><i class="icon-file"></i> Excel</a></li>
											<li><a href="#"><i class="icon-file"></i> PDF</a></li>
										</ul>
									</div>
                                </div>
							</div><br />
                           <div class="box-content nopadding">
								<div style="overflow-x:auto;" class="box-content nopadding">
                              
                               
                            
						        <table class="table table-hover table-nomargin table-bordered dataTable-columnfilter dataTable dataTable-noheader table-edit">
										<thead>
										<tr>
											<th style="width: 3px"><p class="text-center">ลำดับ</p></th>
											<th style="width: 85px"><p class="text-center">รายงานหน่วยสายแพทย์</p></th>
											<th style="width: 3px"><p class="text-center">รายงานที่ต้องส่ง</p></th>
											<th style="width: 3px"><p class="text-center">ยืนยันการส่ง</p></th>
											<th style="width: 3px"><p class="text-center">ตรวจสอบ</p></th>
											<th style="width: 3px"><p class="text-center">ผู้ตรวจรายงาน</p></th>
										</tr>
									</thead>
										<tbody>
											<tr>
											<td>1</td>
											<td><a href="Hotpital_Check.aspx">รพ.พระมงกุฎเกล้า</a></td>
											
                                            <td><p class="text-right">12</p></td>
                                            <td><p class="text-right">10</p></td>
                                            <td><p class="text-right">8</p></td>
											<td><p class="text-center">อัครวิน  สุดสาคร</p></td>
										</tr>
											<tr>
											<td>2</td>
											<td>รพ.อานันทมหิดล</td>
                                                
                                            <td><p class="text-right">20</p></td>
                                            <td><p class="text-right">16</p></td>
                                            <td><p class="text-right">7</p></td>
											<td><p class="text-center">อัครวิน  สุดสาคร</p></td>
										</tr>
											<tr>
											<td>3</td>
											<td>รพ.ค่ายธนะรัชต์</td>
											
                                            <td><p class="text-right">16</p></td>
                                            <td><p class="text-right">14</p></td>
                                            <td><p class="text-right">5</p></td>
											<td><p class="text-center">อัครวิน  สุดสาคร</p></td>
										</tr>
											<tr>
											<td>4</td>
											<td>รพ.ค่ายจักรพงษ์ </td>
											
                                            <td><p class="text-right">15</p></td>
                                            <td><p class="text-right">11</p></td>
                                            <td><p class="text-right">6</p></td>
											<td><p class="text-center">อัครวิน  สุดสาคร</p></td>
										</tr>
											
											<tr>
											<td>5</td>
											<td>รพ.ค่ายอดิศร</td>
											
                                            <td><p class="text-right">18</p></td>
                                            <td><p class="text-right">12</p></td>
                                            <td><p class="text-right">4</p></td>
											<td><p class="text-center">อัครวิน  สุดสาคร</p></td>
										</tr>
											<tr>
											<td>6</td>
											<td>รพ.ค่ายสุรสีห์</td>
											
                                            <td><p class="text-right">17</p></td>
                                            <td><p class="text-right">15</p></td>
                                            <td><p class="text-right">8</p></td>
											<td><p class="text-center">อัครวิน  สุดสาคร</p></td>
											<tr>
											<td>7</td>
											<td>รพ.ค่ายภาณุรังษี </td>
											
                                            <td><p class="text-right">13</p></td>
                                            <td><p class="text-right">7</p></td>
                                            <td><p class="text-right">3</p></td>
											<td><p class="text-center">อัครวิน  สุดสาคร</p></td>
										</tr>
											<tr>
											<td>8</td>
											<td>รพ.ค่ายสุรสีห์</td>
											
                                            <td><p class="text-right">12</p></td>
                                            <td><p class="text-right">12</p></td>
                                            <td><p class="text-right">7</p></td>
											<td><p class="text-center">อัครวิน  สุดสาคร</p></td>
										</tr>
											<tr>
											<td>9</td>
											<td>รพ.ค่ายสุรสิงหนาท</td>
											
                                            <td><p class="text-right">15</p></td>
                                            <td><p class="text-right">10</p></td>
                                            <td><p class="text-right">5</p></td>
											<td><p class="text-center">อัครวิน  สุดสาคร</p></td>
										</tr>
											<tr>
											<td>10</td>
											<td>รพ.ค่ายนวมินทราชินี </td>
											
                                            <td><p class="text-right">15</p></td>
                                            <td><p class="text-right">7</p></td>
                                            <td><p class="text-right">6</p></td>
											<td><p class="text-center">อัครวิน  สุดสาคร</p></td>
										</tr>
											<tr>
											<td>11</td>
											<td>รพ.รร.จปร.</td>
											
                                            <td><p class="text-right">12</p></td>
                                            <td><p class="text-right">4</p></td>
                                            <td><p class="text-right">4</p></td>
											<td><p class="text-center">อัครวิน  สุดสาคร</p></td>
										</tr>
											<tr>
											<td>12</td>
											<td>รพ.ค่ายรามราชนิเวศน์</td>
											
                                            <td><p class="text-right">13</p></td>
                                            <td><p class="text-right">6</p></td>
                                            <td><p class="text-right">4</p></td>
											<td><p class="text-center">อัครวิน  สุดสาคร</p></td>
										</tr>
											<tr>
											<td>13</td>
											<td>รพ.ค่ายสุรนารี </td>
											
                                            <td><p class="text-right">10</p></td>
                                            <td><p class="text-right">5</p></td>
                                            <td><p class="text-right">3</p></td>
											<td><p class="text-center">อัครวิน  สุดสาคร</p></td>
										</tr>
											<tr>
											<td>14</td>
											<td>รพ.ค่ายสรรพสิทธิประสงค์</td>
											
                                            <td><p class="text-right">15</p></td>
                                            <td><p class="text-right">3</p></td>
                                            <td><p class="text-right">8</p></td>
											<td><p class="text-center">อัครวิน  สุดสาคร</p></td>
										</tr>
										</tbody>
									</table>
						      </div>
							</div>
						</div>
				   	</div>
                  </div>
			</div>
				
		</div>
	</div>
</asp:Content>









