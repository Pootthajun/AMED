﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="Form_1114.aspx.cs" Inherits="AMED.Form_1114" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <title>รง.ผสต.14 - กรมการแพทย์ทหารบก</title>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <div class="container-fluid" id="content">
		<div id="left">
			<div class="subnav">
				<div class="subnav-title">
					<a href="#" class='toggle-subnav'><i class="icon-angle-down"></i><span><p class="icon-building"></p> หน่วยสายแพทย์</span></a>
				</div>
				<ul class="subnav-menu">
					<li>
						<a href="Default.aspx">สรุปภาพรวม</a>
					</li>
					<li class="active">
							<a href="Hotpital.aspx">การบันทึกข้อมูล</a>
							
						</li>
					<li>
						<a href="Report.aspx">สรุปรายงาน</a>
					</li>
				</ul>
			</div>
			<div class="subnav">
				<div class="subnav-title">
					<a href="#" class='toggle-subnav'><i class="icon-angle-down"></i><span><p class="icon-building"></p>  ส่วนกลาง</span></a>
				</div>
				<ul class="subnav-menu">
					<li>
						<a href="Default.aspx">สรุปภาพรวม</a>
					</li>
					<li>
							<a href="Department.aspx">การตรวจสอบการบันทึกข้อมูล</a>
							
					</li>
					<li>
						<a href="Report.aspx">สรุปรายงาน</a>
					</li>
				</ul>
			</div>
            <div class="subnav">
				<div class="subnav-title">
					<a href="#" class='toggle-subnav'><i class="icon-angle-down"></i><span><p class="icon-cog"></p> ข้อมูลพื้นฐาน</span></a>
				</div>
				<ul class="subnav-menu">
					<li class='dropdown'>
						<a href="#" data-toggle="dropdown"> ข้อมุลทั่วไป</a>
						<ul class="dropdown-menu">
							<li>
								<a href="MT_Region.aspx"> กองทัพภาค</a>
							</li>
							<li>
								<a href="MT_Hospital.aspx"> หน่วยสายแพทย์</a>
							</li>
							<li>
								<a href="MT_Department.aspx"> หน่วยงาน (ส่วนกลาง)</a>
							</li>
							<li>
								<a href="MT_AddOrganization.aspx"> สังกัด</a>
							</li>
							<li>
								<a href="MT_Province.aspx"> จังหวัด</a>
							</li>
						</ul>
					</li>
					<li class='dropdown'>
						<a href="#" data-toggle="dropdown">กองวิทยาการ</a>
						<ul class="dropdown-menu">
							<li>
								<a href="MTS_ICD10.aspx"> รหัสโรค (ICD10)</a>
							</li>
							<li>
								<a href="MTS_GroupRule.aspx"> กลุ่มโรคที่ขัดต่อกฎกระทรวง</a>
							</li>
							<li>
								<a href="MTS_Rule.aspx"> โรคที่ขัดต่อกฎกระทรวง</a>
							</li>
							<li>
								<a href="MTS_Sizebed.aspx"> ขนาดเตียง</a>
							</li>
							<li>
								<a href="MTS_ProHotpital.aspx"> ตำแหน่งในโรงพยาบาล</a>
							</li>
							<li>
								<a href="MTS_Wards.aspx"> หอผู้ป่วย</a>
							</li>
						</ul>
					</li>
                    <li class='dropdown'>
						<a href="#" data-toggle="dropdown">กองงบประมาณการเงิน</a>
						<ul class="dropdown-menu">
							<li>
								<a href="MTF_Assets.aspx">สินทรัพย์และหนี้สิน</a>
							</li>
							<li>
								<a href="MTF_Progress.aspx">ผลการดำเนินงาน</a>
							</li>
							<li>
								<a href="MTF_Reception.aspx">การรับ-การจ่าย</a>
							</li>
							<li>
								<a href="MTF_Deposit.aspx">เงินฝาก</a>
							</li>
						</ul>
					</li>
                    <li class='dropdown'>
						<a href="#" data-toggle="dropdown">กองส่งกำลังบำรุง</a>
						<ul class="dropdown-menu">
							<li>
								<a href="MTT_TypeDrug.aspx"> ประเภทยา</a>
							</li>
							<li>
								<a href="MTT_Drug.aspx"> ยา</a>
							</li>
							<li>
								<a href="MTT_DrugNature.aspx">ลักษณะยา</a>
							</li>
							<li>
								<a href="MTT_MedicalSupplies.aspx">เวชภัณฑ์</a>
							</li>
						</ul>
					</li>
                    <li class='dropdown'>
						<a href="#" data-toggle="dropdown">ผู้ใช้งาน (User)</a>
						<ul class="dropdown-menu">
							<li>
								<a href="MT_PersonType.aspx"> ประเภทบุคคล</a>
							</li>
							<li>
								<a href="MT_Person.aspx">บุคคลย่อย</a>
							</li>
							<li>
								<a href="#">กำหนดสิทธิ์</a>
							</li>
							<li>
								<a href="MT_User.aspx">ผู้ใช้งานในระบบ</a>
							</li>
						</ul>
					</li>
                     <li class='dropdown'>
						<a href="#" data-toggle="dropdown">การส่งรายงาน</a>
						<ul class="dropdown-menu">
							<li>
								<a href="MTR_DeadlineReports.aspx"> กำหนดส่งรายงาน</a>
							</li>
							<li>
								<a href="MTR_EditReport.aspx"> แก้ไขข้อมูลรายงาน</a>
							</li>
							<li>
								<a href="MTR_NameReport.aspx"> ชื่อแบบฟอร์มรายงาน</a>
							</li>
						</ul>
					</li>
				</ul>
			</div>
		</div>
		<div id="main">
			<div class="container-fluid">
				<div class="page-header">
					<div class="pull-left">
						<h4>รายงานสรุปผลการตรวจร่างกายของครอบครัวกำลังพลกองทัพบก ( รง.ผสต.14 )</h4>
					</div>
					
				</div>
				<div class="breadcrumbs">
					<ul>
						<li>
							<a href="Default.aspx">Dashborad</a>
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<a href="forms-basic.html">กองวิทยาการ</a>
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<a href="MT_User.aspx">รง.ผสต.14</a>
						</li>
					</ul>
					<div class="close-bread">
						<a href="#"><i class="icon-remove"></i></a>
					</div>
				</div>
				

				<div class="row-fluid">

						<div class="span12">
						  <div class="box">
							<div class="box-title">
                                <div class='basic-margin'>
								  <div class="fileupload fileupload-new" data-provides="fileupload">
									<div class="input-append">
										<div class="uneditable-input span3"><i class="icon-file fileupload-exists"></i> <span class="fileupload-preview"></span></div><span class="btn btn-file"><span class="fileupload-new">Select file</span><span class="fileupload-exists">Change</span><input type="file" name="aaaa" /></span><a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
								    	</div>
                                    
                                    <div class="btn-group span2 pull-left">
                                        <button class="btn btn-info" rel="tooltip" title="Add"><i class="icon-plus"></i> เพิ่มข้อมูล</button>
                                    </div>
								   <div class="btn-group span2 pull-right">
												<a href="#" data-toggle="dropdown" class="btn btn-warning dropdown-toggle" rel="tooltip" title="print"><i class="icon-print"></i> พิมพ์รายงาน <span class="caret"></span></a>
												<ul class="dropdown-menu dropdown-warning">
													<li>
														<a href="#"><i class="icon-file"></i> Excel</a>
													</li>
													<li>
														<a href="#"><i class="icon-file"></i> PDF</a>
													</li>
												</ul>
											</div></div>
                                </div>
							</div>
                         
                            <br />

                           <div style="overflow-x:auto;" class="box-content nopadding">
                              
                               <form action="#" method="get" class='form-vertical'>
                                    <div class="span1">
											<div class="control-group">
												<label for="textfield" class="control-label">กองทัพ</label>
                                                <asp:DropDownList ID="ddlทั้งหมด" runat="server" Width="90px">
                                                <asp:ListItem Text=" ส่วนกลาง" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="ภาคที่ 2" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="ภาคที่ 3" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="ภาคที่ 4" Value="3"></asp:ListItem>
                                            </asp:DropDownList>
											</div>
                                            
										</div>
										<div class="span3">
                                            <div class="control-group">
												<label for="textfield" class="control-label">หน่วยสายแพทย์</label>
                                                <asp:DropDownList ID="DropDownList2" runat="server" Width="200px">
                                                <asp:ListItem Text="ทั้งหมด" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="รพ.พระมงกุฎเกล้า" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="รพ.อานันทมหิดล" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="รพ.ค่ายสุรนารี" Value="3"></asp:ListItem>
                                            </asp:DropDownList>
											</div>
										</div>
									 <div class="span1">
										    <div class="control-group">
												    <label for="textfield" class="control-label">ปีงบประมาณ</label>
                                                    <asp:DropDownList ID="DropDownList1" runat="server" Width="90px">
                                                    <asp:ListItem Text="ทั้งหมด" Value="0"></asp:ListItem>
                                                    <asp:ListItem Text="2560" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="2559" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="2558" Value="3"></asp:ListItem>
                                                    <asp:ListItem Text="2557" Value="4"></asp:ListItem>
                                                </asp:DropDownList>
											    </div>
                                              </div>
										<div class="span4">
                                            <div class="control-group">
												<label for="textfield" class="control-label">ประจำเดือน</label>
                                                <asp:DropDownList ID="DropDownList3" runat="server" Width="200">
                                                <asp:ListItem Text="ทั้งหมด" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="มกราคม" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="กุมภาพันธ์" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="มีนาคม" Value="3"></asp:ListItem>
                                                <asp:ListItem Text="เมษายน" Value="4"></asp:ListItem>
                                                <asp:ListItem Text="พฤษภาคม" Value="5"></asp:ListItem>
                                                <asp:ListItem Text="กรกฎาคม" Value="6"></asp:ListItem>
                                                <asp:ListItem Text="สิงหาคม" Value="7"></asp:ListItem>
                                                <asp:ListItem Text="กันยายน" Value="8"></asp:ListItem>
                                                <asp:ListItem Text="ตุลาคม" Value="9"></asp:ListItem>
                                                <asp:ListItem Text="พฤศจิกายน" Value="10"></asp:ListItem>
                                                <asp:ListItem Text="ธันวาคม" Value="11"></asp:ListItem>
                                            </asp:DropDownList>
											</div>
										</div>
								</form><br />

								<table class="table table-hover table-nomargin table-condensed table-bordered" style="overflow-x:auto;">
									<thead>
										<tr>
											<th rowspan="3" style="width: 20px"><p class="text-center">ลำดับ</p></th>
											<th rowspan="3" style="width: 250px"><p class="text-center">รายการ</p></th>
											<th colspan="7" style="width: 200px"><p class="text-center">จำนวนกำลังพลกองทัพบก (ราย)</p></th>
										</tr>
										<tr>
											<th colspan="3" style="width: 100px"><p class="text-center">อายุไม่เกิน 35 ปีบริบูรณ์</p></th>
											<th colspan="3" style="width: 100px"><p class="text-center">อายุเกินเกิน 35 ปีบริบูรณ์</p></th>
											<th rowspan="2" style="width: 50px"><p class="text-center">รวม</p></th>
										</tr>
										<tr>
											<th style="width: 50px"><p class="text-center">ชาย</p></th>
											<th style="width: 50px"><p class="text-center">หญิง</p></th>
											<th style="width: 50px"><p class="text-center">รวม</p></th>
											<th style="width: 50px"><p class="text-center">ชาย</p></th>
											<th style="width: 50px"><p class="text-center">หญิง</p></th>
											<th style="width: 50px"><p class="text-center">รวม</p></th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td><p class="text-center">1</p></td>
											<td><b>จำนวนครอบครัวกำลังพลทั้งหมด (ที่มีอยู่จริง)</b></td>
											
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
										</tr>
										
										<tr>
											<td><p class="text-center">2</p></td>
											<td><b>จำนวนครอบครัวกำลังพลที่มารับการตรวจ</b></td>
											
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
										</tr>
										
										<tr>
											<td><p class="text-center">3</p></td>
											<td><b>จำนวนครอบครัวกำลังพลที่มารับการตรวจ จำแนกตามชั้นยศ</b></td>
											
											<th>&nbsp; &nbsp;</th>
                                            <th>&nbsp; &nbsp;</th>
                                            <th>&nbsp; &nbsp;</th>
											<th>&nbsp; &nbsp;</th>
                                            <th>&nbsp; &nbsp;</th>
                                            <th>&nbsp; &nbsp;</th>
                                            <th>&nbsp; &nbsp;</th>
										</tr>
										
										<tr>
											<td><p class="text-center"></p></td>
											<td>3.1 นายทหารชั้นสัญญาบัตร (ไม่ต้องใส่ข้อมูล)</td>
											
											<th>&nbsp; &nbsp;</th>
                                            <th>&nbsp; &nbsp;</th>
                                            <th>&nbsp; &nbsp;</th>
											<th>&nbsp; &nbsp;</th>
                                            <th>&nbsp; &nbsp;</th>
                                            <th>&nbsp; &nbsp;</th>
                                            <th>&nbsp; &nbsp;</th>
										</tr>
										
										<tr>
											<td><p class="text-center"></p></td>
											<td>3.2 นายทหารชั้นประทวน (ไม่ต้องใส่ข้อมูล)</td>
											
											<th>&nbsp; &nbsp;</th>
                                            <th>&nbsp; &nbsp;</th>
                                            <th>&nbsp; &nbsp;</th>
											<th>&nbsp; &nbsp;</th>
                                            <th>&nbsp; &nbsp;</th>
                                            <th>&nbsp; &nbsp;</th>
                                            <th>&nbsp; &nbsp;</th>
										</tr>
										
										<tr>
											<td><p class="text-center"></p></td>
											<td>3.3 ลูกจ้างประจำ (ไม่ต้องใส่ข้อมูล)</td>
											
											<th>&nbsp; &nbsp;</th>
                                            <th>&nbsp; &nbsp;</th>
                                            <th>&nbsp; &nbsp;</th>
											<th>&nbsp; &nbsp;</th>
                                            <th>&nbsp; &nbsp;</th>
                                            <th>&nbsp; &nbsp;</th>
                                            <th>&nbsp; &nbsp;</th>
										</tr>
										
										<tr>
											<td><p class="text-center">4</p></td>
											<td><b>จำนวนครอบครัวกำลังพลที่มารับการตรวจ จำแนกตามค่าดัชนีมวลกาย (BMI)</b></td>
											
											<th>&nbsp; &nbsp;</th>
                                            <th>&nbsp; &nbsp;</th>
                                            <th>&nbsp; &nbsp;</th>
											<th>&nbsp; &nbsp;</th>
                                            <th>&nbsp; &nbsp;</th>
                                            <th>&nbsp; &nbsp;</th>
                                            <th>&nbsp; &nbsp;</th>
										</tr>
										
										<tr>
											<td><p class="text-center"></p></td>
											<td>4.1 น้ำหนักน้อยกว่าปกติ ( BMI < 18.5 kg/m2 )</td>
											
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
										</tr>
										
										<tr>
											<td><p class="text-center"></p></td>
											<td> 4.2 น้ำหนักปกติ ( BMI = 18.5-22.9 kg/m2 )</td>
											
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
										</tr>
										
										<tr>
											<td><p class="text-center"></p></td>
											<td>4.3 น้ำหนักเกิน ระดับ1 ( BMI = 23.0-24.9 kg/m2 )</td>
											
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
										</tr>

										<tr>
											<td><p class="text-center"></p></td>
											<td> 4.4 น้ำหนักเกิน ระดับ2 ( BMI = 25.0-29.9 kg/m2 )</td>
											
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
										</tr>

										<tr>
											<td><p class="text-center"></p></td>
											<td> 4.5 ภาวะอ้วน ( BMI >= 30 kg/m2 )</td>
											
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
										</tr>
                                        
										<tr>
											<td><p class="text-center">5</p></td>
											<td><b>ความดันโลหิตผิดปกติ (Hypertension)</b></td>
											
											<th>&nbsp; &nbsp;</th>
                                            <th>&nbsp; &nbsp;</th>
                                            <th>&nbsp; &nbsp;</th>
											<th>&nbsp; &nbsp;</th>
                                            <th>&nbsp; &nbsp;</th>
                                            <th>&nbsp; &nbsp;</th>
                                            <th>&nbsp; &nbsp;</th>
										</tr>

										<tr>
											<td><p class="text-center"></p></td>
											<td>5.1 Systolic ผิดปกติ ( Systolic > 140 mmHg ) (อย่างเดียว)</td>
											
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>

										<tr>
											<td><p class="text-center"></p></td>
											<td>5.2 Diastolic ผิดปกติ ( Diastolic > 90 mmHg ) (อย่างเดียว)</td>
											
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
										</tr>

										<tr>
											<td><p class="text-center"></p></td>
											<td>5.3 Systolic ผิดปกติ (>140 mmHg) และ Diastolic ผิดปกติ (> 90 mmHg)</td>
											
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
										</tr>

										<tr>
											<td><p class="text-center"></p></td>
											<td>5.4 Systolic ผิดปกติ (>140 mmHg) และ Diastolic ผิดปกติ (> 90 mmHg)<br />(ไม่รวม ข้อ 5.1 และ ข้อ 5.2)</td>
											
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
										</tr>

										<tr>
											<td><p class="text-center">6</p></td>
											<td><b>Chest x-ray ผิดปกติ</b></td>
											
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
										</tr>

										<tr>
											<td><p class="text-center">7</p></td>
											<td><b>Urine examination ผิดปกติ</b></td>
											
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
										</tr>

										<tr>
											<td><p class="text-center">8</p></td>
											<td><b>Glucose ผิดปกติ (DM)  ( Glucose > 126 mg/dL )</b></td>
											
											<th>&nbsp; &nbsp;</th>
                                            <th>&nbsp; &nbsp;</th>
                                            <th>&nbsp; &nbsp;</th>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
										</tr>

										<tr>
											<td><p class="text-center">9</p></td>
											<td><b>ภาวะไขมันในเลือดสูง</b></td>
											
											<th>&nbsp; &nbsp;</th>
                                            <th>&nbsp; &nbsp;</th>
                                            <th>&nbsp; &nbsp;</th>
											<th>&nbsp; &nbsp;</th>
                                            <th>&nbsp; &nbsp;</th>
                                            <th>&nbsp; &nbsp;</th>
                                            <th>&nbsp; &nbsp;</th>
										</tr>
										<tr>
											<td><p class="text-center"></p></td>
											<td>9.1 Total Cholesterol  > 240 mg/dL (อย่างเดียว)</td>
											
                                            <th>&nbsp; &nbsp;</th>
                                            <th>&nbsp; &nbsp;</th>
                                            <th>&nbsp; &nbsp;</th>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
										</tr>
										<tr>
											<td><p class="text-center"></p></td>
											<td>9.2 Triglycerides > 200 mg/dL (อย่างเดียว)</td>
											
                                            <th>&nbsp; &nbsp;</th>
                                            <th>&nbsp; &nbsp;</th>
                                            <th>&nbsp; &nbsp;</th>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
										</tr>
										<tr>
											<td><p class="text-center"></p></td>
											<td> 9.3 Total Cholesterol  > 240 mg/dL และ Triglycerides >200 mg/dL<br /> (ไม่รวม ข้อ 9.1 และ ข้อ 9.2)</td>
                                            <th>&nbsp; &nbsp;</th>
                                            <th>&nbsp; &nbsp;</th>
                                            <th>&nbsp; &nbsp;</th>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
										</tr>
										<tr>
											<td><p class="text-center">10</p></td>
											<td><b>ผลตรวจมะเร็งปากมดลูก ( Pap-smear ) ผิดปกติ</b></td>
											
											<th>&nbsp; &nbsp;</th>
                                            <th>&nbsp; &nbsp;</th>
                                            <th>&nbsp; &nbsp;</th>
                                            <th>&nbsp; &nbsp;</th>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
										</tr>
										<tr>
											<td><p class="text-center">11</p></td>
											<td><b>ประวัติโรคประจำตัวของผู้มารับการตรวจ</b></td>
											
											<th>&nbsp; &nbsp;</th>
                                            <th>&nbsp; &nbsp;</th>
                                            <th>&nbsp; &nbsp;</th>
											<th>&nbsp; &nbsp;</th>
                                            <th>&nbsp; &nbsp;</th>
                                            <th>&nbsp; &nbsp;</th>
                                            <th>&nbsp; &nbsp;</th>
										</tr>
										<tr>
											<td><p class="text-center"></p></td>
											<td><b> 11.1   ความดันโลหิตสูง</b></td>
											
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
										</tr>
										<tr>
											<td><p class="text-center"></p></td>
											<td> 11.2   เบาหวาน</td>
											
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
										</tr>
										<tr>
											<td><p class="text-center"></p></td>
											<td>11.3   โรคหัวใจและหลอดเลือด</td>
											
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
										</tr>
										<tr>
											<td><p class="text-center"></p></td>
											<td> 11.4   ไขมันในเลือดสูง</td>
											
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
										</tr>
										<tr>
											<td><p class="text-center"></p></td>
											<td>11.5   โรคประจำตัว 4 โรคที่กำหนดไว้  ตั้งแต่ 2 โรคขึ้นไป</td>
											
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
										</tr>
										<tr>
											<td><p class="text-center"></p></td>
											<td>11.6   โรคประจำตัวอื่นๆ นอกจากโรคประจำตัว 4 โรคที่กำหนดไว้</td>
											
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num11" placeholder="0" class="input-block-level text-right"></td>
										</tr>
                                        <tr>
                                            <td colspan="9">
									        <div class="form-actions pull-right">
										        <button type="submit" class="btn btn-green"><i class="circle_ok text-success"></i> บันทึกข้อมูล</button>
										        <button type="submit" class="btn btn-green"><i class="circle_ok text-success"></i> ยืนยันและส่งข้อมูล</button>
										        <button type="button" class="btn btn-lightred"><i class="circle_remove text-warning"></i> ยกเลิก</button>
									        </div>
                                            </td>
                                        </tr>
									</tbody>
								</table>
							</div>
						</div>
				   	</div>

				</div>
				
			</div>
		</div></div>
</asp:Content>










