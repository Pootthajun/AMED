﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="Form_1119.aspx.cs" Inherits="AMED.Form_1119" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <title>รง.ผสต.19 - กรมการแพทย์ทหารบก</title>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <div class="container-fluid" id="content">
		<div id="left">
			<div class="subnav">
				<div class="subnav-title">
					<a href="#" class='toggle-subnav'><i class="icon-angle-down"></i><span><p class="icon-building"></p> หน่วยสายแพทย์</span></a>
				</div>
				<ul class="subnav-menu">
					<li>
						<a href="Default.aspx">สรุปภาพรวม</a>
					</li>
					<li class="active">
							<a href="Hotpital.aspx">การบันทึกข้อมูล</a>
							
						</li>
					<li>
						<a href="Report.aspx">สรุปรายงาน</a>
					</li>
				</ul>
			</div>
			<div class="subnav">
				<div class="subnav-title">
					<a href="#" class='toggle-subnav'><i class="icon-angle-down"></i><span><p class="icon-building"></p>  ส่วนกลาง</span></a>
				</div>
				<ul class="subnav-menu">
					<li>
						<a href="Default.aspx">สรุปภาพรวม</a>
					</li>
					<li>
							<a href="Department.aspx">การตรวจสอบการบันทึกข้อมูล</a>
							
					</li>
					<li>
						<a href="Report.aspx">สรุปรายงาน</a>
					</li>
				</ul>
			</div>
            <div class="subnav">
				<div class="subnav-title">
					<a href="#" class='toggle-subnav'><i class="icon-angle-down"></i><span><p class="icon-cog"></p> ข้อมูลพื้นฐาน</span></a>
				</div>
				<ul class="subnav-menu">
					<li class='dropdown'>
						<a href="#" data-toggle="dropdown"> ข้อมุลทั่วไป</a>
						<ul class="dropdown-menu">
							<li>
								<a href="MT_Region.aspx"> กองทัพภาค</a>
							</li>
							<li>
								<a href="MT_Hospital.aspx"> หน่วยสายแพทย์</a>
							</li>
							<li>
								<a href="MT_Department.aspx"> หน่วยงาน (ส่วนกลาง)</a>
							</li>
							<li>
								<a href="MT_AddOrganization.aspx"> สังกัด</a>
							</li>
							<li>
								<a href="MT_Province.aspx"> จังหวัด</a>
							</li>
						</ul>
					</li>
					<li class='dropdown'>
						<a href="#" data-toggle="dropdown">กองวิทยาการ</a>
						<ul class="dropdown-menu">
							<li>
								<a href="MTS_ICD10.aspx"> รหัสโรค (ICD10)</a>
							</li>
							<li>
								<a href="MTS_GroupRule.aspx"> กลุ่มโรคที่ขัดต่อกฎกระทรวง</a>
							</li>
							<li>
								<a href="MTS_Rule.aspx"> โรคที่ขัดต่อกฎกระทรวง</a>
							</li>
							<li>
								<a href="MTS_Sizebed.aspx"> ขนาดเตียง</a>
							</li>
							<li>
								<a href="MTS_ProHotpital.aspx"> ตำแหน่งในโรงพยาบาล</a>
							</li>
							<li>
								<a href="MTS_Wards.aspx"> หอผู้ป่วย</a>
							</li>
						</ul>
					</li>
                    <li class='dropdown'>
						<a href="#" data-toggle="dropdown">กองงบประมาณการเงิน</a>
						<ul class="dropdown-menu">
							<li>
								<a href="MTF_Assets.aspx">สินทรัพย์และหนี้สิน</a>
							</li>
							<li>
								<a href="MTF_Progress.aspx">ผลการดำเนินงาน</a>
							</li>
							<li>
								<a href="MTF_Reception.aspx">การรับ-การจ่าย</a>
							</li>
							<li>
								<a href="MTF_Deposit.aspx">เงินฝาก</a>
							</li>
						</ul>
					</li>
                    <li class='dropdown'>
						<a href="#" data-toggle="dropdown">กองส่งกำลังบำรุง</a>
						<ul class="dropdown-menu">
							<li>
								<a href="MTT_TypeDrug.aspx"> ประเภทยา</a>
							</li>
							<li>
								<a href="MTT_Drug.aspx"> ยา</a>
							</li>
							<li>
								<a href="MTT_DrugNature.aspx">ลักษณะยา</a>
							</li>
							<li>
								<a href="MTT_MedicalSupplies.aspx">เวชภัณฑ์</a>
							</li>
						</ul>
					</li>
                    <li class='dropdown'>
						<a href="#" data-toggle="dropdown">ผู้ใช้งาน (User)</a>
						<ul class="dropdown-menu">
							<li>
								<a href="MT_PersonType.aspx"> ประเภทบุคคล</a>
							</li>
							<li>
								<a href="MT_Person.aspx">บุคคลย่อย</a>
							</li>
							<li>
								<a href="#">กำหนดสิทธิ์</a>
							</li>
							<li>
								<a href="MT_User.aspx">ผู้ใช้งานในระบบ</a>
							</li>
						</ul>
					</li>
                     <li class='dropdown'>
						<a href="#" data-toggle="dropdown">การส่งรายงาน</a>
						<ul class="dropdown-menu">
							<li>
								<a href="MTR_DeadlineReports.aspx"> กำหนดส่งรายงาน</a>
							</li>
							<li>
								<a href="MTR_EditReport.aspx"> แก้ไขข้อมูลรายงาน</a>
							</li>
							<li>
								<a href="MTR_NameReport.aspx"> ชื่อแบบฟอร์มรายงาน</a>
							</li>
						</ul>
					</li>
				</ul>
			</div>
		</div>
		<div id="main">
			<div class="container-fluid">
				<div class="page-header">
					<div class="pull-left">
						<h4>รายงานจำนวนแพทย์ จำแนกตามสาขาของการแพทย์เฉพาะทาง ( รง.ผสต.19 )</h4>
					</div>
					
				</div>
				<div class="breadcrumbs">
					<ul>
						<li>
							<a href="Default.aspx">Dashborad</a>
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<a href="forms-basic.html">กองวิทยาการ</a>
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<a href="MT_User.aspx">รง.ผสต.19</a>
						</li>
					</ul>
					<div class="close-bread">
						<a href="#"><i class="icon-remove"></i></a>
					</div>
				</div>
				

				<div class="row-fluid">

						<div class="span12">
						  <div class="box">
							<div class="box-title">
                                <div class='basic-margin'>
								<div class="fileupload fileupload-new" data-provides="fileupload">
									<div class="input-append">
										<div class="uneditable-input span3"><i class="icon-file fileupload-exists"></i> <span class="fileupload-preview"></span></div><span class="btn btn-file"><span class="fileupload-new">Select file</span><span class="fileupload-exists">Change</span><input type="file" name="aaaa" /></span><a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
								    	</div>
								   <div class="btn-group span2 pull-right">
                                  <%-- <button class="btn btn-info" rel="tooltip" title="edit"><i class="icon-edit"></i> แก้ไขเอกสาร</button>--%>
												<a href="#" data-toggle="dropdown" class="btn btn-warning dropdown-toggle" rel="tooltip" title="print"><i class="icon-print"></i> พิมพ์รายงาน <span class="caret"></span></a>
												<ul class="dropdown-menu dropdown-warning">
													<li>
														<a href="#"><i class="icon-file"></i> Excel</a>
													</li>
													<li>
														<a href="#"><i class="icon-file"></i> PDF</a>
													</li>
												</ul>
											</div></div>
                                </div>
							</div>
                            <br />

                           <div style="overflow-x:auto;" class="box-content nopadding"><br />
                               <form action="#" method="get" class='form-vertical'>
                                    <div class="span2">
											<div class="control-group">
												<label for="textfield" class="control-label">กองทัพ</label>
                                                <asp:DropDownList ID="ddlทั้งหมด" runat="server" Width="150px">
                                                <asp:ListItem Text="หน่วยงาน (ส่วนกลาง)" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="กองทัพภาคที่ 2" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="กองทัพภาคที่ 3" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="กองทัพภาคที่ 4" Value="3"></asp:ListItem>
                                            </asp:DropDownList>
											</div>
                                            
										</div>
										<div class="span3">
                                            <div class="control-group">
												<label for="textfield" class="control-label">หน่วยสายแพทย์</label>
                                                <asp:DropDownList ID="DropDownList2" runat="server" Width="230px">
                                                <asp:ListItem Text="ทั้งหมด" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="รพ.พระมงกุฎเกล้า" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="รพ.อานันทมหิดล" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="รพ.ค่ายสุรนารี" Value="3"></asp:ListItem>
                                            </asp:DropDownList>
											</div>
											
										</div>
                                          <div class="span2">
										    <div class="control-group">
												    <label for="textfield" class="control-label">ปีงบประมาณ</label>
                                                    <asp:DropDownList ID="DropDownList4" runat="server" Width="150px">
                                                    <asp:ListItem Text="ทั้งหมด" Value="0"></asp:ListItem>
                                                    <asp:ListItem Text="2560" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="2559" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="2558" Value="3"></asp:ListItem>
                                                    <asp:ListItem Text="2557" Value="4"></asp:ListItem>
                                                </asp:DropDownList>
											    </div>
                                              </div>
										<div class="span3">
                                            <div class="control-group">
												<label for="textfield" class="control-label">ประจำเดือน</label>
                                                <asp:DropDownList ID="DropDownList5" runat="server" Width="230px">
                                                <asp:ListItem Text="ทั้งหมด" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="มกราคม" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="กุมภาพันธ์" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="มีนาคม" Value="3"></asp:ListItem>
                                                <asp:ListItem Text="เมษายน" Value="4"></asp:ListItem>
                                                <asp:ListItem Text="พฤษภาคม" Value="5"></asp:ListItem>
                                                <asp:ListItem Text="กรกฎาคม" Value="6"></asp:ListItem>
                                                <asp:ListItem Text="สิงหาคม" Value="7"></asp:ListItem>
                                                <asp:ListItem Text="กันยายน" Value="8"></asp:ListItem>
                                                <asp:ListItem Text="ตุลาคม" Value="9"></asp:ListItem>
                                                <asp:ListItem Text="พฤศจิกายน" Value="10"></asp:ListItem>
                                                <asp:ListItem Text="ธันวาคม" Value="11"></asp:ListItem>
                                            </asp:DropDownList>
											</div>
										</div>
									
								</form>
								 <table class="table table-hover table-nomargin table-bordered dataTable dataTable-fixedcolumn  dataTable-noheader dataTable-scroll-x dataTable-scroll-y table-edit">
									<thead>
										<tr>
											<th style="width: 20px"><p class="text-center">ลำดับ</p></th>
											<th style="width: 200px"><p class="text-center">สาขาของการแพทย์เฉพาะทาง</p></th>
											<th style="width: 50px"><p class="text-center">จำนวนแพทย์ที่บรรจุ</p></th>
											<th style="width: 50px"><p class="text-center">จำนวนแพทย์ที่มาชรก.</p></th>
											<th style="width: 50px"><p class="text-center">จำนวนแพทย์ที่ไปชรก.</p></th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td><p class="text-center">1.01</p></td>
											<td>หอผู้ป่วยศัลยกรรม (Surgical Ward)</td>
											
											<td><asp:TextBox ID="TextBox1" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox2" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox3" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
										</tr>
										<tr>
											<td><p class="text-center">1.02</p></td>
											<td>สาขาอายุรศาสตร์โรคเลือด</td>
											
											<td><asp:TextBox ID="TextBox4" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox5" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox6" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
										</tr>
										<tr>
											<td><p class="text-center">1.03</p></td>
											<td>สาขาอายุรศาสตร์ผู้สูงอายุ</td>
											
											<td><asp:TextBox ID="TextBox7" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox8" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox9" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
										</tr>
										<tr>
											<td><p class="text-center">1.04</p></td>
											<td>สาขาประสาทวิทยา</td>
											
											<td><asp:TextBox ID="TextBox10" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox11" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox12" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
										</tr>
										<tr>
											<td><p class="text-center">1.05</p></td>
											<td>สาขาตจวิทยา</td>
											
											<td><asp:TextBox ID="TextBox13" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox14" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox15" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
										</tr>
										<tr>
											<td><p class="text-center">1.06</p></td>
											<td>สาขาเวชศาสตร์ฉุกเฉิน</td>
											
											<td><asp:TextBox ID="TextBox16" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox17" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox18" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
										</tr>
										<tr>
											<td><p class="text-center">1.07</p></td>
											<td>สาขาอายุรศาสตร์โรคติดเชื้อ</td>
											
											<td><asp:TextBox ID="TextBox19" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox20" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox21" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
										</tr>
										<tr>
											<td><p class="text-center">1.08</p></td>
											<td>สาขาอายุรศาสตร์โรคข้อและรูมาติสซั่ม</td>
											
											<td><asp:TextBox ID="TextBox22" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox23" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox24" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
										</tr>
										<tr>
											<td><p class="text-center">1.09</p></td>
											<td>สาขาอายุรศาสตร์โรคต่อมไร้ท่อและเมตาบอลิสม</td>
											
											<td><asp:TextBox ID="TextBox25" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox26" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox27" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
										</tr>
										<tr>
											<td><p class="text-center">1.10</p></td>
											<td>สาขาอายุรศาสตร์โรคภูมิแพ้และภูมิคุ้มกันทางคลินิก</td>
											
											<td><asp:TextBox ID="TextBox28" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox29" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox30" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
										</tr>
										<tr>
											<td><p class="text-center">1.11</p></td>
											<td>สาขาอายุรศาสตร์โรคระบบทางเดินอาหาร</td>
											
											<td><asp:TextBox ID="TextBox31" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox32" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox33" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
										</tr>
										<tr>
											<td><p class="text-center">1.12</p></td>
											<td>สาขาอายุรศาสตร์โรคไต</td>
											
											<td><asp:TextBox ID="TextBox34" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox35" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox36" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
										</tr>
										<tr>
											<td><p class="text-center">1.13</p></td>
											<td>สาขาอายุรศาสตร์โรคหัวใจ</td>
											
											<td><asp:TextBox ID="TextBox37" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox38" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox39" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
										</tr>
										<tr>
											<td><p class="text-center">1.14</p></td>
											<td>สาขาอายุรศาสตร์โรคระบบการหายใจและภาวะวิกฤตโรคระบบการหายใจ</td>
											
											<td><asp:TextBox ID="TextBox40" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox41" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox42" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
										</tr>
										<tr>
											<td><p class="text-center">1.15</p></td>
											<td>สาขาเวชบำบัดวิกฤต</td>
											
											<td><asp:TextBox ID="TextBox43" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox44" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox45" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
										</tr>
										<tr>
											<td><p class="text-center">1.16</p></td>
											<td>สาขาการระงับปวด</td>
											
											<td><asp:TextBox ID="TextBox46" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox47" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox48" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
										</tr>
										<tr>
											<td><p class="text-center">1.17</p></td>
											<td>สาขาโภชนศาสตร์คลินิก</td>
											
											<td><asp:TextBox ID="TextBox49" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox50" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox51" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
										</tr>
										<tr>
											<td><p class="text-center">1.18</p></td>
											<td>สาขาเวชเภสัชวิทยาและพิษวิทยา</td>
											
											<td><asp:TextBox ID="TextBox52" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox53" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox54" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
										</tr>
										<tr>
											<td><p class="text-center">1.19</p></td>
											<td>สาขาอายุรศาสตร์มะเร็งวิทยา</td>
											
											<td><asp:TextBox ID="TextBox55" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox56" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox57" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
										</tr>
                                        
										<%--<tr>
											<td colspan="5"><b>2. ศัลยแพทย์</b></td>
										</tr>--%>
                                        <tr>
											<td><p class="text-center">2.01</p></td>
											<td>สาขาศัลยศาสตร์ทั่วไป</td>
											
											<td><asp:TextBox ID="TextBox58" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox59" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox60" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
										</tr>
										<tr>
											<td><p class="text-center">2.02</p></td>
											<td>สาขาประสาทศัลยศาสตร์</td>
											
											<td><asp:TextBox ID="TextBox61" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox62" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox63" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
										</tr>
										<tr>
											<td><p class="text-center">2.03</p></td>
											<td>สาขาศัลยศาสตร์ตกแต่ง</td>
											
											<td><asp:TextBox ID="TextBox64" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox65" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox66" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
										</tr>
										<tr>
											<td><p class="text-center">2.04</p></td>
											<td>สาขาศัลยศาสตร์ทรวงอก</td>
											
											<td><asp:TextBox ID="TextBox67" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox68" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox69" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
										</tr>
										<tr>
											<td><p class="text-center">2.05</p></td>
											<td>สาขาศัลยศาสตร์ยูโรวิทยา</td>
											
											<td><asp:TextBox ID="TextBox70" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox71" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox72" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
										</tr>
										<tr>
											<td><p class="text-center">2.06</p></td>
											<td>สาขากุมารศัลยศาสตร์</td>
											
											<td><asp:TextBox ID="TextBox73" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox74" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox75" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
										</tr>
										<tr>
											<td><p class="text-center">2.07</p></td>
											<td>สาขาศัลยศาสตร์ลำไส้ใหญ่และทวารหนัก</td>
											
											<td><asp:TextBox ID="TextBox76" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox77" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox78" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
										</tr>
										<tr>
											<td><p class="text-center">2.08</p></td>
											<td>สาขาศัลยศาสตร์หลอดเลือด</td>
											
											<td><asp:TextBox ID="TextBox79" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox80" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox81" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
										</tr>
										<tr>
											<td><p class="text-center">2.09</p></td>
											<td>สาขาศัลยศาสตร์อุบัติเหตุ</td>
											
											<td><asp:TextBox ID="TextBox82" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox83" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox84" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
										</tr>
										<tr>
											<td><p class="text-center">2.10</p></td>
											<td>สาขาศัลยศาสตร์มะเร็งวิทยา</td>
											
											<td><asp:TextBox ID="TextBox85" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox86" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox87" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
										</tr>
										<tr>
											<td><p class="text-center">2.11</p></td>
											<td>สาขาศัลยศาสตร์ออร์โธปิดิกส์</td>
											
											<td><asp:TextBox ID="TextBox88" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox89" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox90" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
										</tr>
										<tr>
											<td><p class="text-center">2.12</p></td>
											<td>สาขาศัลยศาสตร์ตกแต่งและเสริมสร้างใบหน้า</td>
											
											<td><asp:TextBox ID="TextBox91" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox92" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox93" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
										</tr>
										<tr>
											<td><p class="text-center">2.13</p></td>
											<td>สาขาออร์โธปิดิกส์บูรณสภาพ</td>
											
											<td><asp:TextBox ID="TextBox94" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox95" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox96" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
										</tr>

										<%--<tr>
											<td colspan="5"><b>3. สูติ-นรีแพทย์</b></td>
										</tr>--%>
                                        <tr>
											<td><p class="text-center">3.01</p></td>
											<td>สาขาสูติศาสตร์-นรีเวชวิทยาทั่วไป</td>
											
											<td><asp:TextBox ID="TextBox97" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox98" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox99" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
										</tr>
										<tr>
											<td><p class="text-center">3.02</p></td>
											<td>สาขาเวชศาสตร์มารดาและทารกในครรภ์</td>
											
											<td><asp:TextBox ID="TextBox100" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox101" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox102" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
										</tr>
										<tr>
											<td><p class="text-center">3.03</p></td>
											<td>สาขามะเร็งนรีเวชวิทยา</td>
											
											<td><asp:TextBox ID="TextBox103" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox104" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox105" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
										</tr>
										<tr>
											<td><p class="text-center">3.04</p></td>
											<td>สาขาเวชศาสตร์การเจริญพันธุ์</td>
											
											<td><asp:TextBox ID="TextBox106" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox107" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox108" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
										</tr>

                                        <%--<tr>
											<td colspan="5"><b>4. กุมารแพทย์</b></td>
										</tr>--%>
                                        <tr>
											<td><p class="text-center">4.01</p></td>
											<td>สาขากุมารเวชศาสตร์ทั่วไป</td>
											
											<td><asp:TextBox ID="TextBox109" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox110" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox111" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
										</tr>
										<tr>
											<td><p class="text-center">4.02</p></td>
											<td>สาขากุมารเวชศาสตร์โรคหัวใจ</td>
											
											<td><asp:TextBox ID="TextBox112" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox113" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox114" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
										</tr>
										<tr>
											<td><p class="text-center">4.03</p></td>
											<td>สาขากุมารเวชศาสตร์โรคระบบการหายใจ</td>
											
											<td><asp:TextBox ID="TextBox115" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox116" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox117" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
										</tr>
										<tr>
											<td><p class="text-center">4.04</p></td>
											<td>สาขากุมารเวชศาสตร์โรคต่อมไร้ท่อและเมตาบอลิสม</td>
											
											<td><asp:TextBox ID="TextBox118" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox119" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox120" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
										</tr>
										<tr>
											<td><p class="text-center">4.05</p></td>
											<td>สาขากุมารเวชศาสตร์พัฒนาการและพฤติกรรม</td>
											
											<td><asp:TextBox ID="TextBox121" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox122" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox123" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
										</tr>
										<tr>
											<td><p class="text-center">4.06</p></td>
											<td>สาขากุมารเวชศาสตร์โรคไต</td>
											
											<td><asp:TextBox ID="TextBox124" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox125" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox126" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
										</tr>
										<tr>
											<td><p class="text-center">4.07</p></td>
											<td>สาขากุมารเวชศาสตร์โรคติดเชื้อ</td>
											
											<td><asp:TextBox ID="TextBox127" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox128" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox129" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
										</tr>
										<tr>
											<td><p class="text-center">4.08</p></td>
											<td>สาขากุมารเวชศาสตร์โรคทางเดินอาหารและโรคตับ</td>
											
											<td><asp:TextBox ID="TextBox130" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox131" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox132" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
										</tr>
										<tr>
											<td><p class="text-center">4.09</p></td>
											<td>สาขากุมารเวชศาสตร์ประสาทวิทยา</td>
											
											<td><asp:TextBox ID="TextBox133" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox134" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox135" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
										</tr>
										<tr>
											<td><p class="text-center">4.10</p></td>
											<td>สาขากุมารเวชศาสตร์โรคภูมิแพ้และภูมิคุ้มกัน</td>
											
											<td><asp:TextBox ID="TextBox136" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox137" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox138" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
										</tr>
										<tr>
											<td><p class="text-center">4.11</p></td>
											<td>สาขากุมารเวชศาสตร์โรคโลหิตวิทยาและมะเร็งในเด็ก</td>
											
											<td><asp:TextBox ID="TextBox139" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox140" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox141" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
										</tr>
										<tr>
											<td><p class="text-center">4.12</p></td>
											<td>สาขากุมารเวชศาสตร์ทารกแรกเกิดและปริกำเนิด</td>
											
											<td><asp:TextBox ID="TextBox142" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox143" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox144" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
										</tr>
										<tr>
											<td><p class="text-center">4.13</p></td>
											<td>สาขากุมารเวชศาสตร์ตจวิทยา</td>
											
											<td><asp:TextBox ID="TextBox145" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox146" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox147" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
										</tr>
										<tr>
											<td><p class="text-center">4.14</p></td>
											<td>สาขากุมารเวชศาสตร์โภชนาการ</td>
											
											<td><asp:TextBox ID="TextBox148" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox149" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox150" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
										</tr>
                                        
										<%--<tr>
											<td colspan="5"><b>5. แพทย์จักษุ โสต ศอ นาสิก</b></td>
										</tr>--%>
                                        <tr>
											<td><p class="text-center">5.01</p></td>
											<td>สาขาจักษุวิทยา</td>
											
											<td><asp:TextBox ID="TextBox151" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox152" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox153" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
										</tr>
										<tr>
											<td><p class="text-center">5.02</p></td>
											<td>สาขาโสต ศอ นาสิกวิทยา</td>
											
											<td><asp:TextBox ID="TextBox154" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox155" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox156" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
										</tr>
                                        
                                        
										<%--<tr>
											<td colspan="5"><b>6. จิตแพทย์</b></td>
										</tr>--%>
                                        <tr>
											<td><p class="text-center">6.01</p></td>
											<td>สาขาจิตเวชศาสตร์ทั่วไป</td>
											
											<td><asp:TextBox ID="TextBox157" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox158" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox159" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
										</tr>
										<tr>
											<td><p class="text-center">6.02</p></td>
											<td>สาขาจิตเวชศาสตร์เด็กและวัยรุ่น</td>
											
											<td><asp:TextBox ID="TextBox160" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox161" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox162" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
										</tr>
                                        
										<%--<tr>
											<td colspan="5"><b>7. พยาธิแพทย์</b></td>
										</tr>--%>
                                        <tr>
											<td><p class="text-center">7.01</p></td>
											<td>สาขาพยาธิวิทยาทั่วไป</td>
											
											<td><asp:TextBox ID="TextBox163" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox164" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox165" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
										</tr>
										<tr>
											<td><p class="text-center">7.02</p></td>
											<td>สาขาพยาธิวิทยากายวิภาค</td>
											
											<td><asp:TextBox ID="TextBox166" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox167" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox168" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
										</tr>
                                        <tr>
											<td><p class="text-center">7.03</p></td>
											<td>สาขาพยาธิวิทยาคลินิก</td>
											
											<td><asp:TextBox ID="TextBox169" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox170" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox171" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
										</tr>
										<tr>
											<td><p class="text-center">7.04</p></td>
											<td>สาขานิติเวชศาสตร์</td>
											
											<td><asp:TextBox ID="TextBox172" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox173" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox174" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
										</tr>
                                        <tr>
											<td><p class="text-center">7.05</p></td>
											<td>สาขาตจพยาธิวิทยา</td>
											
											<td><asp:TextBox ID="TextBox175" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox176" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox177" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
										</tr>
										<tr>
											<td><p class="text-center">7.06</p></td>
											<td>สาขาพยาธิสูตินรีเวชวิทยา</td>
											
											<td><asp:TextBox ID="TextBox178" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox179" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox180" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
										</tr>

                                        <%--<tr>
											<td colspan="5"><b>8. รังสีแพทย์</b></td>
										</tr>--%>
                                        <tr>
											<td><p class="text-center">8.01</p></td>
											<td>สาขารังสีวิทยาทั่วไป</td>
											
											<td><asp:TextBox ID="TextBox181" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox182" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox183" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
										</tr>
										<tr>
											<td><p class="text-center">8.02</p></td>
											<td>สาขารังสีวิทยาวินิจฉัย </td>
											
											<td><asp:TextBox ID="TextBox184" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox185" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox186" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
										</tr>
                                        <tr>
											<td><p class="text-center">8.03</p></td>
											<td>สาขารังสีรักษาและมะเร็งวิทยา</td>
											
											<td><asp:TextBox ID="TextBox187" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox188" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox189" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
										</tr>
										<tr>
											<td><p class="text-center">8.04</p></td>
											<td>สาขาเวชศาสตร์นิวเคลียร์</td>
											
											<td><asp:TextBox ID="TextBox190" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox191" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox192" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
										</tr>
                                        <tr>
											<td><p class="text-center">8.05</p></td>
											<td>สาขาภาพวินิจฉัยระบบประสาท</td>
											
											<td><asp:TextBox ID="TextBox193" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox194" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox195" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
										</tr>
										<tr>
											<td><p class="text-center">8.06</p></td>
											<td>สาขารังสีร่วมรักษาระบบประสาท</td>
											
											<td><asp:TextBox ID="TextBox196" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox197" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox198" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
										</tr>
                                        <tr>
											<td><p class="text-center">8.07</p></td>
											<td>สาขารังสีร่วมรักษาของลำตัว</td>
											
											<td><asp:TextBox ID="TextBox199" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox200" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox201" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
										</tr>
										<tr>
											<td><p class="text-center">8.08</p></td>
											<td>สาขาภาพวินิจฉัยชั้นสูง</td>
											
											<td><asp:TextBox ID="TextBox202" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox203" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox204" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
										</tr>

                                        
                                       <%-- <tr>
											<td colspan="5"><b>9. วิสัญญีวิทยาแพทย์</b></td>
										</tr>--%>
                                        <tr>
											<td><p class="text-center">9.01</p></td>
											<td>สาขาวิสัญญีวิทยา</td>
											
											<td><asp:TextBox ID="TextBox205" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox206" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox207" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
										</tr>
										<tr>
											<td><p class="text-center">9.02</p></td>
											<td>สาขาวิสัญญีวิทยาสำหรับการผ่าตัดหัวใจ หลอดเลือดใหญ่ และทรวงอก </td>
											
											<td><asp:TextBox ID="TextBox208" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox209" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox210" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
										</tr>
                                        <tr>
											<td><p class="text-center">9.03</p></td>
											<td>สาขาวิสัญญีวิทยาสำหรับเด็ก</td>
											
											<td><asp:TextBox ID="TextBox211" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox212" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox213" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
										</tr>
										<tr>
											<td><p class="text-center">9.04</p></td>
											<td>สาขาวิสัญญีวิทยาสำหรับผู้ป่วยโรคทางระบบประสาท</td>
											
											<td><asp:TextBox ID="TextBox214" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox215" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox216" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
										</tr>
                                        
                                        <%--<tr>
											<td colspan="5"><b>10 แพทย์เวชปฏิบัติ</b></td>
										</tr>--%>
                                        <tr>
											<td><p class="text-center">10.01</p></td>
											<td>สาขาเวชปฏิบัติทั่วไป</td>
											
											<td><asp:TextBox ID="TextBox217" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox218" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox219" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
										</tr>
										<tr>
											<td><p class="text-center">10.02</p></td>
											<td>สาขาเวชศาสตร์ครอบครัว </td>
											
											<td><asp:TextBox ID="TextBox220" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox221" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox222" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
										</tr>
                                        <tr>
											<td><p class="text-center">10.03</p></td>
											<td>สาขาเวชศาสตรฟื้นฟู</td>
											
											<td><asp:TextBox ID="TextBox223" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox224" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox225" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
										</tr>
										<tr>
											<td><p class="text-center">10.04</p></td>
											<td>สาขาเวชศาสตร์ป้องกัน แขนงสาธารณสุขศาสตร์</td>
											
											<td><asp:TextBox ID="TextBox226" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox227" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox228" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
										</tr>
                                        <tr>
											<td><p class="text-center">10.05</p></td>
											<td>สาขาเวชศาสตร์ป้องกัน แขนงระบาดวิทยา</td>
											
											<td><asp:TextBox ID="TextBox229" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox230" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox231" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
										</tr>
										<tr>
											<td><p class="text-center">10.06</p></td>
											<td>สาขาเวชศาสตร์ป้องกัน แขนงเวชศาสตร์ป้องกันคลินิก</td>
											
											<td><asp:TextBox ID="TextBox232" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox233" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox234" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
										</tr>
                                        <tr>
											<td><p class="text-center">10.07</p></td>
											<td>สาขาเวชศาสตร์ป้องกัน แขนงเวชศาสตร์การบิน</td>
											
											<td><asp:TextBox ID="TextBox235" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox236" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox237" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
										</tr>
										<tr>
											<td><p class="text-center">10.08</p></td>
											<td>สาขาเวชศาสตร์ป้องกัน แขนงอาชีวเวชศาสตร์ </td>
											
											<td><asp:TextBox ID="TextBox238" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox239" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox240" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
										</tr>
										<tr>
											<td><p class="text-center">10.09</p></td>
											<td>สาขาเวชศาสตร์ป้องกัน แขนงสุขภาพจิตชุมชน </td>
											
											<td><asp:TextBox ID="TextBox241" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox242" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox243" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
										</tr>
										<tr>
											<td><p class="text-center">10.10</p></td>
											<td>สาขาเวชศาสตร์ป้องกัน แขนงเวชศาสตร์การเดินทางและท่องเที่ยว</td>
											
											<td><asp:TextBox ID="TextBox244" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox245" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox246" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
										</tr>
										<tr>
											<td><p class="text-center">10.11</p></td>
											<td>สาขาเวชศาสตร์ป้องกัน แขนงเวชศาสตร์ทางทะเล </td>
											
											<td><asp:TextBox ID="TextBox247" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox248" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox249" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
										</tr>
										<tr>
											<th>รวมทั้งสิ้น</th>
											<th><b>-</b></th>
											
											<th><b class="pull-right">1234</b></th>
											<th><b class="pull-right">1023</b></th>
											<th><b class="pull-right">1423</b></th>
										</tr>
									</tbody>
								</table>
                               <div class="span9">
									        <div class="form-actions pull-right">
										        <button type="submit" class="btn btn-green"><i class="icon-ok"></i> บันทึกข้อมูล</button>
										        <button type="submit" class="btn btn-green"><i class="icon-ok"></i> ยืนยันและส่งข้อมูล</button>
										        <button type="button" class="btn btn-lightred"><i class="icon-remove"></i> ยกเลิก</button>
									        </div>
									</div>
							</div>
						</div>
				   	</div>

				</div>
				
			</div>
		</div></div>
</asp:Content>











