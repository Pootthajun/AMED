﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="Form_Report.aspx.cs" Inherits="AMED.Form_Report" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <title>การส่งรายงาน - กรมการแพทย์ทหารบก</title>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <div class="container-fluid" id="content">
		<div id="left">
			<div class="subnav">
				<div class="subnav-title">
					<a href="#" class='toggle-subnav'><i class="icon-angle-down"></i><span><p class="icon-bar-chart"></p>  หน่วยสายแพทย์</span></a>
				</div>
				<ul class="subnav-menu">
					<li>
						<a href="Default.aspx">สรุปภาพรวม</a>
					</li>
					<li class='dropdown active'>
							<a href="#" data-toggle="dropdown">การบันทึกข้อมูล</a>
							<ul class="dropdown-menu">
								<li class="active">
									<a href="Form_1101.aspx">( รง.ผสต.1 )</a>
								</li>
								<li>
									<a href="Form_1102.aspx">( รง.ผสต.2 )</a>
								</li>
								<li>
									<a href="Form_1103.aspx">( รง.ผสต.3 )</a>
								</li>
								<li>
									<a href="Form_1104.aspx">( รง.ผสต.4 )</a>
								</li>
								<li>
									<a href="Form_1105.aspx">( รง.ผสต.5 )</a>
								</li>
								<li>
									<a href="Form_1106.aspx">( รง.ผสต.6 )</a>
								</li>
								<li>
									<a href="Form_1108.aspx">( รง.ผสต.8 )</a>
								</li>
								<li>
									<a href="Form_1109.aspx">( รง.ผสต.9 )</a>
								</li>
								<li>
									<a href="Form_1110.aspx">( รง.ผสต.10 )</a>
								</li>
                                <li>
									<a href="Form_1111.aspx">( รง.ผสต.11 )</a>
								</li>
								<li>
									<a href="Form_1112.aspx">( รง.ผสต.12 )</a>
								</li>
								<li>
									<a href="Form_1117.aspx">( รง.ผสต.17 )</a>
								</li>
								<li>
									<a href="Form_1118.aspx">( รง.ผสต.18 )</a>
								</li>
								<li>
									<a href="Form_1119.aspx">( รง.ผสต.19 )</a>
								</li>
								<li>
									<a href="Form_1122.aspx">( รง.ผสต.22 )</a>
								</li>
								<li>
									<a href="Form_1123.aspx">( รง.ผสต.23 )</a>
								</li>
					            <li>
						            <a href="Form_Extinction.aspx">รายงานสถิติการสุญเสีย</a>
					            </li>
					            <li>
						            <a href="MT_ICD10.aspx">รายงานการตรวจโรค</a>
					            </li>
							</ul>
						</li>
				</ul>
			</div>
		</div>
		<div id="main">
			<div class="container-fluid">
				<div class="page-header">
					<div class="pull-left">
						<h4>สถานะการส่งรายงาน</h4>
					</div>
					
				</div>
				<div class="breadcrumbs">
					<ul>
						<li>
							<a href="more-login.html">Home</a>
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<a href="forms-basic.html">กองวิทยาการ</a>
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<a href="MT_User.aspx">การส่งรายงาน</a>
						</li>
					</ul>
					<div class="close-bread">
						<a href="#"><i class="icon-remove"></i></a>
					</div>
				</div>
				

				<div class="row-fluid">

						<div class="span12">
                            <div class="box-content nopadding">
								<form action="#" method="get" class='form-vertical'>
									<div class="row-fluid">
										<div class="span3">
											<div class="control-group">
												<label for="textfield" class="control-label">กองทัพ (ภาค)</label>
												<div class="controls controls-row">
													<select name="s2" id="s1" class='select2-me input-xlarge' data-placeholder="Please select something">
													<option value="">ทั้งหมด</option>
													<option value="01">หน่วยงาน (ส่วนกลาง)</option>
													<option value="02">กองทัพภาคที่ 1</option>
													<option value="03">กองทัพภาคที่ 2</option>
													<option value="04">กองทัพภาคที่ 3</option>
													<option value="05">กองทัพภาคที่ 4</option>
												</select>
												</div>
											</div>
										</div>
										<div class="span3">
											<div class="control-group">
												<label for="textfield" class="control-label">หน่วยสายแพทย์</label>
												<div class="controls controls-row">
													<select name="s2" id="s1" class='select2-me input-xlarge' data-placeholder="Please select something">
													<option value="">ทั้งหมด</option>
													<option value="01">รพ.พระมงกุฎเกล้า</option>
													<option value="02">รพ.อานันทมหิดล</option>
													<option value="03">รพ.ค่ายสุรนารี</option>
													<option value="04">รพ.ค่ายธนะรัชต์</option>
													<option value="05">รพ.ค่ายจักรพงษ์</option>
													<option value="06">รพ.ค่ายสุรสีห์</option>
													<option value="07">รพ.ค่ายสรรพสิทธิประสงค์</option>
													<option value="08">รพ.ค่ายประจักษ์ศิลปาคม</option>
													<option value="09">รพ.ค่ายสมเด็จพระนเรศวรมหาราช</option>
													<option value="10">รพ.ค่ายจิรประวัติ</option>
													<option value="11">รพ.ค่ายสุรศักดิ์มนตรี</option>
													<option value="12">รพ.ค่ายวชิราวุธ</option>
													<option value="13">รพ.รร.จปร.</option>
													<option value="14">รพ.ค่ายกาวิละ</option>
													<option value="15">รพ.ค่ายอดิศร</option>
													<option value="16">รพ.ค่ายภาณุรังษี</option>
													<option value="17">รพ.ค่ายสุรสิงหนาท</option>
													<option value="18">รพ.ค่ายนวมินทราชินี</option>
													<option value="19">รพ.ค่ายกฤษณ์สีวะรา</option>
													<option value="20">รพ.ค่ายวีรวัฒน์โยธิน</option>
													<option value="21">รพ.ค่ายสมเด็จพระพุทธยอดฟ้าจุฬาโลกมหาราช</option>
													<option value="22">รพ.ค่ายเม็งรายมหาราช</option>
													<option value="23">รพ.ค่ายวชิรปราการ</option>
													<option value="24">รพ.ค่ายพิชัยดาบหัก</option>
													<option value="25">รพ.ค่ายพ่อขุนผาเมือง</option>
													<option value="26">รพ.ค่ายเสนาณรงค์</option>
													<option value="27">รพ.ค่ายอิงคยุทธบริหาร</option>
													<option value="28">รพ.รพ.ค่ายวิภาวดีรังสิต</option>
													<option value="29">รพ.ค่ายรามราชนิเวศน์</option>
													<option value="30">รพ.ค่ายพระยอดเมืองขวาง</option>
													<option value="31">รพ.ค่ายศรีสองรัก</option>
												</select>
												</div>
											</div>
										</div>
										<div class="span3">
											<div class="control-group">
												<label for="textfield" class="control-label">ประจำเดือน</label>
												<div class="controls controls-row">
													<select name="s2" id="s1" class='select2-me input-xlarge' data-placeholder="Please select something">
													<option value="">ทั้งหมด</option>
													<option value="01">มกราคม</option>
													<option value="02">กุมภาพันธ์</option>
													<option value="03">มีนาคม</option>
													<option value="04">เมษายน</option>
													<option value="05">พฤษภาคม</option>
													<option value="06">มิถุนายน</option>
													<option value="07">กรกฎาคม</option>
													<option value="08">สิงหาคม</option>
													<option value="09">กันยายน</option>
													<option value="10">ตุลาคม</option>
													<option value="11">พฤศจิกายน</option>
													<option value="12">ธันวาคม</option>
												</select>
												</div>
											</div>
										</div>
										<div class="span3">
											<div class="control-group">
												<label for="textfield" class="control-label">สถานะ</label>
												<div class="controls controls-row">
													<select name="s2" id="s1" class='select2-me input-xlarge' data-placeholder="Please select something">
													<option value="">ทั้งหมด</option>
													<option value="01">ส่งแล้ว</option>
													<option value="02">ยังไม่ส่ง</option>
												</select>
												</div>
											</div>
										</div>
									</div>
								</form>
							</div>
							<div class="box box-color box-small box-bordered">
                                <div class="control-group">
									</div>
								<div class="box-title">
									<h4>
										<i class="icon-reorder"></i>
										สถานะการส่งรายงาน
									</h4>
									<div class="actions">
                                        <a href="#" class="btn btn-mini custom-checkbox checkbox-active">Automatic refresh<i class="icon-check-empty"></i></a>
								
										<a href="#" class="btn btn-mini content-refresh"><i class="icon-refresh"></i></a>
										<a href="#" class="btn btn-mini content-remove"><i class="icon-remove"></i></a>
										<a href="#" class="btn btn-mini content-slideUp"><i class="icon-angle-down"></i></a>
									</div>
								</div>
								<div style="overflow-x:auto;" class="box-content nopadding">
								<table class="table table-hover table-nomargin">
									<tbody>
										<tr>
											<th class="text-center">หน่วยงาน (ส่วนกลาง)</th>
											<th class="text-center">หน่วยงานสายแพทย์</th>
											<th class="text-center">ชื่อรายงานที่ยังไม่ส่ง</th>
											<th class="text-center">วันที่ส่ง</th>
											<th class="text-center">สถานะ</th>
										</tr>
										<tr>
											<td>๑๐๑</td>
											<td>รพ.พระมงกุฎเกล้า</td>
											<td>รายงานจำนวนผู้ป่วยจำแนกตามประเภทบุคคล</td>
											<td>5/12/2016</td>
											<td><i class="icon-remove text-center text-warning"></i></td>
										</tr>
										<tr>
											<td>๑๐๓</td>
											<td>รพ.ค่ายธนะรัชต์</td>
											<td>รายงานจำนวนผู้ป่วยจำแนกตามประเภทบุคคล ( รง.ผสต.1 ) </td>
											<td>5/12/2016</td>
											<td><i class="icon-remove text-center text-warning"></i></td>
										</tr>
										<tr>
											<td>๑๑๐</td>
											<td>รพ.รร.จปร.</td>
											<td>รายงานจำนวนผู้ป่วยในจำแนกตามสาเหตุป่วย ( รง.ผสต.2 )</td>
											<td>5/12/2016</td>
											<td><i class="icon-remove text-center text-warning"></i></td>
										</tr>
										<tr>
											<td>๑๔๐</td>
											<td>พัน สร.13</td>
											<td>รายงานจำนวนผู้ป่วยในจำแนกตามสาเหตุปลดพิการ ( รง.ผสต.3 )</td>
											<td>5/12/2016</td>
											<td><i class="icon-remove text-center text-warning"></i></td>
										</tr>
                                        <tr>
											<th class="text-center">กองทัพภาคที่ 1</th>
											<th class="text-center">หน่วยงานสายแพทย์</th>
											<th class="text-center">ชื่อรายงานที่ยังไม่ส่ง</th>
											<th class="text-center">วันที่ส่ง</th>
											<th class="text-center">สถานะ</th>
										</tr>
										<tr>
											<td>๑๐๒</td>
											<td>รพ.อานันทมหิดล</td>
											<td>รายงานจำนวนผู้ป่วยในจำแนกตามสาเหตุตาย( รง.ผสต.4 )</td>
											<td>5/12/2016</td>
											<td><i class="icon-remove text-center text-warning"></i></td>
										</tr>
										<tr>
											<td>๑๐๔</td>
											<td>รพ.ค่ายจักรพงษ์</td>
											<td>รายงานจำนวนผู้ป่วยนอกจำแนกตามสาเหตุป่วย ( รง.ผสต.5 )</td>
											<td>5/12/2016</td>
											<td><i class="icon-remove text-center text-warning"></i></td>
										</tr>
										<tr>
											<td>๑๐๕</td>
											<td>รพ.ค่ายอดิสร</td>
											<td>บัญชียอดกำลังพลซึ่งต้องสนับสนุนบริการแพทย์( รง.ผสต.6 )</td>
											<td>5/12/2016</td>
											<td><i class="icon-remove text-center text-warning"></i></td>
										</tr>
										<tr>
											<td>๑๐๖</td>
											<td>รพ.ค่ายภาณุรังษี</td>
											<td>รายงานจำนวนผู้มารับบริการของ รพ.ทบ. ( ตัวชี้วัด ) ( รง.ผสต.8 )</td>
											<td>5/12/2016</td>
											<td><i class="icon-remove text-center text-warning"></i></td>
										</tr>
										<tr>
											<td>๑๐๗</td>
											<td>รพ.ค่ายสุรสีห์</td>
											<td>รายงานจำนวนผู้ป่วยที่มารับบริการฝังเข็มจำแนกตาม สาเหตุป่วยและความพึงพอใจของผู้มารับบริการฝังเข็ม ( รง.ผสต.10 )</td>
											<td>5/12/2016</td>
											<td><i class="icon-remove text-center text-warning"></i></td>
										</tr>
										<tr>
											<td>๑๐๘</td>
											<td>รพ.ค่ายสุรสิงหนาท</td>
											<td>รายงานจำนวนผู้ป่วยที่มารับบริการฝังเข็มจำแนกตาม สาเหตุป่วยและความพึงพอใจของผู้มารับบริการฝังเข็ม ( รง.ผสต.10 )</td>
											<td>5/12/2016</td>
											<td><i class="icon-remove text-center text-warning"></i></td>
										</tr>
										<tr>
											<td>๑๐๙</td>
											<td>รพ.ค่ายนวมินทราชินี</td>
											<td>รายงานข้อมูลการตรวจร่างกายของกำลังพลกองทัพบกรายบุคคล ( รง.ผสต.11 )</td>
											<td>5/12/2016</td>
											<td><i class="icon-remove text-center text-warning"></i></td>
										</tr>
										<tr>
											<td>๑๑๑</td>
											<td>รพ.ค่ายรามราชนิเวศน์</td>
											<td>รายงานข้อมูลการตรวจร่างกายของครอบครัวกำลังพลกองทัพบกรายบุคคล ( รง.ผสต.12 )</td>
											<td>5/12/2016</td>
											<td><i class="icon-remove text-center text-warning"></i></td>
										</tr>
										<tr>
											<td>๑๑๒</td>
											<td>หน่วยตรวจโรค คสพ.</td>
											<td>รายงานสรุปผลการตรวจร่างกายของกำลังพลกองทัพบก( รง.ผสต.13 )</td>
											<td>5/12/2016</td>
											<td><i class="icon-remove text-center text-warning"></i></td>
										</tr>
										<tr>
											<td>๑๑๓</td>
											<td>มว.พยาบาล กวรบ.ศอ.สพ.ทบ</td>
											<td>รายงานสรุปผลการตรวจร่างกายของครอบครัวกำลังพลกองทัพบก ( รง.ผสต.14 )</td>
											<td>5/12/2016</td>
											<td><i class="icon-remove text-center text-warning"></i></td>
										</tr>
										
										<tr>
											<td>๑๔๒</td>
											<td>รพ.พระมงกุฎเกล้า</td>
											<td>รายงานจำนวนผู้ป่วยในจำแนกตามสาเหตุป่วย ( รง.ผสต.2 )</td>
											<td>5/12/2016</td>
											<td><i class="icon-remove text-center text-warning"></i></td>
										</tr>
										<tr>
											<td>๑๔๖</td>
											<td>รพ.อานันทมหิดล</td>
											<td>รายงานจำนวนผู้ป่วยในจำแนกตามสาเหตุปลดพิการ ( รง.ผสต.3 )</td>
											<td>5/12/2016</td>
											<td><i class="icon-remove text-center text-warning"></i></td>
										</tr>
										<tr>
											<td>๑๔๙</td>
											<td>รพ.พระมงกุฎเกล้า</td>
											<td>รายงานจำนวนแพทย์ จำแนกตามสาขาความเชี่ยวชาญเฉพาะทาง ( รง.ผสต.19 )</td>
											<td>5/12/2016</td>
											<td><i class="icon-remove text-center text-warning"></i></td>
										</tr>
									
									</tbody>
								</table>
								<div class="table-pagination">
									<a href="#" class='disabled'>Previous</a>
									<span>
										<a href="#" class='active'>1</a>
										<a href="#">2</a>
										<a href="#">3</a>
									</span>
									<a href="#">Next</a>
								</div>
							</div>
							</div>
						</div>
						

					</div>
				
			</div>
		</div></div>
</asp:Content>








