﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="MT_Rule.aspx.cs" Inherits="AMED.MT_Rule" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <title>กฏกระทรวงที่ปลด - กรมการแพทย์ทหารบก</title>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder2" runat="server">
        <div class="container-fluid">
	      <img src="assets/img/header/logo.png">
			    
			   <div class="user">
				    <ul class='main-nav'>
				    <li class="highli">
					    <a href="#" data-toggle="dropdown" class='dropdown-toggle'>
						    <span>สรุปรายงานประจำปี</span>
						    <span class="caret"></span>
					    </a>
					    <ul class="dropdown-menu">
						    <li>
							    <a href="assets/forms-basic.html">Basic forms</a>
						    </li>
						    <li>
							    <a href="assets/forms-extended.html">Extended forms</a>
						    </li>
						    <li>
							    <a href="assets/forms-validation.html">Validation</a>
						    </li>
						    <li class='active'>
							    <a href="assets/forms-wizard.html">Wizard</a>
						    </li>
					    </ul>
				    </li>
				    <li class="highli">
					    <a href="#" data-toggle="dropdown" class='dropdown-toggle'>
						    <span>ข้อมูลรายงาน</span>
					    </a>
				    </li>
				    <li class="highli active">
					    <a href="#" data-toggle="dropdown" class='dropdown-toggle'>
						    <span>ตั้งค่า</span>
						    <span class="caret"></span>
					    </a>
					    <ul class="dropdown-menu pull-right">
						     <li class="active">
							    <a href="#">ข้อมูลทั่วไป</a>
						    </li>
						    <li>
							    <a href="#">กองวิทยาการ</a>
						    </li>
						     <li>
							    <a href="#">กองงบประมาณและการเงิน</a>
						    </li>
						     <li>
							    <a href="#">กองส่งกำลังบำรุง</a>
						    </li>
						     <li>
							    <a href="#">ผู้ใช้ (User)</a>
						    </li>
						     <li>
							    <a href="#">การส่งรายงาน</a>
						    </li>
						    
					    </ul>
				    </li>
                    <li class="highli">
					    <a href="#" data-toggle="dropdown" class='dropdown-toggle'>
						    <span><i class="icon-user"></i>  Atiwat</span>
					    </a>
					    <ul class="dropdown-menu pull-right">
						    <li>
							    <a href="more-userprofile.html">Edit profile</a>
						    </li>
						    <li>
							    <a href="#">Account settings</a>
						    </li>
						    <li>
							    <a href="more-login.html">Sign out</a>
						    </li>
					    </ul>
				    </li>
			    </ul>
				    
			    </div>
		    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <div class="container-fluid" id="content">
		<div id="left">
			<div class="subnav">
				<div class="subnav-title">
					<a href="#" class='toggle-subnav'><i class="icon-angle-down"></i><span><p class="icon-file"></p>  กองวิทยาการ</span></a>
				</div>
				<ul class="subnav-menu">
					<li>
						<a href="#" data-toggle="dropdown"><i class="icon-angle-right"></i> รหัสโรค (ICD-10)</a>
					</li>
					<li>
						<a href="#"><i class="icon-angle-right"></i> กลุ่มโรคที่ขัดกับกฎกระทรวง</a>
					</li>
					<li class='active'>
						<a href="#"><i class="icon-angle-right"></i> โรคที่ขัดกับกฎกระทรวง</a>
					</li>
					<li>
						<a href="#"><i class="icon-angle-right"></i> ขนาดเตียง</a>
					</li>
					<li>
						<a href="#"><i class="icon-angle-right"></i> ตำแหน่งในโรงพยาบาล</a>
					</li>
					<li>
						<a href="#"><i class="icon-angle-right"></i> หอผู้ป่วย</a>
					</li>
				</ul>
			</div>
			
		</div>
		<div id="main">
			<div class="container-fluid">
				<div class="page-header">
					<div class="pull-left">
						<h3>กฏกระทรวงที่ปลด</h3>
					</div>
					<div class="pull-right">
						<%--<ul class="minitiles">
							<li class='blue'>
								<a href="#"><i class="icon-bar-chart"></i></a>
							</li>
							<li class='lightred'>
								<a href="#"><i class="icon-bolt"></i></a>
							</li>
						</ul>--%>
						<ul class="stats">
							<li class='satblue'>
								<i class="icon-calendar"></i>
								<div class="details">
									<span class="big">December 22, 2013</span>
									<span>Wednesday, 13:56</span>
								</div>
							</li>
						</ul>
					</div>
				</div>
				<div class="breadcrumbs">
					<ul>
						<li>
							<a href="more-login.html">Home</a>
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<a href="forms-basic.html">ตั้งค่า</a>
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<a href="forms-wizard.html">กฏกระทรวงที่ปลด</a>
						</li>
					</ul>
					<div class="close-bread">
						<a href="#"><i class="icon-remove"></i></a>
					</div>
				</div>
				

				<div class="row-fluid">

						<div class="span12"><br />
                            <a href="MT_AddRule.aspx" class="btn btn-blue"><i class="icon-plus"></i> เพิ่ม กฏกระทรวงที่ปลด</a>
							<div class="box box-color orange box-small box-bordered">
								<div class="box-title">
									<h3>
										<i class="icon-reorder"></i>
										Small widget
									</h3>
									<div class="actions">
										<a href="#" class="btn btn-mini content-refresh"><i class="icon-refresh"></i></a>
										<a href="#" class="btn btn-mini content-remove"><i class="icon-remove"></i></a>
										<a href="#" class="btn btn-mini content-slideUp"><i class="icon-angle-down"></i></a>
									</div>
								</div>
								<div class="box-content nopadding">
								<table class="table table-hover table-nomargin">
									<thead>
										<tr>
											<th style="width: 50px">ลำดับ</th>
											<th style="width: 550px">ชื่อกลุ่มกฏกระทรวงที่ปลด</th>
											<th style="width: 50px">แก้ไข</th>
											<th style="width: 50px">ลบ</th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td>1</td>
											<td>
												Internet
												Explorer 4.0
											</td>
											<td><i class="icon-edit text-center"></i></td>
											<td><i class="icon-trash text-center"></i></td>
										</tr>
										<tr>
											<td>2</td>
											<td>Nokia N800</td>
											<td><i class="icon-edit text-center"></i></td>
											<td><i class="icon-trash text-center"></i></td>
										</tr>
										<tr>
											<td>3</td>
											<td>NetFront 3.4</td>
											<td><i class="icon-edit text-center"></i></td>
											<td><i class="icon-trash text-center"></i></td>
										</tr>
										<tr>
											<td>4</td>
											<td>Dillo 0.8</td>
											<td><i class="icon-edit text-center"></i></td>
											<td><i class="icon-trash text-center"></i></td>
										</tr>
										<tr>
											<td>5</td>
											<td>Links</td>
											<td><i class="icon-edit text-center"></i></td>
											<td><i class="icon-trash text-center"></i></td>
										</tr>
										<tr>
											<td>6</td>
											<td>Lynx</td>
											<td><i class="icon-edit text-center"></i></td>
											<td><i class="icon-trash text-center"></i></td>
										</tr>
									</tbody>
								</table>
								<div class="table-pagination">
									<a href="#" class='disabled'>Previous</a>
									<span>
										<a href="#" class='active'>1</a>
										<a href="#">2</a>
										<a href="#">3</a>
									</span>
									<a href="#">Next</a>
								</div>
							</div>
							</div>
						</div>
						

					</div>
				
			</div>
		</div></div>
</asp:Content>


