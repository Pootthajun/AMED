﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="Form_1123.aspx.cs" Inherits="AMED.Form_1123" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <title>รง.ผสต.23 - กรมการแพทย์ทหารบก</title>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <div class="container-fluid" id="content">
		<div id="left">
			<div class="subnav">
				<div class="subnav-title">
					<a href="#" class='toggle-subnav'><i class="icon-angle-down"></i><span><p class="icon-building"></p> หน่วยสายแพทย์</span></a>
				</div>
				<ul class="subnav-menu">
					<li>
						<a href="Default.aspx">สรุปภาพรวม</a>
					</li>
					<li class="active">
							<a href="Hotpital.aspx">การบันทึกข้อมูล</a>
							
						</li>
					<li>
						<a href="Report.aspx">สรุปรายงาน</a>
					</li>
				</ul>
			</div>
			<div class="subnav">
				<div class="subnav-title">
					<a href="#" class='toggle-subnav'><i class="icon-angle-down"></i><span><p class="icon-building"></p>  ส่วนกลาง</span></a>
				</div>
				<ul class="subnav-menu">
					<li>
						<a href="Default.aspx">สรุปภาพรวม</a>
					</li>
					<li>
							<a href="Department.aspx">การตรวจสอบการบันทึกข้อมูล</a>
							
					</li>
					<li>
						<a href="Report.aspx">สรุปรายงาน</a>
					</li>
				</ul>
			</div>
            <div class="subnav">
				<div class="subnav-title">
					<a href="#" class='toggle-subnav'><i class="icon-angle-down"></i><span><p class="icon-cog"></p> ข้อมูลพื้นฐาน</span></a>
				</div>
				<ul class="subnav-menu">
					<li class='dropdown'>
						<a href="#" data-toggle="dropdown"> ข้อมุลทั่วไป</a>
						<ul class="dropdown-menu">
							<li>
								<a href="MT_Region.aspx"> กองทัพภาค</a>
							</li>
							<li>
								<a href="MT_Hospital.aspx"> หน่วยสายแพทย์</a>
							</li>
							<li>
								<a href="MT_Department.aspx"> หน่วยงาน (ส่วนกลาง)</a>
							</li>
							<li>
								<a href="MT_AddOrganization.aspx"> สังกัด</a>
							</li>
							<li>
								<a href="MT_Province.aspx"> จังหวัด</a>
							</li>
						</ul>
					</li>
					<li class='dropdown'>
						<a href="#" data-toggle="dropdown">กองวิทยาการ</a>
						<ul class="dropdown-menu">
							<li>
								<a href="MTS_ICD10.aspx"> รหัสโรค (ICD10)</a>
							</li>
							<li>
								<a href="MTS_GroupRule.aspx"> กลุ่มโรคที่ขัดต่อกฎกระทรวง</a>
							</li>
							<li>
								<a href="MTS_Rule.aspx"> โรคที่ขัดต่อกฎกระทรวง</a>
							</li>
							<li>
								<a href="MTS_Sizebed.aspx"> ขนาดเตียง</a>
							</li>
							<li>
								<a href="MTS_ProHotpital.aspx"> ตำแหน่งในโรงพยาบาล</a>
							</li>
							<li>
								<a href="MTS_Wards.aspx"> หอผู้ป่วย</a>
							</li>
						</ul>
					</li>
                    <li class='dropdown'>
						<a href="#" data-toggle="dropdown">กองงบประมาณการเงิน</a>
						<ul class="dropdown-menu">
							<li>
								<a href="MTF_Assets.aspx">สินทรัพย์และหนี้สิน</a>
							</li>
							<li>
								<a href="MTF_Progress.aspx">ผลการดำเนินงาน</a>
							</li>
							<li>
								<a href="MTF_Reception.aspx">การรับ-การจ่าย</a>
							</li>
							<li>
								<a href="MTF_Deposit.aspx">เงินฝาก</a>
							</li>
						</ul>
					</li>
                    <li class='dropdown'>
						<a href="#" data-toggle="dropdown">กองส่งกำลังบำรุง</a>
						<ul class="dropdown-menu">
							<li>
								<a href="MTT_TypeDrug.aspx"> ประเภทยา</a>
							</li>
							<li>
								<a href="MTT_Drug.aspx"> ยา</a>
							</li>
							<li>
								<a href="MTT_DrugNature.aspx">ลักษณะยา</a>
							</li>
							<li>
								<a href="MTT_MedicalSupplies.aspx">เวชภัณฑ์</a>
							</li>
						</ul>
					</li>
                    <li class='dropdown'>
						<a href="#" data-toggle="dropdown">ผู้ใช้งาน (User)</a>
						<ul class="dropdown-menu">
							<li>
								<a href="MT_PersonType.aspx"> ประเภทบุคคล</a>
							</li>
							<li>
								<a href="MT_Person.aspx">บุคคลย่อย</a>
							</li>
							<li>
								<a href="#">กำหนดสิทธิ์</a>
							</li>
							<li>
								<a href="MT_User.aspx">ผู้ใช้งานในระบบ</a>
							</li>
						</ul>
					</li>
                     <li class='dropdown'>
						<a href="#" data-toggle="dropdown">การส่งรายงาน</a>
						<ul class="dropdown-menu">
							<li>
								<a href="MTR_DeadlineReports.aspx"> กำหนดส่งรายงาน</a>
							</li>
							<li>
								<a href="MTR_EditReport.aspx"> แก้ไขข้อมูลรายงาน</a>
							</li>
							<li>
								<a href="MTR_NameReport.aspx"> ชื่อแบบฟอร์มรายงาน</a>
							</li>
						</ul>
					</li>
				</ul>
			</div>
		</div>
		<div id="main">
			<div class="container-fluid">
				<div class="page-header">
					<div class="pull-left">
						<h4>รายงานจำนวนกำลังพลของหน่วยสายแพทย์ จำแนกตามตำแหน่ง ( รง.ผสต.23 )</h4>
					</div>
					
				</div>
				<div class="breadcrumbs">
					<ul>
						<li>
							<a href="Default.aspx">Dashborad</a>
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<a href="forms-basic.html">กองวิทยาการ</a>
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<a href="MT_User.aspx">รง.ผสต.23</a>
						</li>
					</ul>
					<div class="close-bread">
						<a href="#"><i class="icon-remove"></i></a>
					</div>
				</div>
				
				<div class="row-fluid">
						<div class="span12">
						  <div class="box">
							<div class="box-title">
                                <div class='basic-margin'>
								<div class="fileupload fileupload-new" data-provides="fileupload">
									<div class="input-append">
										<div class="uneditable-input span3"><i class="icon-file fileupload-exists"></i> <span class="fileupload-preview"></span></div><span class="btn btn-file"><span class="fileupload-new">Select file</span><span class="fileupload-exists">Change</span><input type="file" name="aaaa" /></span><a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
								    	</div>
								   <div class="btn-group span2 pull-right">
                                  <%-- <button class="btn btn-info" rel="tooltip" title="edit"><i class="icon-edit"></i> แก้ไขเอกสาร</button>--%>
												<a href="#" data-toggle="dropdown" class="btn btn-warning dropdown-toggle" rel="tooltip" title="print"><i class="icon-print"></i> พิมพ์รายงาน <span class="caret"></span></a>
												<ul class="dropdown-menu dropdown-warning">
													<li>
														<a href="#"><i class="icon-file"></i> Excel</a>
													</li>
													<li>
														<a href="#"><i class="icon-file"></i> PDF</a>
													</li>
												</ul>
										</div></div>
                                </div>
							</div>
                            <br />

                                <div style="overflow-x:auto;" class="box-content nopadding"><br />
                                    <form action="#" method="get" class='form-vertical'>
                                    <div class="span2">
											<div class="control-group">
												<label for="textfield" class="control-label">กองทัพ</label>
                                                <asp:DropDownList ID="ddlทั้งหมด" runat="server" Width="150px">
                                                <asp:ListItem Text="หน่วยงาน (ส่วนกลาง)" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="กองทัพภาคที่ 2" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="กองทัพภาคที่ 3" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="กองทัพภาคที่ 4" Value="3"></asp:ListItem>
                                            </asp:DropDownList>
											</div>
                                            
										</div>
										<div class="span3">
                                            <div class="control-group">
												<label for="textfield" class="control-label">หน่วยสายแพทย์</label>
                                                <asp:DropDownList ID="DropDownList2" runat="server" Width="230px">
                                                <asp:ListItem Text="ทั้งหมด" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="รพ.พระมงกุฎเกล้า" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="รพ.อานันทมหิดล" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="รพ.ค่ายสุรนารี" Value="3"></asp:ListItem>
                                            </asp:DropDownList>
											</div>
											<%--<div class="control-group">
												<label for="textfield" class="control-label">หน่วยสายแพทย์</label>
												<div class="controls controls-row">
													<select name="s2" id="s1" class='select2-me input-xlarge' data-placeholder="Please select something">
													<option value="">ทั้งหมด</option>
													<option value="01">รพ.พระมงกุฎเกล้า</option>
													<option value="02">รพ.อานันทมหิดล</option>
													<option value="03">รพ.ค่ายสุรนารี</option>
													<option value="04">รพ.ค่ายธนะรัชต์</option>
													<option value="05">รพ.ค่ายจักรพงษ์</option>
													<option value="06">รพ.ค่ายสุรสีห์</option>
													<option value="07">รพ.ค่ายสรรพสิทธิประสงค์</option>
													<option value="08">รพ.ค่ายประจักษ์ศิลปาคม</option>
													<option value="09">รพ.ค่ายสมเด็จพระนเรศวรมหาราช</option>
													<option value="10">รพ.ค่ายจิรประวัติ</option>
													<option value="11">รพ.ค่ายสุรศักดิ์มนตรี</option>
													<option value="12">รพ.ค่ายวชิราวุธ</option>
													<option value="13">รพ.รร.จปร.</option>
													<option value="14">รพ.ค่ายกาวิละ</option>
													<option value="15">รพ.ค่ายอดิศร</option>
													<option value="16">รพ.ค่ายภาณุรังษี</option>
													<option value="17">รพ.ค่ายสุรสิงหนาท</option>
													<option value="18">รพ.ค่ายนวมินทราชินี</option>
													<option value="19">รพ.ค่ายกฤษณ์สีวะรา</option>
													<option value="20">รพ.ค่ายวีรวัฒน์โยธิน</option>
													<option value="21">รพ.ค่ายสมเด็จพระพุทธยอดฟ้าจุฬาโลกมหาราช</option>
													<option value="22">รพ.ค่ายเม็งรายมหาราช</option>
													<option value="23">รพ.ค่ายวชิรปราการ</option>
													<option value="24">รพ.ค่ายพิชัยดาบหัก</option>
													<option value="25">รพ.ค่ายพ่อขุนผาเมือง</option>
													<option value="26">รพ.ค่ายเสนาณรงค์</option>
													<option value="27">รพ.ค่ายอิงคยุทธบริหาร</option>
													<option value="28">รพ.รพ.ค่ายวิภาวดีรังสิต</option>
													<option value="29">รพ.ค่ายรามราชนิเวศน์</option>
													<option value="30">รพ.ค่ายพระยอดเมืองขวาง</option>
													<option value="31">รพ.ค่ายศรีสองรัก</option>
												</select>
												</div>
											</div>--%>
										</div>
                                          <div class="span2">
										    <div class="control-group">
												    <label for="textfield" class="control-label">ปีงบประมาณ</label>
                                                    <asp:DropDownList ID="DropDownList4" runat="server" Width="150px">
                                                    <asp:ListItem Text="ทั้งหมด" Value="0"></asp:ListItem>
                                                    <asp:ListItem Text="2560" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="2559" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="2558" Value="3"></asp:ListItem>
                                                    <asp:ListItem Text="2557" Value="4"></asp:ListItem>
                                                </asp:DropDownList>
											    </div>
                                              </div>
										<div class="span3">
                                            <div class="control-group">
												<label for="textfield" class="control-label">ประจำเดือน</label>
                                                <asp:DropDownList ID="DropDownList5" runat="server" Width="230px">
                                                <asp:ListItem Text="ทั้งหมด" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="มกราคม" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="กุมภาพันธ์" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="มีนาคม" Value="3"></asp:ListItem>
                                                <asp:ListItem Text="เมษายน" Value="4"></asp:ListItem>
                                                <asp:ListItem Text="พฤษภาคม" Value="5"></asp:ListItem>
                                                <asp:ListItem Text="กรกฎาคม" Value="6"></asp:ListItem>
                                                <asp:ListItem Text="สิงหาคม" Value="7"></asp:ListItem>
                                                <asp:ListItem Text="กันยายน" Value="8"></asp:ListItem>
                                                <asp:ListItem Text="ตุลาคม" Value="9"></asp:ListItem>
                                                <asp:ListItem Text="พฤศจิกายน" Value="10"></asp:ListItem>
                                                <asp:ListItem Text="ธันวาคม" Value="11"></asp:ListItem>
                                            </asp:DropDownList>
											</div>
										</div>
									
								</form>
                                    <table class="table table-hover table-nomargin table-bordered dataTable dataTable-noheader dataTable-fixedcolumn dataTable-scroll-x dataTable-scroll-y table-edit">
										<thead>
										<tr>
											<th style="width: 20px"><p class="text-center">ลำดับ</p></th>
											<th style="width: 200px"><p class="text-center">สาขาของการแพทย์เฉพาะทาง</p></th>
											<th style="width: 50px"><p class="text-center">นายทหาร</p></th>
											<th style="width: 50px"><p class="text-center">นายสิบ</p></th>
											<th style="width: 50px"><p class="text-center">ข้าราชการกลาโหมพลเรือน</p></th>
											<th style="width: 50px"><p class="text-center">ลูกจ้างประจำ</p></th>
											<th style="width: 50px"><p class="text-center">ลูกจ้างชั่วคราว</p></th>
											<th style="width: 50px"><p class="text-center">พนักงานราชการ</p></th>
										</tr>
									</thead>
										<tbody>
											<tr>
											<td><p class="text-center">01</p></td>
											<td>นายแพทย์</td>
											
											<td><input type="text" name="textfield" id="num21" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num22" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num23" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num21" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num22" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num23" placeholder="0" class="input-block-level text-right"></td>
										</tr>
										<tr>
											<td><p class="text-center">02</p></td>
											<td>ทันตแพทย์</td>
											
											<td><input type="text" name="textfield" id="num31" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num32" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num3" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num21" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num22" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num23" placeholder="0" class="input-block-level text-right"></td>
										</tr>
										<tr>
											<td><p class="text-center">03</p></td>
											<td>เจ้าพนักงานทันตสาธารณสุข</td>
											
											<td><input type="text" name="textfield" id="num41" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num42" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num43" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num21" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num22" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num23" placeholder="0" class="input-block-level text-right"></td>
										</tr>
										<tr>
											<td><p class="text-center">04</p></td>
											<td>นายสัตวแพทย์</td>
											
											<td><input type="text" name="textfield" id="num51" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num52" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num53" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num21" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num22" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num23" placeholder="0" class="input-block-level text-right"></td>
										</tr>
										<tr>
											<td><p class="text-center">05</p></td>
											<td>สัตวแพทย์</td>
											
											<td><input type="text" name="textfield" id="num61" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num62" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num63" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num21" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num22" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num23" placeholder="0" class="input-block-level text-right"></td>
										</tr>
										<tr>
											<td><p class="text-center">06</p></td>
											<td>เภสัชกร</td>
											
											<td><input type="text" name="textfield" id="num71" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num72" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num73" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num21" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num22" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num23" placeholder="0" class="input-block-level text-right"></td>
										</tr>
										<tr>
											<td><p class="text-center">07</p></td>
											<td>เจ้าพนักงานเภสัชกรรม</td>
											
											<td><input type="text" name="textfield" id="num81" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num82" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num83" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num21" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num22" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num23" placeholder="0" class="input-block-level text-right"></td>
										</tr>
										<tr>
											<td><p class="text-center">08</p></td>
											<td>พยาบาลวิชาชีพ</td>
											
											<td><input type="text" name="textfield" id="num81" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num82" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num83" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num21" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num22" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num23" placeholder="0" class="input-block-level text-right"></td>
										</tr>
										<tr>
											<td><p class="text-center">09</p></td>
											<td>พยาบาลเทคนิค</td>
											
											<td><input type="text" name="textfield" id="num81" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num82" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num83" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num21" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num22" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num23" placeholder="0" class="input-block-level text-right"></td>
										</tr>
										<tr>
											<td><p class="text-center">10</p></td>
											<td>วิสัญญีพยาบาล</td>
											
											<td><input type="text" name="textfield" id="num81" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num82" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num83" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num21" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num22" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num23" placeholder="0" class="input-block-level text-right"></td>
										</tr>
										<tr>
											<td><p class="text-center">11</p></td>
											<td>เจ้าพนักงานผดุงครรภ์สาธารณสุข</td>
											
											<td><input type="text" name="textfield" id="num81" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num82" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num83" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num21" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num22" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num23" placeholder="0" class="input-block-level text-right"></td>
										</tr>
										<tr>
											<td><p class="text-center">12</p></td>
											<td>นักวิชาการสาธารณสุข</td>
											
											<td><input type="text" name="textfield" id="num81" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num82" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num83" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num21" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num22" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num23" placeholder="0" class="input-block-level text-right"></td>
										</tr>
										<tr>
											<td><p class="text-center">13</p></td>
											<td>นักวิชาการสุขาภิบาล</td>
											
											<td><input type="text" name="textfield" id="num81" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num82" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num83" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num21" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num22" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num23" placeholder="0" class="input-block-level text-right"></td>
										</tr>
										<tr>
											<td><p class="text-center">14</p></td>
											<td>นักกายภาพบำบัด</td>
											
											<td><input type="text" name="textfield" id="num81" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num82" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num83" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num21" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num22" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num23" placeholder="0" class="input-block-level text-right"></td>
										</tr>
										<tr>
											<td><p class="text-center">15</p></td>
											<td>นักอาชีวบำบัด</td>
											
											<td><input type="text" name="textfield" id="num81" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num82" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num83" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num21" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num22" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num23" placeholder="0" class="input-block-level text-right"></td>
										</tr>
										<tr>
											<td><p class="text-center">16</p></td>
											<td>เจ้าพนักงานอาชีวบำบัด</td>
											
											<td><input type="text" name="textfield" id="num81" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num82" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num83" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num21" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num22" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num23" placeholder="0" class="input-block-level text-right"></td>
										</tr>
										<tr>
											<td><p class="text-center">17</p></td>
											<td>เจ้าพนักงานเวชกรรมฟื้นฟู</td>
											
											<td><input type="text" name="textfield" id="num81" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num82" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num83" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num21" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num22" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num23" placeholder="0" class="input-block-level text-right"></td>
										</tr>
										<tr>
											<td><p class="text-center">18</p></td>
											<td>นักเทคนิกการแพทย์</td>
											
											<td><input type="text" name="textfield" id="num81" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num82" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num83" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num21" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num22" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num23" placeholder="0" class="input-block-level text-right"></td>
										</tr>
										<tr>
											<td><p class="text-center">19</p></td>
											<td>นักวิทยาศาสตร์การแพทย์</td>
											
											<td><input type="text" name="textfield" id="num81" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num82" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num83" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num21" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num22" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num23" placeholder="0" class="input-block-level text-right"></td>
										</tr>
										<tr>
											<td><p class="text-center">20</p></td>
											<td>เจ้าพนักงานวิทยาศาสตร์การแพทย์</td>
											
											<td><input type="text" name="textfield" id="num81" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num82" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num83" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num21" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num22" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num23" placeholder="0" class="input-block-level text-right"></td>
										</tr>
										<tr>
											<td><p class="text-center">21</p></td>
											<td>นักรังสี (นักฟิสิกส์รังสี)</td>
											
											<td><input type="text" name="textfield" id="num81" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num82" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num83" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num21" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num22" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num23" placeholder="0" class="input-block-level text-right"></td>
										</tr>
										<tr>
											<td><p class="text-center">22</p></td>
											<td>นักรังสีการแพทย์</td>
											
											<td><input type="text" name="textfield" id="num81" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num82" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num83" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num21" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num22" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num23" placeholder="0" class="input-block-level text-right"></td>
										</tr>
										<tr>
											<td><p class="text-center">23</p></td>
											<td>เจ้าพนักงานรังสีการแพทย์</td>
											
											<td><input type="text" name="textfield" id="num81" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num82" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num83" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num21" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num22" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num23" placeholder="0" class="input-block-level text-right"></td>
										</tr>
										<tr>
											<td><p class="text-center">24</p></td>
											<td>นักโภชนาการ</td>
											
											<td><input type="text" name="textfield" id="num81" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num82" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num83" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num21" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num22" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num23" placeholder="0" class="input-block-level text-right"></td>
										</tr>
										<tr>
											<td><p class="text-center">25</p></td>
											<td>โภชนากร</td>
											
											<td><input type="text" name="textfield" id="num81" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num82" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num83" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num21" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num22" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num23" placeholder="0" class="input-block-level text-right"></td>
										</tr>
										<tr>
											<td><p class="text-center">26</p></td>
											<td>นักจิตวิทยา</td>
											
											<td><input type="text" name="textfield" id="num81" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num82" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num83" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num21" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num22" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num23" placeholder="0" class="input-block-level text-right"></td>
										</tr>
										<tr>
											<td><p class="text-center">27</p></td>
											<td>นักสังคมสงเคราะห์</td>
											
											<td><input type="text" name="textfield" id="num81" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num82" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num83" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num21" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num22" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num23" placeholder="0" class="input-block-level text-right"></td>
										</tr>
										<tr>
											<td><p class="text-center">28</p></td>
											<td>นักวิชาการสถิติ</td>
											
											<td><input type="text" name="textfield" id="num81" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num82" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num83" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num21" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num22" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num23" placeholder="0" class="input-block-level text-right"></td>
										</tr>
										<tr>
											<td><p class="text-center">29</p></td>
											<td>เจ้าพนักงานเวชสถิติ</td>
											
											<td><input type="text" name="textfield" id="num81" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num82" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num83" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num21" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num22" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num23" placeholder="0" class="input-block-level text-right"></td>
										</tr>
										<tr>
											<td><p class="text-center">30</p></td>
											<td>เจ้าพนักงานสถิติ</td>
											
											<td><input type="text" name="textfield" id="num81" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num82" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num83" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num21" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num22" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num23" placeholder="0" class="input-block-level text-right"></td>
										</tr>
										<tr>
											<td><p class="text-center">31</p></td>
											<td>เจ้าพนักงานสาธารณสุข</td>
											
											<td><input type="text" name="textfield" id="num81" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num82" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num83" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num21" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num22" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num23" placeholder="0" class="input-block-level text-right"></td>
										</tr>
										<tr>
											<td><p class="text-center">32</p></td>
											<td>นักวิเคราะห์นโยบายและแผน</td>
											
											<td><input type="text" name="textfield" id="num81" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num82" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num83" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num21" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num22" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num23" placeholder="0" class="input-block-level text-right"></td>
										</tr>
										<tr>
											<td><p class="text-center">33</p></td>
											<td>ผู้ปฏิบัติงานด้านการแพทย์แผนไทย</td>
											
											<td><input type="text" name="textfield" id="num81" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num82" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num83" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num21" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num22" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num23" placeholder="0" class="input-block-level text-right"></td>
										</tr>
										<tr>
											<td><p class="text-center">34</p></td>
											<td>นักวิชาการเงินและบัญชี</td>
											
											<td><input type="text" name="textfield" id="num81" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num82" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num83" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num21" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num22" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num23" placeholder="0" class="input-block-level text-right"></td>
										</tr>
										<tr>
											<td><p class="text-center">35</p></td>
											<td>นักวิชาการคอมพิวเตอร์</td>
											
											<td><input type="text" name="textfield" id="num81" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num82" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num83" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num21" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num22" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num23" placeholder="0" class="input-block-level text-right"></td>
										</tr>
										<tr>
											<td><p class="text-center">36</p></td>
											<td>นักจัดการงานทั่วไป</td>
											
											<td><input type="text" name="textfield" id="num81" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num82" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num83" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num21" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num22" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num23" placeholder="0" class="input-block-level text-right"></td>
										</tr>
										<tr>
											<td><p class="text-center">37</p></td>
											<td>นักทรัพยากรบุคคล</td>
											
											<td><input type="text" name="textfield" id="num81" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num82" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num83" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num21" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num22" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num23" placeholder="0" class="input-block-level text-right"></td>
										</tr>
										<tr>
											<td><p class="text-center">38</p></td>
											<td>เจ้าพนักงานโสตทัศนศึกษา</td>
											
											<td><input type="text" name="textfield" id="num81" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num82" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num83" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num21" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num22" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num23" placeholder="0" class="input-block-level text-right"></td>
										</tr>
										<tr>
											<td><p class="text-center">39</p></td>
											<td>นักวิชาการอาหารและยา</td>
											
											<td><input type="text" name="textfield" id="num81" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num82" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num83" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num21" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num22" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num23" placeholder="0" class="input-block-level text-right"></td>
										</tr>
										<tr>
											<td><p class="text-center">40</p></td>
											<td>นักกิจกรรมบำบัด</td>
											
											<td><input type="text" name="textfield" id="num81" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num82" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num83" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num21" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num22" placeholder="0" class="input-block-level text-right"></td>
											<td><input type="text" name="textfield" id="num23" placeholder="0" class="input-block-level text-right"></td>
										</tr>
										</tbody>
									</table>
								<div class="span9">
									        <div class="form-actions pull-right">
										        <button type="submit" class="btn btn-green"><i class="icon-ok"></i> บันทึกข้อมูล</button>
										        <button type="submit" class="btn btn-green"><i class="icon-ok"></i> ยืนยันและส่งข้อมูล</button>
										        <button type="button" class="btn btn-lightred"><i class="icon-remove"></i> ยกเลิก</button>
									        </div>
									</div>
							</div>
						</div>
				   	</div>

				</div>
				
			</div>
		</div>
    </div>
</asp:Content>













