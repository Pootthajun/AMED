﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="Form_Draft.aspx.cs" Inherits="AMED.Form_Draft" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <title>รายงานการตรวจโรค - กรมการแพทย์ทหารบก</title>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <div class="container-fluid" id="content">
		<div id="left">
			<div class="subnav">
				<div class="subnav-title">
					<a href="#" class='toggle-subnav'><i class="icon-angle-down"></i><span><p class="icon-building"></p> หน่วยสายแพทย์</span></a>
				</div>
				<ul class="subnav-menu">
					<li>
						<a href="Default.aspx">สรุปภาพรวม</a>
					</li>
					<li class="active">
							<a href="Hotpital.aspx">การบันทึกข้อมูล</a>
							
						</li>
					<li>
						<a href="Report.aspx">สรุปรายงาน</a>
					</li>
				</ul>
			</div>
			<div class="subnav">
				<div class="subnav-title">
					<a href="#" class='toggle-subnav'><i class="icon-angle-down"></i><span><p class="icon-building"></p>  ส่วนกลาง</span></a>
				</div>
				<ul class="subnav-menu">
					<li>
						<a href="Default.aspx">สรุปภาพรวม</a>
					</li>
					<li>
							<a href="Department.aspx">การตรวจสอบการบันทึกข้อมูล</a>
							
					</li>
					<li>
						<a href="Report.aspx">สรุปรายงาน</a>
					</li>
				</ul>
			</div>
            <div class="subnav">
				<div class="subnav-title">
					<a href="#" class='toggle-subnav'><i class="icon-angle-down"></i><span><p class="icon-cog"></p> ข้อมูลพื้นฐาน</span></a>
				</div>
				<ul class="subnav-menu">
					<li class='dropdown'>
						<a href="#" data-toggle="dropdown"> ข้อมุลทั่วไป</a>
						<ul class="dropdown-menu">
							<li>
								<a href="MT_Region.aspx"> กองทัพภาค</a>
							</li>
							<li>
								<a href="MT_Hospital.aspx"> หน่วยสายแพทย์</a>
							</li>
							<li>
								<a href="MT_Department.aspx"> หน่วยงาน (ส่วนกลาง)</a>
							</li>
							<li>
								<a href="MT_AddOrganization.aspx"> สังกัด</a>
							</li>
							<li>
								<a href="MT_Province.aspx"> จังหวัด</a>
							</li>
						</ul>
					</li>
					<li class='dropdown'>
						<a href="#" data-toggle="dropdown">กองวิทยาการ</a>
						<ul class="dropdown-menu">
							<li>
								<a href="MTS_ICD10.aspx"> รหัสโรค (ICD10)</a>
							</li>
							<li>
								<a href="MTS_GroupRule.aspx"> กลุ่มโรคที่ขัดต่อกฎกระทรวง</a>
							</li>
							<li>
								<a href="MTS_Rule.aspx"> โรคที่ขัดต่อกฎกระทรวง</a>
							</li>
							<li>
								<a href="MTS_Sizebed.aspx"> ขนาดเตียง</a>
							</li>
							<li>
								<a href="MTS_ProHotpital.aspx"> ตำแหน่งในโรงพยาบาล</a>
							</li>
							<li>
								<a href="MTS_Wards.aspx"> หอผู้ป่วย</a>
							</li>
						</ul>
					</li>
                    <li class='dropdown'>
						<a href="#" data-toggle="dropdown">กองงบประมาณการเงิน</a>
						<ul class="dropdown-menu">
							<li>
								<a href="MTF_Assets.aspx">สินทรัพย์และหนี้สิน</a>
							</li>
							<li>
								<a href="MTF_Progress.aspx">ผลการดำเนินงาน</a>
							</li>
							<li>
								<a href="MTF_Reception.aspx">การรับ-การจ่าย</a>
							</li>
							<li>
								<a href="MTF_Deposit.aspx">เงินฝาก</a>
							</li>
						</ul>
					</li>
                    <li class='dropdown'>
						<a href="#" data-toggle="dropdown">กองส่งกำลังบำรุง</a>
						<ul class="dropdown-menu">
							<li>
								<a href="MTT_TypeDrug.aspx"> ประเภทยา</a>
							</li>
							<li>
								<a href="MTT_Drug.aspx"> ยา</a>
							</li>
							<li>
								<a href="MTT_DrugNature.aspx">ลักษณะยา</a>
							</li>
							<li>
								<a href="MTT_MedicalSupplies.aspx">เวชภัณฑ์</a>
							</li>
						</ul>
					</li>
                    <li class='dropdown'>
						<a href="#" data-toggle="dropdown">ผู้ใช้งาน (User)</a>
						<ul class="dropdown-menu">
							<li>
								<a href="MT_PersonType.aspx"> ประเภทบุคคล</a>
							</li>
							<li>
								<a href="MT_Person.aspx">บุคคลย่อย</a>
							</li>
							<li>
								<a href="#">กำหนดสิทธิ์</a>
							</li>
							<li>
								<a href="MT_User.aspx">ผู้ใช้งานในระบบ</a>
							</li>
						</ul>
					</li>
                     <li class='dropdown'>
						<a href="#" data-toggle="dropdown">การส่งรายงาน</a>
						<ul class="dropdown-menu">
							<li>
								<a href="MTR_DeadlineReports.aspx"> กำหนดส่งรายงาน</a>
							</li>
							<li>
								<a href="MTR_EditReport.aspx"> แก้ไขข้อมูลรายงาน</a>
							</li>
							<li>
								<a href="MTR_NameReport.aspx"> ชื่อแบบฟอร์มรายงาน</a>
							</li>
						</ul>
					</li>
				</ul>
			</div>
		</div>
			
		<div id="main">
			<div class="container-fluid">
				<div class="page-header">
					<div class="pull-left">
						<h4>รายงานการตรวจโรคชายไทยก่อนเกณฑ์ทหาร</h4>
					</div>
					
				</div>
				<div class="breadcrumbs">
					<ul>
						<li>
							<a href="Default.aspx">Dashborad</a>
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<a href="forms-basic.html">กองวิทยาการ</a>
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<a href="MT_User.aspx">การตรวจโรคชายไทยก่อนเกณฑ์ทหาร</a>
						</li>
					</ul>
					<div class="close-bread">
						<a href="#"><i class="icon-remove"></i></a>
					</div>
				</div>
				

				<div class="row-fluid">

						<div class="span12">
						  <div class="box">
                             <div class="box-title">
                                <div class='basic-margin'>
								<div class="fileupload fileupload-new" data-provides="fileupload">
									<div class="input-append">
										<div class="uneditable-input span3"><i class="icon-file fileupload-exists"></i> <span class="fileupload-preview"></span></div><span class="btn btn-file"><span class="fileupload-new">Select file</span><span class="fileupload-exists">Change</span><input type="file" name="aaaa" /></span><a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
								    	</div>
								   <div class="btn-group span2 pull-right">
                                  <%-- <button class="btn btn-info" rel="tooltip" title="edit"><i class="icon-edit"></i> แก้ไขเอกสาร</button>--%>
												<a href="#" data-toggle="dropdown" class="btn btn-warning dropdown-toggle" rel="tooltip" title="print"><i class="icon-print"></i> พิมพ์รายงาน <span class="caret"></span></a>
												<ul class="dropdown-menu dropdown-warning">
													<li>
														<a href="#"><i class="icon-file"></i> Excel</a>
													</li>
													<li>
														<a href="#"><i class="icon-file"></i> PDF</a>
													</li>
												</ul>
											</div></div>
                                </div>
							</div>
                            <br />
                              
                           <div style="overflow-x:auto;" class="box-content nopadding"><br />
                               <form action="#" method="get" class='form-vertical'>
                                    <div class="span2">
											<div class="control-group">
												<label for="textfield" class="control-label">กองทัพ</label>
                                                <asp:DropDownList ID="DropDownList13" runat="server" Width="150px">
                                                <asp:ListItem Text="หน่วยงาน (ส่วนกลาง)" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="กองทัพภาคที่ 2" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="กองทัพภาคที่ 3" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="กองทัพภาคที่ 4" Value="3"></asp:ListItem>
                                            </asp:DropDownList>
											</div>
                                            
										</div>
										<div class="span3">
                                            <div class="control-group">
												<label for="textfield" class="control-label">หน่วยสายแพทย์</label>
                                                <asp:DropDownList ID="DropDownList14" runat="server" Width="230px">
                                                <asp:ListItem Text="ทั้งหมด" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="รพ.พระมงกุฎเกล้า" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="รพ.อานันทมหิดล" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="รพ.ค่ายสุรนารี" Value="3"></asp:ListItem>
                                            </asp:DropDownList>
											</div>
										</div>
                                          <div class="span2">
										    <div class="control-group">
												    <label for="textfield" class="control-label">ปีงบประมาณ</label>
                                                    <asp:DropDownList ID="DropDownList15" runat="server" Width="150px">
                                                    <asp:ListItem Text="ทั้งหมด" Value="0"></asp:ListItem>
                                                    <asp:ListItem Text="2560" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="2559" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="2558" Value="3"></asp:ListItem>
                                                    <asp:ListItem Text="2557" Value="4"></asp:ListItem>
                                                </asp:DropDownList>
											    </div>
                                              </div>
										<div class="span3">
                                            <div class="control-group">
												<label for="textfield" class="control-label">ประจำเดือน</label>
                                                <asp:DropDownList ID="DropDownList16" runat="server" Width="230px">
                                                <asp:ListItem Text="ทั้งหมด" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="มกราคม" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="กุมภาพันธ์" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="มีนาคม" Value="3"></asp:ListItem>
                                                <asp:ListItem Text="เมษายน" Value="4"></asp:ListItem>
                                                <asp:ListItem Text="พฤษภาคม" Value="5"></asp:ListItem>
                                                <asp:ListItem Text="กรกฎาคม" Value="6"></asp:ListItem>
                                                <asp:ListItem Text="สิงหาคม" Value="7"></asp:ListItem>
                                                <asp:ListItem Text="กันยายน" Value="8"></asp:ListItem>
                                                <asp:ListItem Text="ตุลาคม" Value="9"></asp:ListItem>
                                                <asp:ListItem Text="พฤศจิกายน" Value="10"></asp:ListItem>
                                                <asp:ListItem Text="ธันวาคม" Value="11"></asp:ListItem>
                                            </asp:DropDownList>
											</div>
										</div>
									
								</form>
                                  <table class="table table-hover table-nomargin table-bordered dataTable dataTable-noheader dataTable-fixedcolumn dataTable-scroll-x dataTable-scroll-y table-edit">
									<thead>
										<tr>
											<th style="width: 20px"><p class="text-center">ลำดับ</p></th>
											<th style="width: 100px"><p class="text-center">ชื่อ</p></th>
											<th style="width: 100px"><p class="text-center">นามสกุล</p></th>
											<th style="width: 50px"><p class="text-center">เลขประจำตัวประชาชน</p></th>
											<th style="width: 150px"><p class="text-center">โรคที่ตรวจพบ</p></th>
											<th style="width: 150px"><p class="text-center">กฎระทรวงที่ปลด</p></th>
											<th style="width: 100px"><p class="text-center">คณะกรรมการแพทย์ผู้ตรวจ (1)</p></th>
											<th style="width: 100px"><p class="text-center">คณะกรรมการแพทย์ผู้ตรวจ (2)</p></th>
											<th style="width: 100px"><p class="text-center">คณะกรรมการแพทย์ผู้ตรวจ (3)</p></th>
											<th style="width: 200px"><p class="text-center">ภูมิลำเนาทหาร (เลขที่/หมู่ที่/ตำบล/อำเภอ)</p></th>
											<th style="width: 50px"><p class="text-center">ภูมิลำเนาทหาร (จังหวัด)</p></th>
											<th style="width: 50px"><p class="text-center">วัน/เดือน/ปี  ที่ออกใบรับรองแพทย์</p></th>
											<th style="width: 150px"><p class="text-center">หมายเหตุ</p></th>
										</tr>
									</thead>
									<tbody>
                                        <tr>
											<td><p class="text-center">01</p></td>

											<td><asp:TextBox ID="TextBox7" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox8" runat="server" CssClass="input-block-level text-right" Width="200px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox9" runat="server" CssClass="input-block-level text-right"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox10" runat="server" CssClass="input-block-level text-right"></asp:TextBox></td>
											<td>
                                            <asp:DropDownList ID="DropDownList12" Class="control" runat="server" Width="150px">
                                                <asp:ListItem Text="สังกัด" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="สังกัด 1" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="สังกัด 2" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="สังกัด 3" Value="3"></asp:ListItem>
                                                <asp:ListItem Text="สังกัด 4" Value="4"></asp:ListItem>
                                            </asp:DropDownList>
                                           </td>
											<td><asp:TextBox ID="txtNum60" runat="server" CssClass="input-block-level text-right"></asp:TextBox></td>
											<td><asp:TextBox ID="txtNum61" runat="server" CssClass="input-block-level text-right"></asp:TextBox></td>
											<td><asp:TextBox ID="txtNum62" runat="server" CssClass="input-block-level text-right"></asp:TextBox></td>
											<td><asp:TextBox ID="txtNum63" runat="server" CssClass="input-block-level text-right"></asp:TextBox></td>
											<td><asp:TextBox ID="txtNum64" runat="server" CssClass="input-block-level text-right"></asp:TextBox></td>
											<td><asp:TextBox ID="txtNum65" runat="server" CssClass="input-block-level text-right"></asp:TextBox></td>
											<td><asp:TextBox ID="txtNum66" runat="server" CssClass="input-block-level text-right"></asp:TextBox></td>
										</tr>
										<tr>
											<td><p class="text-center">02</p></td>
											
											<td><asp:TextBox ID="TextBox11" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox14" runat="server" CssClass="input-block-level text-right" Width="200px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox12" runat="server" CssClass="input-block-level text-right"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox13" runat="server" CssClass="input-block-level text-right"></asp:TextBox></td>
											<td>
                                            <asp:DropDownList ID="DropDownList11" Class="control" runat="server" Width="150px">
                                                <asp:ListItem Text="สังกัด" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="สังกัด 1" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="สังกัด 2" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="สังกัด 3" Value="3"></asp:ListItem>
                                                <asp:ListItem Text="สังกัด 4" Value="4"></asp:ListItem>
                                            </asp:DropDownList>
                                           </td>
											<td><asp:TextBox ID="TextBox15" runat="server" CssClass="input-block-level text-right"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox1" runat="server" CssClass="input-block-level text-right"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox2" runat="server" CssClass="input-block-level text-right"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox3" runat="server" CssClass="input-block-level text-right"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox4" runat="server" CssClass="input-block-level text-right"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox5" runat="server" CssClass="input-block-level text-right"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox6" runat="server" CssClass="input-block-level text-right"></asp:TextBox></td>
										</tr>
										<tr>
											<td><p class="text-center">03</p></td>
											
											<td><asp:TextBox ID="TextBox16" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox17" runat="server" CssClass="input-block-level text-right" Width="200px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox18" runat="server" CssClass="input-block-level text-right"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox19" runat="server" CssClass="input-block-level text-right"></asp:TextBox></td>
											<td>
                                            <asp:DropDownList ID="DropDownList10" Class="control" runat="server" Width="150px">
                                                <asp:ListItem Text="สังกัด" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="สังกัด 1" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="สังกัด 2" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="สังกัด 3" Value="3"></asp:ListItem>
                                                <asp:ListItem Text="สังกัด 4" Value="4"></asp:ListItem>
                                            </asp:DropDownList>
                                           </td>
											<td><asp:TextBox ID="TextBox20" runat="server" CssClass="input-block-level text-right"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox21" runat="server" CssClass="input-block-level text-right"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox22" runat="server" CssClass="input-block-level text-right"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox23" runat="server" CssClass="input-block-level text-right"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox24" runat="server" CssClass="input-block-level text-right"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox25" runat="server" CssClass="input-block-level text-right"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox26" runat="server" CssClass="input-block-level text-right"></asp:TextBox></td>
										</tr>
										<tr>
											<td><p class="text-center">04</p></td>
											
											<td><asp:TextBox ID="TextBox27" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox28" runat="server" CssClass="input-block-level text-right" Width="200px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox29" runat="server" CssClass="input-block-level text-right"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox30" runat="server" CssClass="input-block-level text-right"></asp:TextBox></td>
											<td>
                                            <asp:DropDownList ID="DropDownList9" Class="control" runat="server" Width="150px">
                                                <asp:ListItem Text="สังกัด" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="สังกัด 1" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="สังกัด 2" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="สังกัด 3" Value="3"></asp:ListItem>
                                                <asp:ListItem Text="สังกัด 4" Value="4"></asp:ListItem>
                                            </asp:DropDownList>
                                           </td>
											<td><asp:TextBox ID="TextBox31" runat="server" CssClass="input-block-level text-right"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox32" runat="server" CssClass="input-block-level text-right"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox33" runat="server" CssClass="input-block-level text-right"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox34" runat="server" CssClass="input-block-level text-right"></asp:TextBox></td>
											<td><asp:TextBox ID="txtNum25" runat="server" CssClass="input-block-level text-right"></asp:TextBox></td>
											<td><asp:TextBox ID="txtNum26" runat="server" CssClass="input-block-level text-right"></asp:TextBox></td>
											<td><asp:TextBox ID="txtNum27" runat="server" CssClass="input-block-level text-right"></asp:TextBox></td>
										</tr>
										<tr>
											<td><p class="text-center">05</p></td>
											
											<td><asp:TextBox ID="TextBox35" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox77" runat="server" CssClass="input-block-level text-right" Width="200px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox36" runat="server" CssClass="input-block-level text-right"></asp:TextBox></td>
											<td><asp:TextBox ID="txtNum29" runat="server" CssClass="input-block-level text-right"></asp:TextBox></td>
											<td>
                                            <asp:DropDownList ID="DropDownList8" Class="control" runat="server" Width="150px">
                                                <asp:ListItem Text="สังกัด" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="สังกัด 1" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="สังกัด 2" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="สังกัด 3" Value="3"></asp:ListItem>
                                                <asp:ListItem Text="สังกัด 4" Value="4"></asp:ListItem>
                                            </asp:DropDownList>
                                           </td>
											<td><asp:TextBox ID="txtNum30" runat="server" CssClass="input-block-level text-right"></asp:TextBox></td>
											<td><asp:TextBox ID="txtNum31" runat="server" CssClass="input-block-level text-right"></asp:TextBox></td>
											<td><asp:TextBox ID="txtNum32" runat="server" CssClass="input-block-level text-right"></asp:TextBox></td>
											<td><asp:TextBox ID="txtNum33" runat="server" CssClass="input-block-level text-right"></asp:TextBox></td>
											<td><asp:TextBox ID="txtNum34" runat="server" CssClass="input-block-level text-right"></asp:TextBox></td>
											<td><asp:TextBox ID="txtNum35" runat="server" CssClass="input-block-level text-right"></asp:TextBox></td>
											<td><asp:TextBox ID="txtNum36" runat="server" CssClass="input-block-level text-right"></asp:TextBox></td>
										</tr>
										<tr>
											<td><p class="text-center">06</p></td>
											
											<td><asp:TextBox ID="TextBox37" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox78" runat="server" CssClass="input-block-level text-right" Width="200px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox38" runat="server" CssClass="input-block-level text-right"></asp:TextBox></td>
											<td><asp:TextBox ID="txtNum38" runat="server" CssClass="input-block-level text-right"></asp:TextBox></td>
											<td>
                                            <asp:DropDownList ID="DropDownList7" Class="control" runat="server" Width="150px">
                                                <asp:ListItem Text="สังกัด" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="สังกัด 1" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="สังกัด 2" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="สังกัด 3" Value="3"></asp:ListItem>
                                                <asp:ListItem Text="สังกัด 4" Value="4"></asp:ListItem>
                                            </asp:DropDownList>
                                           </td>
											<td><asp:TextBox ID="txtNum39" runat="server" CssClass="input-block-level text-right"></asp:TextBox></td>
											<td><asp:TextBox ID="txtNum40" runat="server" CssClass="input-block-level text-right"></asp:TextBox></td>
											<td><asp:TextBox ID="txtNum41" runat="server" CssClass="input-block-level text-right"></asp:TextBox></td>
											<td><asp:TextBox ID="txtNum42" runat="server" CssClass="input-block-level text-right"></asp:TextBox></td>
											<td><asp:TextBox ID="txtNum43" runat="server" CssClass="input-block-level text-right"></asp:TextBox></td>
											<td><asp:TextBox ID="txtNum44" runat="server" CssClass="input-block-level text-right"></asp:TextBox></td>
											<td><asp:TextBox ID="txtNum45" runat="server" CssClass="input-block-level text-right"></asp:TextBox></td>
										</tr>
										<tr>
											<td><p class="text-center">07</p></td>
											
											<td><asp:TextBox ID="TextBox39" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox79" runat="server" CssClass="input-block-level text-right" Width="200px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox40" runat="server" CssClass="input-block-level text-right"></asp:TextBox></td>
											<td><asp:TextBox ID="txtNum47" runat="server" CssClass="input-block-level text-right"></asp:TextBox></td>
											<td>
                                            <asp:DropDownList ID="DropDownList6" Class="control" runat="server" Width="150px">
                                                <asp:ListItem Text="สังกัด" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="สังกัด 1" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="สังกัด 2" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="สังกัด 3" Value="3"></asp:ListItem>
                                                <asp:ListItem Text="สังกัด 4" Value="4"></asp:ListItem>
                                            </asp:DropDownList>
                                           </td>
											<td><asp:TextBox ID="txtNum48" runat="server" CssClass="input-block-level text-right"></asp:TextBox></td>
											<td><asp:TextBox ID="txtNum49" runat="server" CssClass="input-block-level text-right"></asp:TextBox></td>
											<td><asp:TextBox ID="txtNum50" runat="server" CssClass="input-block-level text-right"></asp:TextBox></td>
											<td><asp:TextBox ID="txtNum51" runat="server" CssClass="input-block-level text-right"></asp:TextBox></td>
											<td><asp:TextBox ID="txtNum52" runat="server" CssClass="input-block-level text-right"></asp:TextBox></td>
											<td><asp:TextBox ID="txtNum53" runat="server" CssClass="input-block-level text-right"></asp:TextBox></td>
											<td><asp:TextBox ID="txtNum54" runat="server" CssClass="input-block-level text-right"></asp:TextBox></td>
										</tr>
										<tr>
											<td><p class="text-center">08</p></td>
											
											<td><asp:TextBox ID="TextBox41" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox80" runat="server" CssClass="input-block-level text-right" Width="200px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox42" runat="server" CssClass="input-block-level text-right"></asp:TextBox></td>
											<td><asp:TextBox ID="txtNum56" runat="server" CssClass="input-block-level text-right"></asp:TextBox></td>
											<td>
                                            <asp:DropDownList ID="DropDownList5" Class="control" runat="server" Width="150px">
                                                <asp:ListItem Text="สังกัด" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="สังกัด 1" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="สังกัด 2" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="สังกัด 3" Value="3"></asp:ListItem>
                                                <asp:ListItem Text="สังกัด 4" Value="4"></asp:ListItem>
                                            </asp:DropDownList>
                                           </td>
											<td><asp:TextBox ID="txtNum57" runat="server" CssClass="input-block-level text-right"></asp:TextBox></td>
											<td><asp:TextBox ID="txtNum58" runat="server" CssClass="input-block-level text-right"></asp:TextBox></td>
											<td><asp:TextBox ID="txtNum59" runat="server" CssClass="input-block-level text-right"></asp:TextBox></td>
											<td><asp:TextBox ID="txtNum67" runat="server" CssClass="input-block-level text-right"></asp:TextBox></td>
											<td><asp:TextBox ID="txtNum68" runat="server" CssClass="input-block-level text-right"></asp:TextBox></td>
											<td><asp:TextBox ID="txtNum69" runat="server" CssClass="input-block-level text-right"></asp:TextBox></td>
											<td><asp:TextBox ID="txtNum70" runat="server" CssClass="input-block-level text-right"></asp:TextBox></td>
										</tr>
										<tr>
											<td><p class="text-center">09</p></td>
											
											<td><asp:TextBox ID="TextBox43" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox81" runat="server" CssClass="input-block-level text-right" Width="200px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox44" runat="server" CssClass="input-block-level text-right"></asp:TextBox></td>
											<td><asp:TextBox ID="txtNum72" runat="server" CssClass="input-block-level text-right"></asp:TextBox></td>
											<td>
                                            <asp:DropDownList ID="DropDownList4" Class="control" runat="server" Width="150px">
                                                <asp:ListItem Text="สังกัด" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="สังกัด 1" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="สังกัด 2" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="สังกัด 3" Value="3"></asp:ListItem>
                                                <asp:ListItem Text="สังกัด 4" Value="4"></asp:ListItem>
                                            </asp:DropDownList>
                                           </td>
											<td><asp:TextBox ID="txtNum73" runat="server" CssClass="input-block-level text-right"></asp:TextBox></td>
											<td><asp:TextBox ID="txtNum74" runat="server" CssClass="input-block-level text-right"></asp:TextBox></td>
											<td><asp:TextBox ID="txtNum75" runat="server" CssClass="input-block-level text-right"></asp:TextBox></td>
											<td><asp:TextBox ID="txtNum76" runat="server" CssClass="input-block-level text-right"></asp:TextBox></td>
											<td><asp:TextBox ID="txtNum77" runat="server" CssClass="input-block-level text-right"></asp:TextBox></td>
											<td><asp:TextBox ID="txtNum78" runat="server" CssClass="input-block-level text-right"></asp:TextBox></td>
											<td><asp:TextBox ID="txtNum79" runat="server" CssClass="input-block-level text-right"></asp:TextBox></td>
										</tr>
										<tr>
											<td><p class="text-center">10</p></td>
											
											<td><asp:TextBox ID="TextBox45" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox82" runat="server" CssClass="input-block-level text-right" Width="200px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox46" runat="server" CssClass="input-block-level text-right"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox47" runat="server" CssClass="input-block-level text-right"></asp:TextBox></td>
											<td>
                                            <asp:DropDownList ID="DropDownList3" Class="control" runat="server" Width="150px">
                                                <asp:ListItem Text="สังกัด" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="สังกัด 1" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="สังกัด 2" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="สังกัด 3" Value="3"></asp:ListItem>
                                                <asp:ListItem Text="สังกัด 4" Value="4"></asp:ListItem>
                                            </asp:DropDownList>
                                           </td>
											<td><asp:TextBox ID="TextBox48" runat="server" CssClass="input-block-level text-right"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox49" runat="server" CssClass="input-block-level text-right"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox50" runat="server" CssClass="input-block-level text-right"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox51" runat="server" CssClass="input-block-level text-right"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox52" runat="server" CssClass="input-block-level text-right"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox53" runat="server" CssClass="input-block-level text-right"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox54" runat="server" CssClass="input-block-level text-right"></asp:TextBox></td>
										</tr>
										<tr>
											<td><p class="text-center">11</p></td>
											
											<td><asp:TextBox ID="TextBox55" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox56" runat="server" CssClass="input-block-level text-right" Width="200px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox57" runat="server" CssClass="input-block-level text-right"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox58" runat="server" CssClass="input-block-level text-right"></asp:TextBox></td>
											<td>
                                            <asp:DropDownList ID="DropDownList2" Class="control" runat="server" Width="150px">
                                                <asp:ListItem Text="สังกัด" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="สังกัด 1" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="สังกัด 2" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="สังกัด 3" Value="3"></asp:ListItem>
                                                <asp:ListItem Text="สังกัด 4" Value="4"></asp:ListItem>
                                            </asp:DropDownList>
                                           </td>
											<td><asp:TextBox ID="TextBox59" runat="server" CssClass="input-block-level text-right"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox60" runat="server" CssClass="input-block-level text-right"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox61" runat="server" CssClass="input-block-level text-right"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox62" runat="server" CssClass="input-block-level text-right"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox63" runat="server" CssClass="input-block-level text-right"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox64" runat="server" CssClass="input-block-level text-right"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox65" runat="server" CssClass="input-block-level text-right"></asp:TextBox></td>
										</tr>
										<tr>
											<td><p class="text-center">12</p></td>
											<td><asp:TextBox ID="TextBox66" runat="server" CssClass="input-block-level text-right" Width="150px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox67" runat="server" CssClass="input-block-level text-right" Width="200px"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox68" runat="server" CssClass="input-block-level text-right"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox69" runat="server" CssClass="input-block-level text-right"></asp:TextBox></td>
											<td>
                                            <asp:DropDownList ID="DropDownList1" Class="control" runat="server" Width="150px">
                                                <asp:ListItem Text="สังกัด" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="สังกัด 1" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="สังกัด 2" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="สังกัด 3" Value="3"></asp:ListItem>
                                                <asp:ListItem Text="สังกัด 4" Value="4"></asp:ListItem>
                                            </asp:DropDownList>
                                           </td>
											<td><asp:TextBox ID="TextBox70" runat="server" CssClass="input-block-level text-right"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox71" runat="server" CssClass="input-block-level text-right"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox72" runat="server" CssClass="input-block-level text-right"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox73" runat="server" CssClass="input-block-level text-right"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox74" runat="server" CssClass="input-block-level text-right"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox75" runat="server" CssClass="input-block-level text-right"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox76" runat="server" CssClass="input-block-level text-right"></asp:TextBox></td>
										</tr>
										
									</tbody>
								</table>
						              <div class="span9">
									        <div class="form-actions pull-right">
										        <button type="submit" class="btn btn-green"><i class="icon-ok"></i> บันทึกข้อมูล</button>
										        <button type="submit" class="btn btn-green"><i class="icon-ok"></i> ยืนยันและส่งข้อมูล</button>
										        <button type="button" class="btn btn-lightred"><i class="icon-remove"></i> ยกเลิก</button>
									        </div>
									</div>
							</div>
						</div>
				   	</div>

				</div>
				
			</div>
		</div>
    </div>
</asp:Content>













