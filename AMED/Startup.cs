﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(AMED.Startup))]
namespace AMED
{
    public partial class Startup {
        public void Configuration(IAppBuilder app) {
            ConfigureAuth(app);
        }
    }
}
