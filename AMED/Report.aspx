﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="Report.aspx.cs" Inherits="AMED.Report" %>
<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <title>สรุปรายงาน - กรมการแพทย์ทหารบก</title>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <div class="container-fluid" id="content">
		<div id="left">
			<div class="subnav">
				<div class="subnav-title">
					<a href="#" class='toggle-subnav'><i class="icon-angle-down"></i><span><p class="icon-building"></p> หน่วยสายแพทย์</span></a>
				</div>
				<ul class="subnav-menu">
					<li>
						<a href="Default.aspx">สรุปภาพรวม</a>
					</li>
					<li>
							<a href="Hotpital.aspx">การบันทึกข้อมูล</a>
							
						</li>
					<li class="active">
						<a href="Report.aspx">สรุปรายงาน</a>
					</li>
				</ul>
			</div>
			<div class="subnav">
				<div class="subnav-title">
					<a href="#" class='toggle-subnav'><i class="icon-angle-down"></i><span><p class="icon-building"></p>  ส่วนกลาง</span></a>
				</div>
				<ul class="subnav-menu">
					<li>
						<a href="Default.aspx">สรุปภาพรวม</a>
					</li>
					<li>
							<a href="Department.aspx">การตรวจสอบการบันทึกข้อมูล</a>
							
					</li>
					<li>
						<a href="Report.aspx">สรุปรายงาน</a>
					</li>
				</ul>
			</div>
            <div class="subnav">
				<div class="subnav-title">
					<a href="#" class='toggle-subnav'><i class="icon-angle-down"></i><span><p class="icon-cog"></p> ข้อมูลพื้นฐาน</span></a>
				</div>
				<ul class="subnav-menu">
					<li class='dropdown'>
						<a href="#" data-toggle="dropdown"> ข้อมุลทั่วไป</a>
						<ul class="dropdown-menu">
							<li>
								<a href="MT_Region.aspx"> กองทัพภาค</a>
							</li>
							<li>
								<a href="MT_Hospital.aspx"> หน่วยสายแพทย์</a>
							</li>
							<li>
								<a href="MT_Department.aspx"> หน่วยงาน (ส่วนกลาง)</a>
							</li>
							<li>
								<a href="MT_AddOrganization.aspx"> สังกัด</a>
							</li>
							<li>
								<a href="MT_Province.aspx"> จังหวัด</a>
							</li>
						</ul>
					</li>
					<li class='dropdown'>
						<a href="#" data-toggle="dropdown">กองวิทยาการ</a>
						<ul class="dropdown-menu">
							<li>
								<a href="MTS_ICD10.aspx"> รหัสโรค (ICD10)</a>
							</li>
							<li>
								<a href="MTS_GroupRule.aspx"> กลุ่มโรคที่ขัดต่อกฎกระทรวง</a>
							</li>
							<li>
								<a href="MTS_Rule.aspx"> โรคที่ขัดต่อกฎกระทรวง</a>
							</li>
							<li>
								<a href="MTS_Sizebed.aspx"> ขนาดเตียง</a>
							</li>
							<li>
								<a href="MTS_ProHotpital.aspx"> ตำแหน่งในโรงพยาบาล</a>
							</li>
							<li>
								<a href="MTS_Wards.aspx"> หอผู้ป่วย</a>
							</li>
						</ul>
					</li>
                    <li class='dropdown'>
						<a href="#" data-toggle="dropdown">กองงบประมาณการเงิน</a>
						<ul class="dropdown-menu">
							<li>
								<a href="MTF_Assets.aspx">สินทรัพย์และหนี้สิน</a>
							</li>
							<li>
								<a href="MTF_Progress.aspx">ผลการดำเนินงาน</a>
							</li>
							<li>
								<a href="MTF_Reception.aspx">การรับ-การจ่าย</a>
							</li>
							<li>
								<a href="MTF_Deposit.aspx">เงินฝาก</a>
							</li>
						</ul>
					</li>
                    <li class='dropdown'>
						<a href="#" data-toggle="dropdown">กองส่งกำลังบำรุง</a>
						<ul class="dropdown-menu">
							<li>
								<a href="MTT_TypeDrug.aspx"> ประเภทยา</a>
							</li>
							<li>
								<a href="MTT_Drug.aspx"> ยา</a>
							</li>
							<li>
								<a href="MTT_DrugNature.aspx">ลักษณะยา</a>
							</li>
							<li>
								<a href="MTT_MedicalSupplies.aspx">เวชภัณฑ์</a>
							</li>
						</ul>
					</li>
                    <li class='dropdown'>
						<a href="#" data-toggle="dropdown">ผู้ใช้งาน (User)</a>
						<ul class="dropdown-menu">
							<li>
								<a href="MT_PersonType.aspx"> ประเภทบุคคล</a>
							</li>
							<li>
								<a href="MT_Person.aspx">บุคคลย่อย</a>
							</li>
							<li>
								<a href="#">กำหนดสิทธิ์</a>
							</li>
							<li>
								<a href="MT_User.aspx">ผู้ใช้งานในระบบ</a>
							</li>
						</ul>
					</li>
                     <li class='dropdown'>
						<a href="#" data-toggle="dropdown">การส่งรายงาน</a>
						<ul class="dropdown-menu">
							<li>
								<a href="MTR_DeadlineReports.aspx"> กำหนดส่งรายงาน</a>
							</li>
							<li>
								<a href="MTR_EditReport.aspx"> แก้ไขข้อมูลรายงาน</a>
							</li>
							<li>
								<a href="MTR_NameReport.aspx"> ชื่อแบบฟอร์มรายงาน</a>
							</li>
						</ul>
					</li>
				</ul>
			</div>
		</div>
		<div id="main">
			<div class="container-fluid">
				<div class="page-header">
					<div class="pull-left">
						<h4>สรุปรายงาน</h4>
					</div>
					<div class="pull-right">
						<ul class="stats">
							<li class='satblue' style="line-height:20px">
								<i class="icon-calendar"></i>
								<div class="details">
									<span class="big">December 22, 2013</span>
									<span>Wednesday, 13:56</span>
								</div>
							</li>
						</ul>
					</div>
				</div>
				<div class="breadcrumbs">
					<ul>
						<li>
							<a href="Default.aspx">Dashborad</a>
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<a href="forms-basic.html">กองวิทยาการ</a>
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<a href="MT_User.aspx">สรุปรายงาน</a>
						</li>
					</ul>
					<div class="close-bread">
						<a href="#"><i class="icon-remove"></i></a>
					</div>
				</div>
				

				<div class="row-fluid">

						<div class="span12">
						  <div class="box">
							<div class="box-title">
                                <div class='basic-margin'>
								   <div class="btn-group span2 pull-right">
                                  <%-- <button class="btn btn-info" rel="tooltip" title="edit"><i class="icon-edit"></i> แก้ไขเอกสาร</button>--%>
												<a href="#" data-toggle="dropdown" class="btn btn-warning dropdown-toggle" rel="tooltip" title="print"><i class="icon-print"></i> พิมพ์รายงาน <span class="caret"></span></a>
												<ul class="dropdown-menu dropdown-warning">
													<li>
														<a href="#"><i class="icon-file"></i> Excel</a>
													</li>
													<li>
														<a href="#"><i class="icon-file"></i> PDF</a>
													</li>
												</ul>
											</div>
                                </div>
							</div>
                            <br />

                           <div style="overflow-x:auto;" class="box-content nopadding"><br />
								<form action="#" method="get" class='form-vertical'>
                                    <div class="span1">
											<div class="control-group">
												<label for="textfield" class="control-label">กองทัพ</label>
                                                <asp:DropDownList ID="ddlทั้งหมด" runat="server" Width="90px">
                                                <asp:ListItem Text=" ส่วนกลาง" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="ภาคที่ 2" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="ภาคที่ 3" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="ภาคที่ 4" Value="3"></asp:ListItem>
                                            </asp:DropDownList>
											</div>
                                            
										</div>
										<div class="span4">
                                            <div class="control-group">
												<label for="textfield" class="control-label">หน่วยสายแพทย์</label>
                                                <asp:DropDownList ID="DropDownList2" runat="server" Width="200px">
                                                <asp:ListItem Text="ทั้งหมด" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="รพ.พระมงกุฎเกล้า" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="รพ.อานันทมหิดล" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="รพ.ค่ายสุรนารี" Value="3"></asp:ListItem>
                                            </asp:DropDownList>
											</div>
										</div>
									 <div class="span1">
										    <div class="control-group">
												    <label for="textfield" class="control-label">จากปี</label>
                                                    <asp:DropDownList ID="DropDownList1" runat="server" Width="90px">
                                                    <asp:ListItem Text="ทั้งหมด" Value="0"></asp:ListItem>
                                                    <asp:ListItem Text="2560" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="2559" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="2558" Value="3"></asp:ListItem>
                                                    <asp:ListItem Text="2557" Value="4"></asp:ListItem>
                                                </asp:DropDownList>
											    </div>
                                              </div>
										<div class="span2">
                                            <div class="control-group">
												<label for="textfield" class="control-label">ประจำเดือน</label>
                                                <asp:DropDownList ID="DropDownList3" runat="server" Width="150">
                                                <asp:ListItem Text="ทั้งหมด" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="มกราคม" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="กุมภาพันธ์" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="มีนาคม" Value="3"></asp:ListItem>
                                                <asp:ListItem Text="เมษายน" Value="4"></asp:ListItem>
                                                <asp:ListItem Text="พฤษภาคม" Value="5"></asp:ListItem>
                                                <asp:ListItem Text="กรกฎาคม" Value="6"></asp:ListItem>
                                                <asp:ListItem Text="สิงหาคม" Value="7"></asp:ListItem>
                                                <asp:ListItem Text="กันยายน" Value="8"></asp:ListItem>
                                                <asp:ListItem Text="ตุลาคม" Value="9"></asp:ListItem>
                                                <asp:ListItem Text="พฤศจิกายน" Value="10"></asp:ListItem>
                                                <asp:ListItem Text="ธันวาคม" Value="11"></asp:ListItem>
                                            </asp:DropDownList>
											</div>
										</div>
                                         <div class="span1">
										    <div class="control-group">
												    <label for="textfield" class="control-label">ถึงปี</label>
                                                    <asp:DropDownList ID="DropDownList4" runat="server" Width="90px">
                                                    <asp:ListItem Text="ทั้งหมด" Value="0"></asp:ListItem>
                                                    <asp:ListItem Text="2560" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="2559" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="2558" Value="3"></asp:ListItem>
                                                    <asp:ListItem Text="2557" Value="4"></asp:ListItem>
                                                </asp:DropDownList>
											    </div>
                                              </div>
										<div class="span2">
                                            <div class="control-group">
												<label for="textfield" class="control-label">ประจำเดือน</label>
                                                <asp:DropDownList ID="DropDownList5" runat="server" Width="150">
                                                <asp:ListItem Text="ทั้งหมด" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="มกราคม" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="กุมภาพันธ์" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="มีนาคม" Value="3"></asp:ListItem>
                                                <asp:ListItem Text="เมษายน" Value="4"></asp:ListItem>
                                                <asp:ListItem Text="พฤษภาคม" Value="5"></asp:ListItem>
                                                <asp:ListItem Text="กรกฎาคม" Value="6"></asp:ListItem>
                                                <asp:ListItem Text="สิงหาคม" Value="7"></asp:ListItem>
                                                <asp:ListItem Text="กันยายน" Value="8"></asp:ListItem>
                                                <asp:ListItem Text="ตุลาคม" Value="9"></asp:ListItem>
                                                <asp:ListItem Text="พฤศจิกายน" Value="10"></asp:ListItem>
                                                <asp:ListItem Text="ธันวาคม" Value="11"></asp:ListItem>
                                            </asp:DropDownList>
											</div>
										</div>
								</form>
                               <div class="box-content">
                                        <form action="#" method="POST" class='form-horizontal form-validate' id="bb">
									       <div class="control-group">
                                             <label for="textfield" class="control-label">ชื่อรายงาน :</label>
										        <div class="control-group">
												        <%--<label for="textfield" class="control-label">ชื่อรายงาน</label>--%>
                                                        <asp:DropDownList ID="DropDownList7" runat="server" Width="870">
                                                        <asp:ListItem Text="ทั้งหมด" Value="0"></asp:ListItem>
                                                        <asp:ListItem Text="1. รายงานสรุปจำนวนผู้มารับบริการของหน่วยสายแพทย์กองทัพบก ในแต่ละปีงบประมาณ" Value="1"></asp:ListItem>
                                                        <asp:ListItem Text="2. สถิติจำนวนผู้มารับบริการแบบผู้ป่วยนอกของหน่วยสายแพทย์ ทบ.รายหน่วย ในแต่ละปีงบประมาณ" Value="2"></asp:ListItem>
                                                        <asp:ListItem Text="3. สถิติจำนวนผู้มารับบริการแบบผู้ป่วยในของหน่วยสายแพทย์ ทบ.รายหน่วย ในแต่ละปีงบประมาณ" Value="3"></asp:ListItem>
                                                        <asp:ListItem Text="4. สถิติข้อมูลการให้บริการด้านการรักษาพยาบาลของโรงพยาบาลกองทัพบก ในแต่ละปีงบประมาณ" Value="4"></asp:ListItem>
                                                        <asp:ListItem Text="5. สถิติเปรียบเทียบข้อมูลการให้บริการด้านการรักษาพยาบาลของโรงพยาบาลกองทัพบก ในแต่ละปีงบประมาณ" Value="5"></asp:ListItem>
                                                        <asp:ListItem Text="6. สถิติจำนวนและอัตราของผู้ป่วยนอกตามกลุ่มสาเหตุป่วย ที่มารับบริการจากหน่วยสายแพทย์กองทัพบก ต่อจำนวนผู้ป่วย 1000 คน ในแต่ละปีงบประมาณ" Value="6"></asp:ListItem>
                                                        <asp:ListItem Text="7. แผนภูมิแสดง 10 อันดับโรคสูงสุดที่พบในผู้ป่วยนอกของหน่วยสายแพทย์กองทัพบก ในแต่ละปีงบประมาณ" Value="7"></asp:ListItem>
                                                        <asp:ListItem Text="8. สถิติจำนวนและอัตราผู้ป่วยในตามกลุ่มสาเหตุป่วย ที่มารับบริการจากหน่วยสายแพทย์กองทัพบก ต่อจำนวนผู้ป่วย 1000 คน ในแต่ละปีงบประมาณ" Value="8"></asp:ListItem>
                                                        <asp:ListItem Text="9. แผนภูมิแสดง 10 อันดับโรคสูงสุดที่พบในผู้ป่วยในของหน่วยสายแพทย์กองทัพบก ในแต่ละปีงบประมาณ" Value="9"></asp:ListItem>
                                                        <asp:ListItem Text="10. แผนภูมิแสดง 10 อันดับสาเหตุการตายสูงสุดของผู้ป่วยในที่ถึงแก่กรรม ในโรงพยาบาลกองทัพบก ในแต่ละปีงบประมาณ" Value="10"></asp:ListItem>
                                                        <asp:ListItem Text="11. แผนภูมิแสดงจำนวนกำลังพลกองทัพบกที่มารับการตรวจร่างกายจากหน่วยสายแพทย์กองทัพบก ในแต่ละปีงบประมาณ" Value="11"></asp:ListItem>
                                                        <asp:ListItem Text="12. แผนภูมิแสดงสถิติการให้บริการด้านการรักษาพยาบาลของโรงพยาบาลกองทัพบก" Value="12"></asp:ListItem>
                                                        <asp:ListItem Text="13. แผนภูมิแสดงสถิติจำนวนผู้มารับบริการแบบผู้ป่วยนอกของหน่วยสายแพทย์ ทบ. " Value="13"></asp:ListItem>
                                                        <asp:ListItem Text="14. แผนภูมิแสดงสถิติจำนวนผู้มารับบริการแบบผู้ป่วยในของหน่วยสายแพทย์ ทบ. " Value="14"></asp:ListItem>
                                                        <asp:ListItem Text="15. สถิติจำนวนผู้มารับบริการแบบผู้ป่วยนอกของหน่วยสายแพทย์ ทบ. รายหน่วย" Value="15"></asp:ListItem>
                                                        <asp:ListItem Text="16. สถิติจำนวนผู้มารับบริการแบบผู้ป่วยในของหน่วยสายแพทย์ ทบ. รายหน่วย" Value="16"></asp:ListItem>
                                                        <asp:ListItem Text="17. สถิติจำนวนผู้ป่วยในจำหน่ายของ รพ.ทบ. แยกรายประเภทบุคคล ประจำปีงบประมาณ" Value="17"></asp:ListItem>
                                                        <asp:ListItem Text="18. สถิติจำนวนผู้ป่วยในที่ยกมาจากเดือนก่อนของหน่วยสายแพทย์ ทบ. จำแนกตามกองทัพภาค  ประจำปีงบประมาณ" Value="18"></asp:ListItem>
                                                        <asp:ListItem Text="19. สถิติผู้มารับบริการแบบผู้ป่วยนอกของหน่วยสายแพทย์ ทบ. จำแนกตามการจัดหน่วย  ประจำปีงบประมาณ" Value="19"></asp:ListItem>
                                                        <asp:ListItem Text="20. สถิติจำนวนผู้มารับบริการของหน่วยสายแพทย์กองทัพบก ประจำปีงบประมาณ" Value="20"></asp:ListItem>
                                                        <asp:ListItem Text="21. สถิติจำนวนผู้มารับบริการของ รพ.ทบ. แยกรายประเภทบุคคล ประจำปีงบประมาณ" Value="21"></asp:ListItem>
                                                    </asp:DropDownList>
                                                      
											        </div>
                                            </div>
                                        </form>
									</div>
                                 <%--<table class="table table-hover table-nomargin table-bordered dataTable-columnfilter dataTable dataTable-noheader table-edit">

										<thead>
										<tr>
                                            <th style="width: 15px"><p class="text-center">ลำดับ</p> </th>
											<th style="width: 85px"><p class="text-center">รายงานหน่วยสายแพทย์</p></th>
										</tr>
									</thead>
										<tbody>
										<tr>
                                            <td><p class="text-center"> 1</p></td>
											<td><a href="FormDepartment_1101.aspx"> รายงานจำนวนผู้ป่วยจำแนกตามประเภทบุคคล ( รง.ผสต.1 ) </a></td>
									    </tr>
										<tr>
                                            <td><p class="text-center"> 2</p></td>
											<td><a href="FormDepartment_1102.aspx"> รายงานจำนวนผู้ป่วยในจำแนกตามสาเหตุป่วย ( รง.ผสต.2 )</a></td>
										</tr>
										<tr>
                                            <td><p class="text-center"> 3</p></td>
											<td><a href="FormDepartment_1103.aspx"> รายงานจำนวนผู้ป่วยในจำแนกตามสาเหตุปลดพิการ ( รง.ผสต.3 )</a></td>
										</tr>
										<tr>
                                            <td><p class="text-center"> 4</p></td>
											<td><a href="FormDepartment_1104.aspx"> รายงานจำนวนผู้ป่วยในจำแนกตามสาเหตุตาย( รง.ผสต.4 )</a></td>
										</tr>
											
										<tr>
                                            <td><p class="text-center"> 5</p></td>
											<td><a href="FormDepartment_1105.aspx"> รายงานจำนวนผู้ป่วยนอกจำแนกตามสาเหตุป่วย ( รง.ผสต.5 )</a></td>
										</tr>
										<tr>
                                            <td><p class="text-center"> 6</p></td>
											<td><a href="FormDepartment_1106.aspx"> บัญชียอดกำลังพลซึ่งต้องสนับสนุนบริการแพทย์( รง.ผสต.6 )</a></td>
										</tr>
										<tr>
                                            <td><p class="text-center"> 7</p></td>
											<td><a href="FormDepartment_1108.aspx"> รายงานจำนวนผู้มารับบริการของ รพ.ทบ. ( ตัวชี้วัด ) ( รง.ผสต.8 )</a></td>
										</tr>
										<tr>
                                            <td><p class="text-center"> 8</p></td>
											<td><a href="FormDepartment_1109.aspx"> รายงานจำนวนผู้ป่วยที่มารับบริการฝังเข็มจำแนกตามประเภทบุคคล ( รง.ผสต.9 )</a></td>
										</tr>
										<tr>
                                            <td><p class="text-center"> 9</p></td>
											<td><a href="FormDepartment_1110.aspx"> รายงานจำนวนผู้ป่วยที่มารับบริการฝังเข็มจำแนกตามสาเหตุป่วยและความพึงพอใจของผู้มารับบริการฝังเข็ม ( รง.ผสต.10 )</a></td>
											
										</tr>
										<tr>
                                            <td><p class="text-center">10</p></td>
											<td><a href="FormDepartment_1111.aspx"> ข้อมูลการตรวจร่างกายของกำลังกองพลทัพบกรายบุคคล ( รง.ผสต.11 )</a></td>
										</tr>
										<tr>
                                            <td><p class="text-center">11</p></td>
											<td><a href="FormDepartment_1112.aspx"> ข้อมูลการตรวจร่างกายของครอบครัวกำลังกองพลทัพบกรายบุคคล ( รง.ผสต.12 )</a></td>
										</tr>
										<tr>
                                            <td><p class="text-center">12</p></td>
											<td><a href="FormDepartment_1117.aspx"> รายงานจำนวนผู้ป่วยนอก จำนวนผู้ป่วยรับมา และจำนวนผุ้ป่วยส่งต่อไปที่อื่น ( รง.ผสต.17 )</a></td>
										</tr>
										<tr>
                                            <td><p class="text-center">13</p></td>
											<td><a href="FormDepartment_1118.aspx"> รายงานจำนวนผู้ป่วยใน จำนวนพยาบาล และจำนวนเตียงที่ใช้จริง ( รง.ผสต.18 )</a></td>
										</tr>
										<tr>
                                            <td><p class="text-center">14</p></td>
											<td><a href="FormDepartment_1119.aspx"> รายงานจำนวนแพทย์ จำแนกตามสาขาของการแพทย์เฉพาะทาง ( รง.ผสต.19 )</a></td>
										</tr>
										<tr>
                                            <td><p class="text-center">15</p></td>
											<td><a href="FormDepartment_1122_1.aspx"> รายงานข้อมูลสถานที่ เครื่องมือแพทย์ และครุภัณฑ์การแพทย์ ( รง.ผสต.22 )</a></td>
										</tr>
										<tr>
                                            <td><p class="text-center">16</p></td>
											<td><a href="FormDepartment_1122_2.aspx"> รายงานข้อมูลสถานที่ เครื่องมือแพทย์ และครุภัณฑ์การแพทย์ ( รง.ผสต.22 )</a></td>
										</tr>
										<tr>
                                            <td><p class="text-center">17</p></td>
											<td><a href="FormDepartment_1123.aspx"> รายงานจำนวนกำลังพลของหน่วยสายแพทย์ จำแนกตามตำแหน่ง ( รง.ผสต.23 )</a></td>
										</tr>
										<tr>
                                            <td><p class="text-center">18</p></td>
											<td><a href="FormDepartment_Draft.aspx"> รายงานการตรวจโรคชายไทยก่อนเกณฑ์ทหาร</a></td>
										</tr>
										<tr>
                                            <td><p class="text-center">19</p></td>
											<td><a href="FormDepartment_Extinction.aspx"> รายงานสถิติการสุญเสีย</a></td>
										</tr>
										<tr>
                                            <td><p class="text-center">20</p></td>
											<td><a href="FinanceDe_Statements170.aspx"> งบแสดงฐานะการเงิน ว170</a></td>
										</tr>
										<tr>
                                            <td><p class="text-center">21</p></td>
											<td><a href="FinanceDe_Performance170.aspx"> งบแสดงผลการดำเนินงาน ว170</a></td>
										</tr>
										<tr>
                                            <td><p class="text-center">22</p></td>
											<td><a href="FinanceDe_Reception.aspx"> รายงานการรับ-จ่ายเงิน ว170</a></td>
										</tr>
										<tr>
                                            <td><p class="text-center">23</p></td>
											<td><a href="FinanceDe_Balance.aspx"> รายงานเงินคงเหลือ ณ วันปลาย ว170</a></td>
										</tr>
										<tr>
                                            <td><p class="text-center">24</p></td>
											<td><a href="FinanceDe_Statements237.aspx"> งบแสดงฐานะการเงิน ว237</a></td>
										</tr>
										<tr>
                                            <td><p class="text-center">25</p></td>
											<td><a href="FinanceDe_Performance237.aspx"> งบแสดงผลการดำเนินงาน ว237</a></td>
										</tr>
										<tr>
                                            <td><p class="text-center">26</p></td>
											<td><a href="MedicineDe_Manage.aspx"> สรุปการบริหารยาและเวชภัณฑ์ ก</a></td>
										</tr>
										<tr>
                                            <td><p class="text-center">27</p></td>
											<td><a href="MedicineDe_Status.aspx"> สถานภาพยาและเวชภัณฑ์ ข</a></td>
										</tr>
										<tr>
                                            <td><p class="text-center">28</p></td>
											<td><a href="MedicineDe_Amount.aspx"> สรุปจำนวนรายการยา (Item)</a></td>
											
										</tr>
										</tbody>
									</table>--%>
						      </div>
							</div>
						</div>
				   	</div>

				</div>
				
			</div>
		</div>
</asp:Content>









