﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Master.Master" AutoEventWireup="true" CodeBehind="Form_1105.aspx.cs" Inherits="AMED.Form_1105" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolder1" runat="server">
    <title>รง.ผสต.5 - กรมการแพทย์ทหารบก</title>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolder3" runat="server">
    <div class="container-fluid" id="content">
		<div id="left">
			<div class="subnav">
				<div class="subnav-title">
					<a href="#" class='toggle-subnav'><i class="icon-angle-down"></i><span><p class="icon-building"></p> หน่วยสายแพทย์</span></a>
				</div>
				<ul class="subnav-menu">
					<li>
						<a href="Default.aspx">สรุปภาพรวม</a>
					</li>
					<li class="active">
							<a href="Hotpital.aspx">การบันทึกข้อมูล</a>
							
						</li>
					<li>
						<a href="Report.aspx">สรุปรายงาน</a>
					</li>
				</ul>
			</div>
			<div class="subnav">
				<div class="subnav-title">
					<a href="#" class='toggle-subnav'><i class="icon-angle-down"></i><span><p class="icon-building"></p>  ส่วนกลาง</span></a>
				</div>
				<ul class="subnav-menu">
					<li>
						<a href="Default.aspx">สรุปภาพรวม</a>
					</li>
					<li>
							<a href="Department.aspx">การตรวจสอบการบันทึกข้อมูล</a>
							
					</li>
					<li>
						<a href="Report.aspx">สรุปรายงาน</a>
					</li>
				</ul>
			</div>
            <div class="subnav">
				<div class="subnav-title">
					<a href="#" class='toggle-subnav'><i class="icon-angle-down"></i><span><p class="icon-cog"></p> ข้อมูลพื้นฐาน</span></a>
				</div>
				<ul class="subnav-menu">
					<li class='dropdown'>
						<a href="#" data-toggle="dropdown"> ข้อมุลทั่วไป</a>
						<ul class="dropdown-menu">
							<li>
								<a href="MT_Region.aspx"> กองทัพภาค</a>
							</li>
							<li>
								<a href="MT_Hospital.aspx"> หน่วยสายแพทย์</a>
							</li>
							<li>
								<a href="MT_Department.aspx"> หน่วยงาน (ส่วนกลาง)</a>
							</li>
							<li>
								<a href="MT_AddOrganization.aspx"> สังกัด</a>
							</li>
							<li>
								<a href="MT_Province.aspx"> จังหวัด</a>
							</li>
						</ul>
					</li>
					<li class='dropdown'>
						<a href="#" data-toggle="dropdown">กองวิทยาการ</a>
						<ul class="dropdown-menu">
							<li>
								<a href="MTS_ICD10.aspx"> รหัสโรค (ICD10)</a>
							</li>
							<li>
								<a href="MTS_GroupRule.aspx"> กลุ่มโรคที่ขัดต่อกฎกระทรวง</a>
							</li>
							<li>
								<a href="MTS_Rule.aspx"> โรคที่ขัดต่อกฎกระทรวง</a>
							</li>
							<li>
								<a href="MTS_Sizebed.aspx"> ขนาดเตียง</a>
							</li>
							<li>
								<a href="MTS_ProHotpital.aspx"> ตำแหน่งในโรงพยาบาล</a>
							</li>
							<li>
								<a href="MTS_Wards.aspx"> หอผู้ป่วย</a>
							</li>
						</ul>
					</li>
                    <li class='dropdown'>
						<a href="#" data-toggle="dropdown">กองงบประมาณการเงิน</a>
						<ul class="dropdown-menu">
							<li>
								<a href="MTF_Assets.aspx">สินทรัพย์และหนี้สิน</a>
							</li>
							<li>
								<a href="MTF_Progress.aspx">ผลการดำเนินงาน</a>
							</li>
							<li>
								<a href="MTF_Reception.aspx">การรับ-การจ่าย</a>
							</li>
							<li>
								<a href="MTF_Deposit.aspx">เงินฝาก</a>
							</li>
						</ul>
					</li>
                    <li class='dropdown'>
						<a href="#" data-toggle="dropdown">กองส่งกำลังบำรุง</a>
						<ul class="dropdown-menu">
							<li>
								<a href="MTT_TypeDrug.aspx"> ประเภทยา</a>
							</li>
							<li>
								<a href="MTT_Drug.aspx"> ยา</a>
							</li>
							<li>
								<a href="MTT_DrugNature.aspx">ลักษณะยา</a>
							</li>
							<li>
								<a href="MTT_MedicalSupplies.aspx">เวชภัณฑ์</a>
							</li>
						</ul>
					</li>
                    <li class='dropdown'>
						<a href="#" data-toggle="dropdown">ผู้ใช้งาน (User)</a>
						<ul class="dropdown-menu">
							<li>
								<a href="MT_PersonType.aspx"> ประเภทบุคคล</a>
							</li>
							<li>
								<a href="MT_Person.aspx">บุคคลย่อย</a>
							</li>
							<li>
								<a href="#">กำหนดสิทธิ์</a>
							</li>
							<li>
								<a href="MT_User.aspx">ผู้ใช้งานในระบบ</a>
							</li>
						</ul>
					</li>
                     <li class='dropdown'>
						<a href="#" data-toggle="dropdown">การส่งรายงาน</a>
						<ul class="dropdown-menu">
							<li>
								<a href="MTR_DeadlineReports.aspx"> กำหนดส่งรายงาน</a>
							</li>
							<li>
								<a href="MTR_EditReport.aspx"> แก้ไขข้อมูลรายงาน</a>
							</li>
							<li>
								<a href="MTR_NameReport.aspx"> ชื่อแบบฟอร์มรายงาน</a>
							</li>
						</ul>
					</li>
				</ul>
			</div>
		</div>
			
		<div id="main">
			<div class="container-fluid">
				<div class="page-header">
					<div class="pull-left">
						<h4>รายงานจำนวนผู้ป่วยนอกจำแนกตามสาเหตุป่วย ( รง.ผสต.5 )</h4>
					</div>
					
				</div>
				<div class="breadcrumbs">
					<ul>
						<li>
							<a href="Default.aspx">Dashborad</a>
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<a href="forms-basic.html">กองวิทยาการ</a>
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<a href="MT_User.aspx">รง.ผสต.5</a>
						</li>
					</ul>
					<div class="close-bread">
						<a href="#"><i class="icon-remove"></i></a>
					</div>
				</div>
				

				<div class="row-fluid">

						<div class="span12">
						  <div class="box">
							<div class="box-title">
                                <div class='basic-margin'>
								<div class="fileupload fileupload-new" data-provides="fileupload">
									<div class="input-append">
										<div class="uneditable-input span3"><i class="icon-file fileupload-exists"></i> <span class="fileupload-preview"></span></div><span class="btn btn-file"><span class="fileupload-new">Select file</span><span class="fileupload-exists">Change</span><input type="file" name="aaaa" /></span><a href="#" class="btn fileupload-exists" data-dismiss="fileupload">Remove</a>
								    	</div>
								   <div class="btn-group span2 pull-right">
                                  <%-- <button class="btn btn-info" rel="tooltip" title="edit"><i class="icon-edit"></i> แก้ไขเอกสาร</button>--%>
												<a href="#" data-toggle="dropdown" class="btn btn-warning dropdown-toggle" rel="tooltip" title="print"><i class="icon-print"></i> พิมพ์รายงาน <span class="caret"></span></a>
												<ul class="dropdown-menu dropdown-warning">
													<li>
														<a href="#"><i class="icon-file"></i> Excel</a>
													</li>
													<li>
														<a href="#"><i class="icon-file"></i> PDF</a>
													</li>
												</ul>
											</div></div>
                                </div>
							</div>
                            <br />
                           
                           <div style="overflow-x:auto;" class="box-content nopadding">
                               <form action="#" method="get" class='form-vertical'>
                                    <div class="span2">
											<div class="control-group">
												<label for="textfield" class="control-label">กองทัพ</label>
                                                <select name="s2" id="s2" class='select2-me input-small'>
												    <option value="1">ทั้งหมด</option>
												    <option value="2" selected disabled>ส่วนกลาง</option>
												    <option value="3">ภาคที่ 1</option>
												    <option value="4">ภาคที่ 2</option>
												    <option value="5">ภาคที่ 3</option>
												    <option value="6">ภาคที่ 4</option>
											      </select>
                                                <%--<asp:DropDownList ID="ddlทั้งหมด" runat="server" Width="150px">
                                                <asp:ListItem Text="หน่วยงาน (ส่วนกลาง)" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="กองทัพภาคที่ 2" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="กองทัพภาคที่ 3" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="กองทัพภาคที่ 4" Value="3"></asp:ListItem>
                                            </asp:DropDownList>--%>
											</div>
                                            
										</div>
										<div class="span3">
                                            <div class="control-group">
												<label for="textfield" class="control-label">หน่วยสายแพทย์</label>
                                                <select name="s2" id="s2" class='select2-me input-large'>
												    <option value="1">ทั้งหมด</option>
												    <option value="2" selected disabled>รพ.พระมงกุฎเกล้า</option>
												    <option value="3">รพ.อานันทมหิดล</option>
												    <option value="4">รพ.ค่ายสุรนารี</option>
											      </select>
                                                <%--<asp:DropDownList ID="DropDownList2" runat="server" Width="230px">
                                                <asp:ListItem Text="ทั้งหมด" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="รพ.พระมงกุฎเกล้า" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="รพ.อานันทมหิดล" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="รพ.ค่ายสุรนารี" Value="3"></asp:ListItem>
                                            </asp:DropDownList>--%>
											</div>
										</div>
                                          <div class="span2">
										    <div class="control-group">
                                                <label for="textfield" class="control-label">ปีงบประมาณ</label>
                                                   <select name="s2" id="s2" class='select2-me input-small'>
												    <option value="1">ทั้งหมด</option>
												    <option value="2" selected disabled>2560</option>
												    <option value="3">2559</option>
												    <option value="4">2558</option>
												    <option value="5">2557</option>
												    <option value="6">2556</option>
												    <option value="7">2555</option>
											      </select>
												 <%--<label for="textfield" class="control-label">ปีงบประมาณ</label>
                                                    <asp:DropDownList ID="DropDownList1" runat="server" Width="120px">
                                                    <asp:ListItem Text="ทั้งหมด" Value="0"></asp:ListItem>
                                                    <asp:ListItem Text="2560" Value="1"></asp:ListItem>
                                                    <asp:ListItem Text="2559" Value="2"></asp:ListItem>
                                                    <asp:ListItem Text="2558" Value="3"></asp:ListItem>
                                                    <asp:ListItem Text="2557" Value="4"></asp:ListItem>
                                                </asp:DropDownList>--%>
											    </div>
                                              </div>
										<div class="span3">
                                            <div class="control-group">
                                              <label for="textfield" class="control-label">ประจำเดือน</label>
                                                <select name="s2" id="s2" class='select2-me input-large'>
												    <option value="1">ทั้งหมด</option>
												    <option value="2" selected disabled>มกราคม</option>
												    <option value="3">กุมภาพันธ์</option>
												    <option value="4">มีนาคม</option>
												    <option value="5">เมษายน</option>
												    <option value="6">พฤษภาคม</option>
												    <option value="7">กรกฎาคม</option>
												    <option value="4">สิงหาคม</option>
												    <option value="5">กันยายน</option>
												    <option value="6">ตุลาคม</option>
												    <option value="7">พฤศจิกายน</option>
												    <option value="7">ธันวาคม</option>
											      </select>
												<%--<label for="textfield" class="control-label">ประจำเดือน</label>
                                                <asp:DropDownList ID="DropDownList3" runat="server" Width="200">
                                                <asp:ListItem Text="ทั้งหมด" Value="0"></asp:ListItem>
                                                <asp:ListItem Text="มกราคม" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="กุมภาพันธ์" Value="2"></asp:ListItem>
                                                <asp:ListItem Text="มีนาคม" Value="3"></asp:ListItem>
                                                <asp:ListItem Text="เมษายน" Value="4"></asp:ListItem>
                                                <asp:ListItem Text="พฤษภาคม" Value="5"></asp:ListItem>
                                                <asp:ListItem Text="กรกฎาคม" Value="6"></asp:ListItem>
                                                <asp:ListItem Text="สิงหาคม" Value="7"></asp:ListItem>
                                                <asp:ListItem Text="กันยายน" Value="8"></asp:ListItem>
                                                <asp:ListItem Text="ตุลาคม" Value="9"></asp:ListItem>
                                                <asp:ListItem Text="พฤศจิกายน" Value="10"></asp:ListItem>
                                                <asp:ListItem Text="ธันวาคม" Value="11"></asp:ListItem>
                                            </asp:DropDownList>--%>
											</div>
										</div>
									
								</form>
								<table class="table table-hover table-nomargin table-condensed table-bordered table-edit">
									<thead>
										<tr>
											<th rowspan="2" style="width: 20px"><p class="text-center">กลุ่มโรคที่</p></th>
											<th rowspan="2" style="width: 50px"><p class="text-center">รหัสโรค</p></th>
											<th rowspan="2" style="width: 150px"><p class="text-center">สาเหตุป่วย ( ชื่อโรค )</p></th>
											<th colspan="3" style="width: 50px"><p class="text-center">จำนวนผู้ป่วยนอก</p></th>
										</tr>
										<tr>
											<th style="width: 20px"><p class="text-center">ก</p></th>
											<th style="width: 20px"><p class="text-center">ข</p></th>
											<th style="width: 20px"><p class="text-center">ค</p></th>
										</tr>
									</thead>
									<tbody>
										<tr>
											<td><p class="text-center">1</p></td>
											<td><p class="text-center">A00-A99 <br />B00-B99</p></td>
											<td>โรคติดเชื้อและปรสิต <br />Certain  infectious  and  parasitic  diseases</td>
											
											<td><asp:TextBox ID="TextBox1" runat="server" CssClass="input-block-level" Width="150"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox2" runat="server" CssClass="input-block-level" Width="150"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox3" runat="server" CssClass="input-block-level" Width="150"></asp:TextBox></td>
										</tr>
										
										<tr>
											<td><p class="text-center">2</p></td>
											<td><p class="text-center">C00-C97 <br />D00-D48</p></td>
											<td>เนื้องอก ( รวมมะเร็ง ) <br />Neoplasms</td>
											
											<td><asp:TextBox ID="TextBox4" runat="server" CssClass="input-block-level" Width="150"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox5" runat="server" CssClass="input-block-level" Width="150"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox6" runat="server" CssClass="input-block-level" Width="150"></asp:TextBox></td>
										</tr>
										
										<tr>
											<td><p class="text-center">3</p></td>
											<td><p class="text-center">D50-D89 <br /></p></td>
											<td>โรคเลือดและอวัยวะสร้างเลือด  และความผิดปกติเกี่ยวกับภูมิคุ้มกัน <br />Diseases  of  the  blood  and  blood  forming  organs  and  certain  disorders  involving  the  immune  mechanism</td>
											
											<td><asp:TextBox ID="TextBox7" runat="server" CssClass="input-block-level" Width="150"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox8" runat="server" CssClass="input-block-level" Width="150"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox9" runat="server" CssClass="input-block-level" Width="150"></asp:TextBox></td>
										</tr>
										
										<tr>
											<td><p class="text-center">4</p></td>
											<td><p class="text-center">E00-E90<br /></p></td>
											<td>โรคเกี่ยวกับต่อมไร้ท่อ  โภชนาการ  และเมตะบอลิสัม <br />Endocrine, nutritional  and  metabolic  diseases</td>
											
											<td><asp:TextBox ID="TextBox10" runat="server" CssClass="input-block-level" Width="150"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox11" runat="server" CssClass="input-block-level" Width="150"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox12" runat="server" CssClass="input-block-level" Width="150"></asp:TextBox></td>
										</tr>
										
										<tr>
											<td><p class="text-center">5</p></td>
											<td><p class="text-center">F00-F99 <br /></p></td>
											<td>ภาวะแปรปรวนทางจิตและพฤติกรรม <br />Mental  and  behavioural  disorders</td>
											
											<td><asp:TextBox ID="TextBox13" runat="server" CssClass="input-block-level" Width="150"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox14" runat="server" CssClass="input-block-level" Width="150"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox15" runat="server" CssClass="input-block-level" Width="150"></asp:TextBox></td>
										</tr>
										
										<tr>
											<td><p class="text-center">6</p></td>
											<td><p class="text-center">G00-G99 <br />B00-B99</p></td>
											<td>โรคระบบประสาท <br />Diseases  of  the  nervous  system</td>
											
											<td><asp:TextBox ID="TextBox16" runat="server" CssClass="input-block-level" Width="150"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox17" runat="server" CssClass="input-block-level" Width="150"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox18" runat="server" CssClass="input-block-level" Width="150"></asp:TextBox></td>
										</tr>
										
										<tr>
											<td><p class="text-center">7</p></td>
											<td><p class="text-center">A00-A99 <br />B00-B99</p></td>
											<td>โรคตารวมส่วนประกอบของตา <br />Diseases  of  the  eye  and  adnexa</td>
											
											<td><asp:TextBox ID="TextBox19" runat="server" CssClass="input-block-level" Width="150"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox20" runat="server" CssClass="input-block-level" Width="150"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox21" runat="server" CssClass="input-block-level" Width="150"></asp:TextBox></td>
										</tr>
										
										<tr>
											<td><p class="text-center">8</p></td>
											<td><p class="text-center">H60-H95 <br /></p></td>
											<td>โรคหูและปุ่มกกหู <br />Diseases  of  the  ear  and  mastoid  process</td>
											
											<td><asp:TextBox ID="TextBox22" runat="server" CssClass="input-block-level" Width="150"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox23" runat="server" CssClass="input-block-level" Width="150"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox24" runat="server" CssClass="input-block-level" Width="150"></asp:TextBox></td>
										</tr>
										
										<tr>
											<td><p class="text-center">9</p></td>
											<td><p class="text-center">I00-I99 <br /></p></td>
											<td>โรคหูและปุ่มกกหู <br />Diseases  of  the  ear  and  mastoid  process</td>
											
											<td><asp:TextBox ID="TextBox25" runat="server" CssClass="input-block-level" Width="150"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox26" runat="server" CssClass="input-block-level" Width="150"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox27" runat="server" CssClass="input-block-level" Width="150"></asp:TextBox></td>
										</tr>
										
										<tr>
											<td><p class="text-center">10</p></td>
											<td><p class="text-center">J00-J99 <br /></p></td>
											<td>โรคระบบหายใจ <br />Diseases  of  the  respiratory  system</td>
											
											<td><asp:TextBox ID="TextBox28" runat="server" CssClass="input-block-level" Width="150"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox29" runat="server" CssClass="input-block-level" Width="150"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox30" runat="server" CssClass="input-block-level" Width="150"></asp:TextBox></td>
										</tr>

										<tr>
											<td><p class="text-center">11</p></td>
											<td><p class="text-center">K00-K93 <br /></p></td>
											<td>โรคระบบย่อยอาหาร  รวมโรคในช่องปาก <br />Diseases  of  the  digestive  system</td>
											
											<td><asp:TextBox ID="TextBox31" runat="server" CssClass="input-block-level" Width="150"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox32" runat="server" CssClass="input-block-level" Width="150"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox33" runat="server" CssClass="input-block-level" Width="150"></asp:TextBox></td>
										</tr>

										<tr>
											<td><p class="text-center">12</p></td>
											<td><p class="text-center">L00-L99<br /></p></td>
											<td>โรคผิวหนังและเนื้อเยื่อใต้ผิวหนัง <br />Diseases  of  the  skin  and  subcutaneous  tissue</td>
											
											<td><asp:TextBox ID="TextBox34" runat="server" CssClass="input-block-level" Width="150"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox35" runat="server" CssClass="input-block-level" Width="150"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox36" runat="server" CssClass="input-block-level" Width="150"></asp:TextBox></td>
										</tr>
                                        
										<tr>
											<td><p class="text-center">13</p></td>
											<td><p class="text-center">M00-M99 <br /></p></td>
											<td>โรคระบบย่อยอาหาร  รวมโรคในช่องปาก <br />Diseases  of  the  digestive  system</td>
											
											<td><asp:TextBox ID="TextBox37" runat="server" CssClass="input-block-level" Width="150"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox38" runat="server" CssClass="input-block-level" Width="150"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox39" runat="server" CssClass="input-block-level" Width="150"></asp:TextBox></td>
										</tr>

										<tr>
											<td><p class="text-center">14</p></td>
											<td><p class="text-center">I00-I99 <br /></p></td>
											<td>โรคระบบกล้ามเนื้อ  รวมโครงร่าง  และเนื้อยึดเสริม <br />Diseases  of  the  musculoskeletal  system  and  connective  tissue</td>
											
											<td><asp:TextBox ID="TextBox40" runat="server" CssClass="input-block-level" Width="150"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox41" runat="server" CssClass="input-block-level" Width="150"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox42" runat="server" CssClass="input-block-level" Width="150"></asp:TextBox></td>
										</tr>

										<tr>
											<td><p class="text-center">15</p></td>
											<td><p class="text-center">N00-N99 <br /></p></td>
											<td>โรคระบบสืบพันธุ์ร่วมปัสสาวะ <br />Diseases  of  the  genitourinary  system</td>
											
											<td><asp:TextBox ID="TextBox43" runat="server" CssClass="input-block-level" Width="150"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox44" runat="server" CssClass="input-block-level" Width="150"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox45" runat="server" CssClass="input-block-level" Width="150"></asp:TextBox></td>
										</tr>

										<tr>
											<td><p class="text-center">16</p></td>
											<td><p class="text-center">P00-P96<br /></p></td>
											<td>ภาวะผิดปกติของทารกที่เกิดขึ้นในระยะปริกำเนิด ( อายุครรภ์ 22 สัปดาห์ขึ้นไปจนถึง 7 วัน  หลังคลอด ) <br />Certain  conditions  originating  in  the  perinatal  period</td>
											
											<td><asp:TextBox ID="TextBox46" runat="server" CssClass="input-block-level" Width="150"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox47" runat="server" CssClass="input-block-level" Width="150"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox48" runat="server" CssClass="input-block-level" Width="150"></asp:TextBox></td>
										</tr>

										<tr>
											<td><p class="text-center">17</p></td>
											<td><p class="text-center">Q00-Q99 <br /></p></td>
											<td>รูปร่างผิดปกติแต่กำเนิด  การพิการจนผิดรูปแต่กำเนิดและโครโมโซมผิดปกติ <br />Congenital  malformations , deformations  and  chromosomal  abnormalities</td>
											
											<td><asp:TextBox ID="TextBox49" runat="server" CssClass="input-block-level" Width="150"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox50" runat="server" CssClass="input-block-level" Width="150"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox51" runat="server" CssClass="input-block-level" Width="150"></asp:TextBox></td>
										</tr>

										<tr>
											<td><p class="text-center">18</p></td>
											<td><p class="text-center">R00-R99<br /></p></td>
											<td>อาการ , อาการแสดงและสิ่งผิดปกติที่พบได้จากการตรวจทางคลินิกและทางห้องปฏิบัติการที่ไม่สามารถจำแนกโรคในกลุ่มอื่นได้ <br />Symptoms , signs  and  abnormal  clinical  and  laboratory  findings , not  elsewhere  classified</td>
											
											<td><asp:TextBox ID="TextBox52" runat="server" CssClass="input-block-level" Width="150"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox53" runat="server" CssClass="input-block-level" Width="150"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox54" runat="server" CssClass="input-block-level" Width="150"></asp:TextBox></td>
										</tr>

										<tr>
											<td><p class="text-center">19</p></td>
											<td><p class="text-center">X40-X49 <br />X60-X69<br />X85-X90<br />Y10-Y19</p></td>
											<td>การเป็นพิษและผลที่ตามมา <br />Poisoning , toxic  effect , and  their  sequelae</td>
											
											<td><asp:TextBox ID="TextBox55" runat="server" CssClass="input-block-level" Width="150"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox56" runat="server" CssClass="input-block-level" Width="150"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox57" runat="server" CssClass="input-block-level" Width="150"></asp:TextBox></td>
										</tr>

										<tr>
											<td><p class="text-center">20</p></td>
											<td><p class="text-center">V01-V99<br />Y85</p></td>
											<td>อุบัติเหตุจากการขนส่ง  และผลที่ตามมา <br />Transport  accidents  and  their  sequelae</td>
											
											<td><asp:TextBox ID="TextBox58" runat="server" CssClass="input-block-level" Width="150"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox59" runat="server" CssClass="input-block-level" Width="150"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox60" runat="server" CssClass="input-block-level" Width="150"></asp:TextBox></td>
										</tr>
                                        
										<tr>
											<td><p class="text-center">21</p></td>
											<td><p class="text-center">W00-W99<br />X00-X19<br />X20-X29<br />X30-X39<br />X50-X59<br />X70-X84<br />X91-X9<br />Y00-Y09<br />Y20-Y36<br />Y40-Y84<br />Y86-Y89</p></td>
											<td>สาเหตุจากภายนอกอื่นๆ  ที่ทำให้ป่วยหรือตายา <br />Other  external  causes  of  morbidity  and  mortality ( eg. accidents , injuries , intentional  self-harm , assault , animals  and  plants , complications  of  medical  and  surgical  care  and  other  unspecified causes )</td>
											
											<td><asp:TextBox ID="TextBox61" runat="server" CssClass="input-block-level" Width="150"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox62" runat="server" CssClass="input-block-level" Width="150"></asp:TextBox></td>
											<td><asp:TextBox ID="TextBox63" runat="server" CssClass="input-block-level" Width="150"></asp:TextBox></td>
										</tr>
                                        <tr>
                                            <td colspan="6">
									        <div class="form-actions pull-right">
										        <button type="submit" class="btn btn-green"><i class="icon-ok"></i> บันทึกข้อมูล</button>
										        <button type="submit" class="btn btn-green"><i class="icon-ok"></i> ยืนยันและส่งข้อมูล</button>
										        <button type="button" class="btn btn-lightred"><i class="icon-remove"></i> ยกเลิก</button>
									        </div>
                                            </td>
                                        </tr>
									</tbody>
								</table>
							</div>
						</div>
				   	</div>

				</div>
				
			</div>
		</div>

    </div>
</asp:Content>









