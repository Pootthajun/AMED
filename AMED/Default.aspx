﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="AMED.Default" %>

<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
	<!-- Apple devices fullscreen -->
	<meta name="apple-mobile-web-app-capable" content="yes" />
	<!-- Apple devices fullscreen -->
	<meta names="apple-mobile-web-app-status-bar-style" content="black-translucent" />
	
	<title>FLAT - Dashboard</title>

	<!-- Bootstrap -->
	<link rel="stylesheet" href="assets/css/bootstrap.min.css">
	<!-- Bootstrap responsive -->
	<link rel="stylesheet" href="assets/css/bootstrap-responsive.min.css">
	<!-- jQuery UI -->
	<link rel="stylesheet" href="assets/css/plugins/jquery-ui/smoothness/jquery-ui.css">
	<link rel="stylesheet" href="assets/css/plugins/jquery-ui/smoothness/jquery.ui.theme.css">
	<!-- PageGuide -->
	<link rel="stylesheet" href="assets/css/plugins/pageguide/pageguide.css">
	<!-- Fullcalendar -->
	<link rel="stylesheet" href="assets/css/plugins/fullcalendar/fullcalendar.css">
	<link rel="stylesheet" href="assets/css/plugins/fullcalendar/fullcalendar.print.css" media="print">
	<!-- chosen -->
	<link rel="stylesheet" href="assets/css/plugins/chosen/chosen.css">
	<!-- select2 -->
	<link rel="stylesheet" href="assets/css/plugins/select2/select2.css">
	<!-- icheck -->
	<link rel="stylesheet" href="assets/css/plugins/icheck/all.css">
	<!-- Theme CSS -->
	<link rel="stylesheet" href="assets/css/style.css">
	<!-- Color CSS -->
	<link rel="stylesheet" href="assets/css/themes.css">

	<!-- jQuery -->
	<script src="assets/js/jquery.min.js"></script>
	
	<!-- Nice Scroll -->
	<script src="assets/js/plugins/nicescroll/jquery.nicescroll.min.js"></script>
	<!-- jQuery UI -->
	<script src="assets/js/plugins/jquery-ui/jquery.ui.core.min.js"></script>
	<script src="assets/js/plugins/jquery-ui/jquery.ui.widget.min.js"></script>
	<script src="assets/js/plugins/jquery-ui/jquery.ui.mouse.min.js"></script>
	<script src="assets/js/plugins/jquery-ui/jquery.ui.draggable.min.js"></script>
	<script src="assets/js/plugins/jquery-ui/jquery.ui.resizable.min.js"></script>
	<script src="assets/js/plugins/jquery-ui/jquery.ui.sortable.min.js"></script>
	<!-- Touch enable for jquery UI -->
	<script src="assets/js/plugins/touch-punch/jquery.touch-punch.min.js"></script>
	<!-- slimScroll -->
	<script src="assets/js/plugins/slimscroll/jquery.slimscroll.min.js"></script>
	<!-- Bootstrap -->
	<script src="assets/js/bootstrap.min.js"></script>
	<!-- vmap -->
	<script src="assets/js/plugins/vmap/jquery.vmap.min.js"></script>
	<script src="assets/js/plugins/vmap/jquery.vmap.world.js"></script>
	<script src="assets/js/plugins/vmap/jquery.vmap.sampledata.js"></script>
	<!-- Bootbox -->
	<script src="assets/js/plugins/bootbox/jquery.bootbox.js"></script>
	<!-- Flot -->
	<script src="assets/js/plugins/flot/jquery.flot.min.js"></script>
	<script src="assets/js/plugins/flot/jquery.flot.bar.order.min.js"></script>
	<script src="assets/js/plugins/flot/jquery.flot.pie.min.js"></script>
	<script src="assets/js/plugins/flot/jquery.flot.resize.min.js"></script>
	<!-- imagesLoaded -->
	<script src="assets/js/plugins/imagesLoaded/jquery.imagesloaded.min.js"></script>
	<!-- PageGuide -->
	<script src="assets/js/plugins/pageguide/jquery.pageguide.js"></script>
	<!-- FullCalendar -->
	<script src="assets/js/plugins/fullcalendar/fullcalendar.min.js"></script>
	<!-- Chosen -->
	<script src="assets/js/plugins/chosen/chosen.jquery.min.js"></script>
	<!-- select2 -->
	<script src="assets/js/plugins/select2/select2.min.js"></script>
	<!-- icheck -->
	<script src="assets/js/plugins/icheck/jquery.icheck.min.js"></script>

	<!-- Theme framework -->
	<script src="assets/js/eakroko.min.js"></script>
	<!-- Theme scripts -->
	<script src="assets/js/application.min.js"></script>
	<!-- Just for demonstration -->
	<script src="assets/js/demonstration.min.js"></script>
	
	<!-- Favicon -->
	<link rel="shortcut icon" href="img/favicon.ico" />
	<!-- Apple devices Dashboradscreen icon -->
	<link rel="apple-touch-icon-precomposed" href="img/apple-touch-icon-precomposed.png" />
    <style>
                li.highli {
                    line-height: 30px;
                }
              .table-edit td input {
               border:none;
               }
              .table-edit th input {
               border:none;
               }
              .control {
                  border:none;
              }
            </style>
</head>

<body>
	
	
	<div id="navigation">
		<div class="container-fluid">
	      <img src="assets/img/header/logo.png">
			    
			    <div class="user">
				    <ul class='main-nav'>
				    
                    <li class="highli">
					    <a href="#" data-toggle="dropdown" class='dropdown-toggle'>
						    <span><i class="icon-user"></i>  Atiwat</span>
					    </a>
					    <ul class="dropdown-menu pull-right">
						    <li>
							    <a href="more-userprofile.html">Edit profile</a>
						    </li>
						    <li>
							    <a href="#">Account settings</a>
						    </li>
						    <li>
							    <a href="more-login.html">Sign out</a>
						    </li>
					    </ul>
				    </li>
			    </ul>
		    </div></div>
	</div>
	<div class="container-fluid" id="content">
		<div id="left">
			<div class="subnav">
				<div class="subnav-title">
					<a href="#" class='toggle-subnav'><i class="icon-angle-down"></i><span><p class="icon-building"></p> หน่วยสายแพทย์</span></a>
				</div>
				<ul class="subnav-menu">
					<li class="active">
						<a href="Default.aspx">สรุปภาพรวม</a>
					</li>
					<li>
							<a href="Hotpital.aspx">การบันทึกข้อมูล</a>
							
						</li>
					<li>
						<a href="Report.aspx">สรุปรายงาน</a>
					</li>
				</ul>
			</div>
			<div class="subnav">
				<div class="subnav-title">
					<a href="#" class='toggle-subnav'><i class="icon-angle-down"></i><span><p class="icon-building"></p>  ส่วนกลาง</span></a>
				</div>
				<ul class="subnav-menu">
					<li>
						<a href="Default.aspx">สรุปภาพรวม</a>
					</li>
					<li>
							<a href="Department.aspx">การตรวจสอบการบันทึกข้อมูล</a>
							
					</li>
					<li>
						<a href="Report.aspx">สรุปรายงาน</a>
					</li>
				</ul>
			</div>
            <div class="subnav">
				<div class="subnav-title">
					<a href="#" class='toggle-subnav'><i class="icon-angle-down"></i><span><p class="icon-cog"></p> ข้อมูลพื้นฐาน</span></a>
				</div>
				<ul class="subnav-menu">
					<li class='dropdown'>
						<a href="#" data-toggle="dropdown"> ข้อมุลทั่วไป</a>
						<ul class="dropdown-menu">
							<li>
								<a href="MT_Region.aspx"> กองทัพภาค</a>
							</li>
							<li>
								<a href="MT_Hospital.aspx"> หน่วยสายแพทย์</a>
							</li>
							<li>
								<a href="MT_Department.aspx"> หน่วยงาน (ส่วนกลาง)</a>
							</li>
							<li>
								<a href="MT_AddOrganization.aspx"> สังกัด</a>
							</li>
							<li>
								<a href="MT_Province.aspx"> จังหวัด</a>
							</li>
						</ul>
					</li>
					<li class='dropdown'>
						<a href="#" data-toggle="dropdown">กองวิทยาการ</a>
						<ul class="dropdown-menu">
							<li>
								<a href="MTS_ICD10.aspx"> รหัสโรค (ICD10)</a>
							</li>
							<li>
								<a href="MTS_GroupRule.aspx"> กลุ่มโรคที่ขัดต่อกฎกระทรวง</a>
							</li>
							<li>
								<a href="MTS_Rule.aspx"> โรคที่ขัดต่อกฎกระทรวง</a>
							</li>
							<li>
								<a href="MTS_Sizebed.aspx"> ขนาดเตียง</a>
							</li>
							<li>
								<a href="MTS_ProHotpital.aspx"> ตำแหน่งในโรงพยาบาล</a>
							</li>
							<li>
								<a href="MTS_Wards.aspx"> หอผู้ป่วย</a>
							</li>
						</ul>
					</li>
                    <li class='dropdown'>
						<a href="#" data-toggle="dropdown">กองงบประมาณการเงิน</a>
						<ul class="dropdown-menu">
							<li>
								<a href="MTF_Assets.aspx">สินทรัพย์และหนี้สิน</a>
							</li>
							<li>
								<a href="MTF_Progress.aspx">ผลการดำเนินงาน</a>
							</li>
							<li>
								<a href="MTF_Reception.aspx">การรับ-การจ่าย</a>
							</li>
							<li>
								<a href="MTF_Deposit.aspx">เงินฝาก</a>
							</li>
						</ul>
					</li>
                    <li class='dropdown'>
						<a href="#" data-toggle="dropdown">กองส่งกำลังบำรุง</a>
						<ul class="dropdown-menu">
							<li>
								<a href="MTT_TypeDrug.aspx"> ประเภทยา</a>
							</li>
							<li>
								<a href="MTT_Drug.aspx"> ยา</a>
							</li>
							<li>
								<a href="MTT_DrugNature.aspx">ลักษณะยา</a>
							</li>
							<li>
								<a href="MTT_MedicalSupplies.aspx">เวชภัณฑ์</a>
							</li>
						</ul>
					</li>
                    <li class='dropdown'>
						<a href="#" data-toggle="dropdown">ผู้ใช้งาน (User)</a>
						<ul class="dropdown-menu">
							<li>
								<a href="MT_PersonType.aspx"> ประเภทบุคคล</a>
							</li>
							<li>
								<a href="MT_Person.aspx">บุคคลย่อย</a>
							</li>
							<li>
								<a href="#">กำหนดสิทธิ์</a>
							</li>
							<li>
								<a href="MT_User.aspx">ผู้ใช้งานในระบบ</a>
							</li>
						</ul>
					</li>
                     <li class='dropdown'>
						<a href="#" data-toggle="dropdown">การส่งรายงาน</a>
						<ul class="dropdown-menu">
							<li>
								<a href="MTR_DeadlineReports.aspx"> กำหนดส่งรายงาน</a>
							</li>
							<li>
								<a href="MTR_EditReport.aspx"> แก้ไขข้อมูลรายงาน</a>
							</li>
							<li>
								<a href="MTR_NameReport.aspx"> ชื่อแบบฟอร์มรายงาน</a>
							</li>
						</ul>
					</li>
				</ul>
			</div>
		</div>
		<div id="main">
			<div class="container-fluid">
				<div class="page-header">
					<div class="pull-left">
						<h1>Dashboard</h1>
					</div>
				</div>
				<div class="breadcrumbs">
					<ul>
						<li>
							<a href="Default.aspx">Dashborad</a>
							<i class="icon-angle-right"></i>
						</li>
						<li>
							<a href="index.html">สรุปภาพรวม</a>
						</li>
					</ul>
					<div class="close-bread">
						<a href="#"><i class="icon-remove"></i></a>
					</div>
				</div>
				<div class="row-fluid">
					<div class="span6">
						<div class="box box-color box-bordered">
							<div class="box-title">
								<h3>
									<i class="icon-bar-chart"></i>
									จำนวนผู้ป่วยนอก (6 เดือนย้อนหลัง)
								</h3>
								<div class="actions">
									<a href="#" class="btn btn-mini content-refresh"><i class="icon-refresh"></i></a>
									<a href="#" class="btn btn-mini content-remove"><i class="icon-remove"></i></a>
									<a href="#" class="btn btn-mini content-slideUp"><i class="icon-angle-down"></i></a>
								</div>
							</div>
							<div class="box-content">
								<div class="statistic-big">
									
									<div class="bottom">
										<div class="flot medium" id="flot-audience"></div>
									</div>
									
								</div>
							</div>
						</div>
					</div>
					<div class="span6">
						<div class="box box-color lightred box-bordered">
							<div class="box-title">
								<h3>
									<i class="icon-bar-chart"></i>
									จำนวนผู้ป่วยใน (6 เดือนย้อนหลัง)
								</h3>
								<div class="actions">
									<a href="#" class="btn btn-mini content-refresh"><i class="icon-refresh"></i></a>
									<a href="#" class="btn btn-mini content-remove"><i class="icon-remove"></i></a>
									<a href="#" class="btn btn-mini content-slideUp"><i class="icon-angle-down"></i></a>
								</div>
							</div>
							<div class="box-content">
								<div class="statistic-big">
									
									<div class="bottom">
										<div class="flot medium" id="flot-hdd"></div>
									</div>
									
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div></div>
		
	</body>

	</html>


